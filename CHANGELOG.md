# Changelog


## Release version 1.4.0 (05 juin 2018)

* [Mod] shell now use a logger to log information instead of System.out/err

---

## Release version 1.3.0 (18 mai 2018)

* [Mod] SystemExec Shell implementation:
  - add support for os-dependent  shell creation
  - redefine Shell API
* [CI] gitlab-ci can now use variable 'JENKINS\_FAIL\_MODE' to specify the behaviour depending on test results

---

## Release version 1.2.0 (22 mars 2018)

* [Mod] separate shell interface to allow multi-shell (for different systems) use.
* [Mod] Enable debug mode '1' by default
* [CI] Fix Jenkins pipeline configuration
* [CI] deploying a release version now automatically update 'gecos-composite' to 
  the latest version in a dedicated branch.

---

## Release version 1.1.0 (19 Jul 2017)

* [Add] a File utility class 'FileUtils'

---

## Release version 1.0.0 (07 Jul 2017)

Initial version.

Migrated GeCoS Framework plugins from SVN repository at
*/trunk/gecos/framework (rev 17741)*


