package fr.irisa.r2d2.parser.runtime;

import java.io.IOException;
import java.io.InputStream;

public class TableInputStream {
	
	private InputStream stream;

	public TableInputStream(InputStream stream) {
		this.stream = stream;
	}

	public short[] readRLETable() throws IOException {
		int length = readValue();
		int max = readValue();
		short[] table = new short[max+1];
		int index = 0;
		for (int i = 0; i < length; ++i) {
			int count = readValue();
			short value = (short) readValue();
			while (count-- > 0 && index <= max) {
				table[index++] = value;
			}
		}
		return table;
	}
	
	public short[] readSimpleTable() throws IOException {
		int length = readValue();
		short[] table = new short[length];
		for (int i = 0; i < length; ++i)
			table[i] = (short) readValue();
		return table;
	}
	
	public int readValue() throws IOException {
		int b = stream.read();
		b = (b<<8) | stream.read();
		if ((b & 0x8000) != 0)
			b -= 0x10000;
		return b;
	}
}
