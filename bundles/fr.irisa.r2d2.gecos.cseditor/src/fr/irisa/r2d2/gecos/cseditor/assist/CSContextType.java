/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.assist;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateContextType;

public class CSContextType extends TemplateContextType {
	
	public static final String NAME = "CompilerScript"; //$NON-NLS-1$
	
	public CSContextType() {
		super(NAME);
	}
	
	public TemplateContext createContext(IDocument document, int offset, String prefix) {
		return new CSContext(this, document, offset, prefix);
	}

}
