/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class CSImages {
	
	private static final URL ICON_BASE_URL = CSEditorPlugin.getDefault().getBundle().getEntry("/icons/");

	public static final String IMG_CS_FILE = "IMG_CS_FILE"; //$NON-NLS-1$
	public static final String IMG_PRIM_RULE = "IMG_PRIM_RULE"; //$NON-NLS-1$
	public static final String IMG_OBJS_TEMPLATE = "IMG_OBJS_TEMPLATE"; //$NON-NLS-1$
    public static final String IMG_OBJS_FUNC = "IMG_OBJS_FUNC"; //$NON-NLS-1$
    public static final String IMG_OBJS_FUNC_ITER = "IMG_OBJS_FUNC_ITER"; //$NON-NLS-1$
	public static final String IMG_OBJS_FUNC_DEPRECATED = "IMG_OBJS_FUNC_DEPRECATED";
    public static final String IMG_MODULES_EXPORT_BANNER = "IMG_MODULES_EXPORT_BANNER"; //$NON-NLS-1$
	public static final String IMG_OBJS_FUNC_CNST_DEPRECATED = "IMG_OBJS_FUNC_CNST_DEPRECATED";

	
	private static ImageRegistry imageRegistry = null;
	
	public static Image get(String key) {
		return getImageRegistry().get(key);
	}
	
	public static ImageDescriptor getDescriptor(String key) {
		return getImageRegistry().getDescriptor(key);
	}
	
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null)
			initializeImageRegistry();
		return imageRegistry;
	}
	
	private static void initializeImageRegistry() {
		imageRegistry = new ImageRegistry(Display.getDefault());
		declareImages();
	}
	
	private static void declareImages() {
		declareRegistryImage(IMG_CS_FILE, "csfile.gif"); //$NON-NLS-1$
		declareRegistryImage(IMG_PRIM_RULE, "prim_obj.gif"); //$NON-NLS-1$
		declareRegistryImage(IMG_OBJS_TEMPLATE, "template_obj.gif"); //$NON-NLS-1$
        declareRegistryImage(IMG_OBJS_FUNC, "func_obj.gif"); //$NON-NLS-1$
        declareRegistryImage(IMG_OBJS_FUNC_ITER, "func_iter_obj.gif"); //$NON-NLS-1$
        declareRegistryImage(IMG_OBJS_FUNC_DEPRECATED, "func_obj_deprecated.png"); //$NON-NLS-1$
        declareRegistryImage(IMG_MODULES_EXPORT_BANNER, "export_wiz.png"); //$NON-NLS-1$
        declareRegistryImage(IMG_OBJS_FUNC_CNST_DEPRECATED, "func_cnst_deprecated.png");
	}
	
	private final static void declareRegistryImage(String key, String path) {
		ImageDescriptor desc = ImageDescriptor.getMissingImageDescriptor();
		try {
			desc = ImageDescriptor.createFromURL(new URL(ICON_BASE_URL, path));
		} catch (MalformedURLException me) {
			CSEditorPlugin.log(me);
		}
		imageRegistry.put(key, desc);
	}
		
}
