package fr.irisa.r2d2.gecos.framework.modules.timer;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Print the elapsed time since the last (re)start of a Timer.\n"
		+ "The timer continues i.e. it is not paused, restarted nor stoped.\n"
		+ "\nSee: 'TimerStart' and 'TimerRestart' modules.")
public class TimerStop {

	private Object ID;

	/**
	 * Instantiates a new timer stop.
	 *
	 * @param ID the id
	 */
	@GSModuleConstructor("-arg1: Integer ID associated with the timer.")
	public TimerStop(int ID) {
		this.ID = new Integer(ID);
	}
	
	/**
	 * Instantiates a new timer stop.
	 *
	 * @param ID the id
	 */
	@GSModuleConstructor("-arg1: String ID associated with the timer.")
	public TimerStop(String ID) {
		this.ID = ID;
	}
	
	/**
	 * Instantiates a new timer stop.
	 */
	@GSModuleConstructor("Use Default Timer ID (as Integer '0')")
	public TimerStop() {
		this(0);
	}
	
	/**
	 * Compute.
	 */
	public void compute() {
		TimerStart timer = TimerStart.getTimer(this.ID);
		if (timer == null) {
			System.err.println("No timer "+ID+"");
		} else {
			long t = System.currentTimeMillis() - timer.getTimestamp();
			System.out.println("Timer "+this.ID+" : "+t+"ms.");
		}
	}
}
