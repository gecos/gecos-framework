package fr.irisa.r2d2.gecos.framework.modules;

import java.io.File;
import java.io.FilenameFilter;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;


@GSModule("Recursively remove a directory (or a file).\n")
public class DirectoryCleaner {

	private File directory;
	private String extension;


	/**
	 * Instantiates a new directory cleaner.
	 *
	 * @param directory the directory
	 * @param extension the extension
	 */
	@GSModuleConstructor(
			"-arg1: directory path."
		  + "\n-arg2: suffix filter.\n"
		  + "   All files/dir ending with this suffix will be removed.")
	public DirectoryCleaner(String directory, String extension) {
		this.directory = new File(directory);
		this.extension = extension;
	}
	
	/**
	 * Instantiates a new directory cleaner.
	 *
	 * @param directory the directory
	 */
	public DirectoryCleaner(String directory) {
		this.directory = new File(directory);
	}

	/**
	 * Compute.
	 */
	public void compute() {
		deleteDirectory(directory, false);
	}

	/**
	 * Delete directory.
	 *
	 * @param path the path
	 * @param deletePath the delete path
	 * @return true, if successful
	 */
	private boolean deleteDirectory(File path, boolean deletePath) {
		boolean resultat = true;

		if (path.exists()) {
			File[] files;
			if (extension != null)
				files = path.listFiles(new ExtensionFilter());
			else
				files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					resultat &= deleteDirectory(files[i], true);
				} else {
					resultat &= files[i].delete();
				}
			}
		}
		if (deletePath)
			resultat &= path.delete();
		return (resultat);
	}

	private class ExtensionFilter implements FilenameFilter {

		/* (non-Javadoc)
		 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
		 */
		public boolean accept(File dir, String name) {

			if (new File(dir, name).isDirectory()) {
				return false;
			}
			name = name.toLowerCase();
			return name.endsWith(extension);

		}

	}
}
