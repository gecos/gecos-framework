package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;

public class ScriptDocumentProvider extends TextFileDocumentProvider {
	
	static protected class ScriptDescriptionInfo extends FileInfo {
		public IScriptCompilationUnit cu;
	}
	
	protected FileInfo createEmptyFileInfo() {
		return new ScriptDescriptionInfo();
	}

	protected FileInfo createFileInfo(Object element) throws CoreException {
		ScriptDescriptionInfo info = (ScriptDescriptionInfo) super.createFileInfo(element);
		info.cu = createBurgDescription();
		return info;
	}
	
	protected IAnnotationModel createAnnotationModel(IFile file) {
		return new ScriptAnnotationModel(file);
	}

	private IScriptCompilationUnit createBurgDescription() {
		return new ScriptCompilationUnit();
	}

	public IScriptCompilationUnit getScriptCompilationUnit(IEditorInput editorInput) {
		ScriptDescriptionInfo info = (ScriptDescriptionInfo) getFileInfo(editorInput);
		return info.cu;
	}

}
