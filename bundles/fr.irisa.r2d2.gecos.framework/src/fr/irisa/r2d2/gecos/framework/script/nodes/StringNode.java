/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class StringNode extends Node {
	private String value;
	
	/**
	 * Instantiates a new string node.
	 *
	 * @param span the span
	 * @param value the value
	 */
	public StringNode(Span span, String value) {
		super(span);
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the string
	 */
	public String value() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asString()
	 */
	public StringNode asString() {
		return this;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject()
	 */
	public Object asObject() { return value; }
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asInteger()
	 */
	public Integer asInteger()  {
		try {
			return Integer.decode(value);
		} catch(NumberFormatException e) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asFloat()
	 */
	public Double asFloat() {
		try {
			return Double.valueOf(value);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#applyPlus(fr.irisa.r2d2.gecos.framework.script.nodes.Node)
	 */
	public Node applyPlus(Node right) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(value);
		buffer.append(right.asString().value());
		return new StringNode(span, buffer.toString());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asBoolean()
	 */
	public boolean asBoolean() {
		return value != null && !value.isEmpty();
	}
	
}