/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.rules.IPartitionTokenScanner;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.editors.text.templates.ContributionContextTypeRegistry;
import org.eclipse.ui.editors.text.templates.ContributionTemplateStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import fr.irisa.r2d2.gecos.cseditor.assist.CSContextType;
import fr.irisa.r2d2.gecos.cseditor.assist.CompositeModuleIndexer;
import fr.irisa.r2d2.gecos.cseditor.assist.WorkspaceModuleManager;
import fr.irisa.r2d2.gecos.cseditor.text.CSPartitionScanner;
import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;

/**
 * The main plugin class to be used in the desktop.
 */
public class CSEditorPlugin extends AbstractUIPlugin {

	private static final String TEMPLATES_KEY = "fr.irisa.r2d2.gecos.cseditor.templates";

	//The shared instance.
	private static CSEditorPlugin plugin;
	
	private IPartitionTokenScanner partitionScanner;
	private TemplateStore templateStore;
	private ContributionContextTypeRegistry contextTypeRegistry;
	private CompositeModuleIndexer moduleIndexer;
	
	/**
	 * The constructor.
	 */
	public CSEditorPlugin() {
		plugin = this;
		this.partitionScanner = new CSPartitionScanner();
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
		//WorkspaceModuleManager.getDefault().dispose();
	}

	/**
	 * Returns the shared instance.
	 */
	public static CSEditorPlugin getDefault() {
		return plugin;
	}
	
	public URL getInstallURL() {
		return getDefault().getBundle().getEntry("/"); //$NON-NLS-1$
	}
	
	public static String getPluginId() {
		return getDefault().getBundle().getSymbolicName();
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path.
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin("org.jcomp.cseditor", path);
	}

	public IPartitionTokenScanner getPartitionScanner() {
		return partitionScanner;
	}
    
	public TemplateStore getTemplateStore() {
		if (templateStore == null) {
			templateStore = new ContributionTemplateStore(
					getTemplateContextRegistry(), getPreferenceStore(), TEMPLATES_KEY);
			try {
				templateStore.load();
			} catch (IOException e) {
				log(e);
			}
		}
		return templateStore;
	}

	public ContextTypeRegistry getTemplateContextRegistry() {
		if (contextTypeRegistry == null) {
			contextTypeRegistry = new ContributionContextTypeRegistry();
			contextTypeRegistry.addContextType(new CSContextType());
		}
		return contextTypeRegistry;
	}
	
	public static void log(IStatus status) {
		getDefault().getLog().log(status);
	}
	
	public static void log(Throwable e) {
		log(new Status(IStatus.ERROR, getPluginId(), 0, "internal error", e)); 
	}
	
	public static Shell getActiveWorkbenchShell() {
		IWorkbenchWindow window = getActiveWorkbenchWindow();
		if (window != null) {
			return window.getShell();
		}
		return null;
	}
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		return getDefault().getWorkbench().getActiveWorkbenchWindow();
	}

	public static void write(Exception exception) {
		exception.printStackTrace();
	}

	public IModuleIndexer getModuleIndexer() {
		if (moduleIndexer == null) {
			moduleIndexer = new CompositeModuleIndexer();
			moduleIndexer.add(WorkspaceModuleManager.getDefault());
			moduleIndexer.add(GecosFramework.getModuleIndexer());
		}
		return moduleIndexer;
	}
    
}
