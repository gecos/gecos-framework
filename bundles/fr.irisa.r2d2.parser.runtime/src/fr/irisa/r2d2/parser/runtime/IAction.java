package fr.irisa.r2d2.parser.runtime;

public interface IAction {

	boolean execute();
}
