/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.script;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.irisa.r2d2.gecos.framework.script.IFunctionFactory;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;

public abstract class AbstractFunctionFactory implements IFunctionFactory {

	private String name;
	private IConfigurationElement element;
	private String description;
	private IScriptEvaluable function;

	/**
	 * Instantiates a new abstract function factory.
	 *
	 * @param name the name
	 * @param element the element
	 */
	public AbstractFunctionFactory(String name, IConfigurationElement element) {
		this.name = name;
		this.element = element;
		this.function = null;
		getDescription(element);
	}
	
	/**
	 * Gets the element.
	 *
	 * @return the element
	 */
	protected IConfigurationElement getElement() {
		return element;
	}

	/**
	 * Gets the description.
	 *
	 * @param element the element
	 * @return the description
	 */
	private void getDescription(IConfigurationElement element) {
		description = "<none>";
		IConfigurationElement[] desc = element.getChildren("description");
		if (desc.length > 0)
			description = desc[0].getValue();
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getFunction()
	 */
	public IScriptEvaluable getFunction() throws ScriptException {
		if (function == null)
			function = createFunction();
		return function;
	}
	
	/**
	 * Creates a new AbstractFunction object.
	 *
	 * @return the i script evaluable
	 * @throws ScriptException the script exception
	 */
	protected abstract IScriptEvaluable createFunction() throws ScriptException;

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getName()
	 */
	public String getName() {
		return name;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getDescription()
	 */
	public String getDescription() {
		return description;
	}
	
}
