


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptMinus extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode expr;
	
	/**
	 * @generated
	 */
	public ScriptNode getExpr() {
		return expr;
	}
	
	/**
	 * @generated
	 */
	public ScriptMinus(Span span, ScriptNode expr) {
		super(span);
		this.expr = expr;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptMinus(this);
	}
}