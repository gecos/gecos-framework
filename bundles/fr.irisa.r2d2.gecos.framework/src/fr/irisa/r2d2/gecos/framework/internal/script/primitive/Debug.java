package fr.irisa.r2d2.gecos.framework.internal.script.primitive;

import fr.irisa.r2d2.gecos.framework.internal.GecosInternalFramework;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.parser.runtime.Span;

public class Debug implements IScriptEvaluable {

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {
		int status = 0;
		if (args.length > 0)
			status = args[0].asInteger();
		GecosInternalFramework.setDebugMode(status);
		return new NullNode(span);
	}

}
