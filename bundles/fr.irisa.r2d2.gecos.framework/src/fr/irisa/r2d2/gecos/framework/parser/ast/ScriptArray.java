


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
@SuppressWarnings("all")
public class ScriptArray extends ScriptNode {

	/**
	 * @generated
	 */
	protected AstList<ScriptNode> elements ;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptArray(Span span, AstList elements) {
		super(span);
		this.elements = elements;
	}
	
	public AstList<ScriptNode> getElements() {
		return elements;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptArray(this);
	}
	
}