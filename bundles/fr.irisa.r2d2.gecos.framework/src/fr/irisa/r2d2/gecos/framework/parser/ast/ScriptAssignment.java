


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptAssignment extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode expr;
	
	/**
	 * @generated
	 */
	private ScriptIdent variable;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptAssignment(Span span, ScriptIdent var, ScriptNode expr) {
		super(span);
		this.variable = var;
		this.expr = expr;
	}
	
	/**
	 * @generated
	 */
	public ScriptIdent getVariable() {
		return variable;
	}
	
	public ScriptNode getExpression() {
		return expr;
	}
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptAssignment(this);
	}
	
}