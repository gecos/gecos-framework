package fr.irisa.r2d2.gecos.framework.modules.timer;

import java.util.HashMap;
import java.util.Map;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Register and start a new Timer.\n"
		+ "\nSee: 'TimerStop' and 'TimerRestart' modules.")
public class TimerStart {
	
	private static Map<Object,TimerStart> timers = new HashMap<>();
	
	/**
	 * Gets the timer.
	 *
	 * @param ID the id
	 * @return the timer
	 */
	public static TimerStart getTimer(Object ID) {
		return timers.get(ID);
	}

	private long timestamp;
	private Object ID;

	
	/**
	 * Instantiates a new timer start.
	 *
	 * @param ID the id
	 */
	@GSModuleConstructor("-arg1: Integer ID associated with the timer.")
	public TimerStart(int ID) {
		this.ID = new Integer(ID);
	}
	
	/**
	 * Instantiates a new timer start.
	 *
	 * @param ID the id
	 */
	@GSModuleConstructor("-arg1: String ID associated with the timer.")
	public TimerStart(String ID) {
		this.ID = ID;
	}
	
	/**
	 * Instantiates a new timer start.
	 */
	@GSModuleConstructor("Use Default Timer ID (as Integer '0')")
	public TimerStart() {
		this(0);
	}
	
	/**
	 * Compute.
	 */
	public void compute() {
		timers.put(this.ID, this);
		this.timestamp = System.currentTimeMillis();
		System.out.println("Timer "+ID+" : Started.");
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return this.timestamp;
	}
}
