/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework;

import java.util.Properties;

public interface IConfiguration {
	
	/**
	 * Run.
	 *
	 * @param properties the properties
	 * @return the int
	 */
	int run(Properties properties);
	
	/**
	 * Configure options.
	 *
	 * @param manager the manager
	 */
	void configureOptions(IOptionsManager manager);

}
