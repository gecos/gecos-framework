<?php

require_once('/usr/share/dokuwiki/inc/init.php');
require_once(DOKU_INC.'inc/parserutils.php');

$id = 'gecos:fonctions';
$file = wikiFN($id, '');
$instructions = p_get_instructions(io_readfile($file));
$out = fopen('doc.xml', 'w') or die("can't open output file");
fwrite($out, "<?xml version='1.0'?>\n");
fwrite($out, "<doc>\n");
fwrite($out, p_render('xhtml', $instructions, $info));
fwrite($out, "</doc>\n");
