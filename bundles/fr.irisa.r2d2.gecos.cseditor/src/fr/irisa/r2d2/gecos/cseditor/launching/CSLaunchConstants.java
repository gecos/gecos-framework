/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.launching;

public interface CSLaunchConstants {

	public static final String WORKING_DIR = "fr.irisa.r2d2.gecos.cseditor.launching.WORKING_DIR";
	public static final String SCRIPT_FILE = "fr.irisa.r2d2.gecos.cseditor.launching.SCRIPT_FILE";
	public static final String SCRIPT_ARGS = "fr.irisa.r2d2.gecos.cseditor.launching.SCRIPT_ARGS";
	public static final String CONFIG_LOCATION = "fr.irisa.r2d2.gecos.cseditor.launching.CONFIG_LOCATION";
	
}
