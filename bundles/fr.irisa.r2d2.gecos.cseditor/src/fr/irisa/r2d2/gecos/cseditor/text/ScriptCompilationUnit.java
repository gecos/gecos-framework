package fr.irisa.r2d2.gecos.cseditor.text;

import java.io.StringReader;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;
import fr.irisa.r2d2.gecos.cseditor.assist.CompleteSemantic;
import fr.irisa.r2d2.gecos.framework.parser.GecosLexer;
import fr.irisa.r2d2.gecos.framework.parser.GecosParser;
import fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNode;
import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.IErrorReporter;
import fr.irisa.r2d2.parser.runtime.NullErrorReporter;

public class ScriptCompilationUnit implements IScriptCompilationUnit {

	private AstList<ScriptNode> statements;

	@SuppressWarnings("unchecked")
	public void reconcile(String source, IErrorReporter reporter) {
		try {
			GecosParser parser = new GecosParser(
					new GecosLexer(new StringReader(source)), reporter);
			statements = (AstList<ScriptNode>) parser.parse().value;
			SemanticAnalyser analyser = new SemanticAnalyser(reporter, CSEditorPlugin.getDefault().getModuleIndexer());
			analyser.analyse(statements);
		} catch (Exception e) {
			// ignore exceptions
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void completeAt(String source, int offset) {
		NullErrorReporter reporter = new NullErrorReporter();
		try {
			GecosParser parser = new GecosParser(
					new GecosLexer(new StringReader(source)), reporter);
			statements = (AstList<ScriptNode>) parser.parse().value;
			CompleteSemantic analyser = new CompleteSemantic(reporter, CSEditorPlugin.getDefault().getModuleIndexer());
			analyser.completeAt(statements, offset);
		} catch (Exception e) {
			// ignore exceptions
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
