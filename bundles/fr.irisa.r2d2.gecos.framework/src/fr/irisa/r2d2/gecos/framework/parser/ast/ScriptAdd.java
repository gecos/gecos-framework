


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptAdd extends ScriptBinary {

	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptAdd(Span span, ScriptNode left, ScriptNode right) {
		super(span);
		this.left = left;
		this.right = right;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptAdd(this);
	}

}