package fr.irisa.r2d2.gecos.framework.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;

import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.parser.runtime.DefaultErrorReporter;

public class GecosScriptLauncher {

	/**
	 * Runs the script file using the same GecosApplication and without changing
	 * the working directory.
	 *
	 * @param scriptFile the script file
	 * @param args the args
	 * @return true, if successful
	 */
	public static boolean launch(String scriptFile, String... args) {
		return new GecosScriptLauncher().launchScript(scriptFile, args);
	}
	
//	/**
//	 * Runs the script file. Equivalent to
//	 * {@link GecosScriptLauncher#launch(String, boolean, boolean, String...)}
//	 * where both booleans are set to newEnvironment.
//	 * 
//	 * @param scriptFile
//	 *            the Compiler Script to run
//	 * @param newEnvironment
//	 *            whether the script should be run in a new application using
//	 *            the script parent directory
//	 * @param args
//	 *            script arguments
//	 * @return
//	 */
//	public static boolean launch(String scriptFile, boolean newEnvironment, String... args) {
//		return new GecosScriptLauncher().launchScript(scriptFile, newEnvironment, args);
//	}

	/**
	 * Runs a Gecos Compiler Script file.
	 *
	 * @param scriptFile            the Compiler Script to run
	 * @param newApplication            whether the script should be run in a new application
	 * @param changeWorkingDirectory            whether to use the current working directory (false) or use
	 *            the script parent directory (true)
	 * @param args            script arguments
	 * @return true, if successful
	 */
	public static boolean launch(String scriptFile, boolean newApplication, boolean changeWorkingDirectory, String... args) {
		return new GecosScriptLauncher().launchScript(scriptFile ,newApplication, changeWorkingDirectory, args);
	}
	
	/**
	 * Runs a Gecos Compiler Script file.
	 *
	 * @param scriptFile            the Compiler Script to run
	 * @param newApplication            whether the script should be run in a new application
	 * @param newWorkingDirectory            the working directory for the script execution, or null to
	 *            keep the same
	 * @param args            script arguments
	 * @return true, if successful
	 */
	public static boolean launch(String scriptFile, boolean newApplication, String newWorkingDirectory, String... args) {
		return new GecosScriptLauncher().launchScript(scriptFile, newApplication, newWorkingDirectory, args);
	}
	
	
	
	/**
	 * Instantiates a new gecos script launcher.
	 */
	private GecosScriptLauncher() {
		reporter = new DefaultErrorReporter(GecosFramework.err);
		dirs = new Stack<>();
	}

	private Stack<String> dirs;
	private DefaultErrorReporter reporter;

	/**
	 * Runs the script file using the same GecosApplication and without changing
	 * the working directory.
	 *
	 * @param scriptFile the script file
	 * @param args the args
	 * @return true, if successful
	 */
	private boolean launchScript(String scriptFile, String... args) {
		return launchScript(scriptFile, false, false, args);
	}

//	/**
//	 * Runs the script file. Equivalent to
//	 * {@link GecosScriptLauncher#launchScript(String, boolean, boolean, String...)}
//	 * where both booleans are set to newEnvironment.
//	 * 
//	 * @param scriptFile
//	 *            the Compiler Script to run
//	 * @param newEnvironment
//	 *            whether the script should be run in a new application using
//	 *            the script parent directory
//	 * @param args
//	 *            script arguments
//	 * @return
//	 */
//	private boolean launchScript(String scriptFile,boolean newEnvironment, String... args) {
//		return launchScript(scriptFile,newEnvironment, newEnvironment, args);
//	}
	
	/**
	 * Runs a Gecos Compiler Script file.
	 *
	 * @param scriptFile            the Compiler Script to run
	 * @param newApplication            whether the script should be run in a new application
	 * @param changeWorkingDirectory            whether to use the current working directory (false) or use the script parent directory (true)
	 * @param args            script arguments
	 * @return true, if successful
	 */
	private boolean launchScript(String scriptFile, boolean newApplication, boolean changeWorkingDirectory, String... args) {
		if (!changeWorkingDirectory)
			return launchScript(scriptFile, newApplication, null, args);
		else {
			File script = new File(scriptFile);
			String newWorkingDir = script.getParentFile().getAbsolutePath();
			return launchScript(scriptFile, newApplication, newWorkingDir, args);
		}
	}
	
	/**
	 * Runs a Gecos Compiler Script file.
	 *
	 * @param scriptFile            the Compiler Script to run
	 * @param newApplication            whether the script should be run in a new application
	 * @param newWorkingDirectory            the working directory for the script execution, or null to
	 *            keep the same.
	 * @param args            script arguments
	 * @return true, if successful
	 */
	private boolean launchScript(String scriptFile, boolean newApplication, String newWorkingDirectory, String... args) {
		// check args
		File file = new File("."+File.separator+scriptFile).getAbsoluteFile();
		String workingDirectory = System.getProperty("user.dir");
		boolean changeWorkingDirectory = newWorkingDirectory != null;
		if (changeWorkingDirectory) {
			File f = new File(newWorkingDirectory);
			if (!(f.exists()) || !(f.isDirectory()))
				throw new IllegalArgumentException();
			workingDirectory = newWorkingDirectory;
		} else {
			
		}
		
		
		//run script
		boolean ranWithErrors = false;
		if (!newApplication) {
			if (changeWorkingDirectory)
				pushCWD(workingDirectory);
			try {
				ranWithErrors = GecosFramework.evaluate(scriptFile,	new FileReader(file), args, reporter);
			} catch (Exception e) {
				ranWithErrors = true;
			}
			if (changeWorkingDirectory)
				popCWD();
		} else {
			try {
				ILaunchConfiguration lc = GecosApplicationConfiguration.createLaunchConfiguration(scriptFile, args);
				ILaunchConfigurationWorkingCopy wc = lc.getWorkingCopy();
				wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_WORKING_DIRECTORY, workingDirectory);
				lc = wc.doSave();

				final File handle = GecosFramework.getRunHandler(file);
				if (!handle.exists())
					try {
						handle.createNewFile();
					} catch (IOException e) { }
				DebugUITools.launch(lc, ILaunchManager.RUN_MODE);
				ExecutionHandler t = new ExecutionHandler(handle);
				t.start();
				while (t.isRunning()) {
					try {
						Thread.sleep(300);
					} catch (Exception e) {}
				}
				
			} catch (Exception e) {
				ranWithErrors = true;
			}
		}
		
		// report errors
		if (ranWithErrors) {
			try {
				reporter.printDiagnostic(scriptFile);
			} catch (IOException e1) {
			}
			return false;
		}
		try {
			reporter.printDiagnostic(scriptFile);
		} catch (IOException e1) {
		}
		return true;
	}

	/**
	 * Pop CWD.
	 */
	private void popCWD() {
		if (!setCurrentDirectory(dirs.pop()))
			GecosFramework.err.println("Could not change working directory, using current one.");
	}

	/**
	 * Push CWD.
	 *
	 * @param absolutePath the absolute path
	 */
	private void pushCWD(String absolutePath) {
		dirs.push(System.getProperty("user.dir"));
		if (!setCurrentDirectory(absolutePath))
			GecosFramework.err.println("Could not change working directory, using current one.");
	}

	/**
	 * Sets the current directory.
	 *
	 * @param directory_name the directory name
	 * @return true, if successful
	 */
	private static boolean setCurrentDirectory(String directory_name) {
		if (directory_name.compareTo(System.getProperty("user.dir")) == 0)
			return true;
        boolean result = false;  // Boolean indicating whether directory was set
        File    directory;       // Desired current working directory
        directory = new File(directory_name).getAbsoluteFile();
        if (directory.exists() || directory.mkdirs()) {
            result = (System.setProperty("user.dir", directory.getAbsolutePath()) != null);
        }
        return result;
    }
	
	
	private class ExecutionHandler extends Thread {
		private File handle;
		
		/**
		 * Instantiates a new execution handler.
		 *
		 * @param handle the handle
		 */
		ExecutionHandler (File handle) {
			this.handle = handle;
		}
		
		private boolean running = true;
		
		/**
		 * Checks if is running.
		 *
		 * @return true, if is running
		 */
		public boolean isRunning() { return running; }
		
		/* (non-Javadoc)
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			while (handle.exists()) {
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) { }
			}
			running = false;
		}
	}
	
}
