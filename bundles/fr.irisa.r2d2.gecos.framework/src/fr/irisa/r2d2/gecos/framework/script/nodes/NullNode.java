/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class NullNode extends Node {

	/**
	 * Instantiates a new null node.
	 *
	 * @param span the span
	 */
	public NullNode(Span span) {
		super(span);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "";
	}
}