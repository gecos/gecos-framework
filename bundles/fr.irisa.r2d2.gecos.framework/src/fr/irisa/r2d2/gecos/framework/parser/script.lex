package fr.irisa.r2d2.gecos.framework.parser;

import java_cup.runtime.*;
import java.io.*;

import fr.irisa.r2d2.parser.runtime.IErrorReporter;

%%

%cupsym IGecosTokens
%cup
%class GecosLexer
%public 
%char 

DIGIT		= [0-9]
HEXDIGIT	= [0-9a-fA-F]
HEXINT		= 0x{HEXDIGIT}+
INT			= {DIGIT}+
EXPO		= [Ee][+-]?{DIGIT}+
FLT1		= {DIGIT}+{EXPO}
FLT2		= {DIGIT}*"."{DIGIT}+({EXPO})?
FLT3		= {DIGIT}+"."{DIGIT}*({EXPO})?
FLT			= {FLT1}|{FLT2}|{FLT3}

LETTER		= [a-zA-Z_]
IDENT		= {LETTER}({LETTER}|{DIGIT})*

%{
	private StringBuffer string = new StringBuffer();
	private int tokenStart;
	private IErrorReporter reporter;
	
	public GecosLexer(Reader reader, IErrorReporter reporter) {
		this(reader);
		this.reporter = reporter;
	}
	
	public Symbol symbol(int kind) {
		return new Symbol(kind, yychar, yychar+yylength());
	}
	
	public Symbol symbol(int kind, Object value) {
		return new Symbol(kind, yychar, yychar+yylength(), value);
	}
	
	public Symbol symbol(int start, int kind, Object value) {
		return new Symbol(kind, start, yychar+yylength(), value);
	}

%}

%state STR

%%

<YYINITIAL> {

#.*\n*				{ /* ignore comments */ }
[ \t\f\r\b\n]+		{ /* ignore white spaces */ }


"true"		{ return symbol(IGecosTokens.TRUE); }
"false"		{ return symbol(IGecosTokens.FALSE); }

"for"		{ return symbol(IGecosTokens.FOR); }
"in"		{ return symbol(IGecosTokens.IN); }
"do"		{ return symbol(IGecosTokens.DO); }
"done"		{ return symbol(IGecosTokens.DONE); }
"while"		{ return symbol(IGecosTokens.WHILE); }
"changing"	{ return symbol(IGecosTokens.CHANGING); }

"if"		{ return symbol(IGecosTokens.IF); }
"then"		{ return symbol(IGecosTokens.THEN); }
"else"		{ return symbol(IGecosTokens.ELSE); }
"fi"		{ return symbol(IGecosTokens.FI); }

"nil"		{ return symbol(IGecosTokens.NIL); }
"function"	{ return symbol(IGecosTokens.FUNCTION); }
"return"	{ return symbol(IGecosTokens.RETURN); }
"import"	{ return symbol(IGecosTokens.IMPORT); }

"->"		{ return symbol(IGecosTokens.ARROW); }
"("			{ return symbol(IGecosTokens.LPAR); }
")"			{ return symbol(IGecosTokens.RPAR); }
"["			{ return symbol(IGecosTokens.LBRT); }
"]"			{ return symbol(IGecosTokens.RBRT); }
"{"			{ return symbol(IGecosTokens.LBRC); }
"}"			{ return symbol(IGecosTokens.RBRC); }
"="			{ return symbol(IGecosTokens.EGAL); }
";"			{ return symbol(IGecosTokens.SEMI); }
","			{ return symbol(IGecosTokens.COMA); }
"+"			{ return symbol(IGecosTokens.PLUS); }
"-"			{ return symbol(IGecosTokens.MINUS); }

{IDENT}		{ return symbol(IGecosTokens.IDENT, yytext()); }
\${INT}		{ return symbol(IGecosTokens.ARGUMENT,
					Integer.decode(yytext().substring(1))); }
{HEXINT}	{ return symbol(IGecosTokens.INT, Integer.decode(yytext())); }
{INT}		{ return symbol(IGecosTokens.INT, Integer.decode(yytext())); }
{FLT}		{ return symbol(IGecosTokens.FLOAT, Double.parseDouble(yytext())); }
\"			{ tokenStart = yychar; string.setLength(0); yybegin(STR); }

.			{ reporter.reportError(yychar, 1, "bad character \'"+yycharat(0)+"\'"); }
<<EOF>>		{ return symbol(IGecosTokens.EOF); }
}

<STR> {
\"					{ yybegin(YYINITIAL); return symbol(tokenStart, IGecosTokens.STRING, string.toString()); }
[^\"\\]+			{ string.append(yytext()); }
\\\"				{ string.append('\"'); }
\\					{ string.append('\\'); }
.					{ reporter.reportError(yychar, 1, "bad character \'"+yycharat(0)+"\'"); }
}
