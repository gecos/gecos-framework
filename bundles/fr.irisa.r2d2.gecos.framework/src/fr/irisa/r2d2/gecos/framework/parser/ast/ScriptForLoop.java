


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptForLoop extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode expr;
	
	/**
	 * @generated
	 */
	protected AstList<ScriptNode> statements ;
	
	/**
	 * @generated
	 */
	private ScriptIdent ident;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptForLoop(Span span, ScriptIdent index, ScriptNode expr, AstList<ScriptNode> statements) {
		super(span);
		this.ident = index;
		this.expr = expr;
		this.statements = statements;
	}
	
	/**
	 * @generated
	 */
	public ScriptIdent getIdent() {
		return ident;
	}
	
	public AstList<ScriptNode> getStatements() {
		return statements;
	}
	
	public ScriptNode getExpression() {
		return expr;
	}
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptForLoop(this);
	}

}