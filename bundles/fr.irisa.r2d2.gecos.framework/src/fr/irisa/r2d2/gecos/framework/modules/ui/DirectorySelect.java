package fr.irisa.r2d2.gecos.framework.modules.ui;


import java.io.File;

import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class DirectorySelect {

	String path = "./";
	
	/**
	 * Instantiates a new directory select.
	 */
	public DirectorySelect() {
	}

	/**
	 * Instantiates a new directory select.
	 *
	 * @param apath the apath
	 */
	public DirectorySelect(String apath) {
		path=apath;
	}
	
	
	/**
	 * Compute.
	 *
	 * @return the file
	 */
	public File compute() {
	    Display d=new Display();;
	    Shell s =  new Shell(d);;
	    DirectoryDialog dlg = new DirectoryDialog(s);
	    dlg .setText("Select");
	    dlg .setFilterPath(path);
        String selected = dlg .open();
        d.close();
		return new File(selected);
	}
}
