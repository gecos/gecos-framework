


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptInteger extends ScriptNode {

	/**
	 * @generated
	 */
	private Integer value;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptInteger(Span span, Integer value) {
		super(span);
		this.value = value;
	}
	
	/**
	 * @generated
	 */
	public Integer getValue() {
		return value;
	}
	
	/**
	 * @generated
	 */
	public void setValue(Integer value) {
		this.value = value;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptInteger(this);
	}
}