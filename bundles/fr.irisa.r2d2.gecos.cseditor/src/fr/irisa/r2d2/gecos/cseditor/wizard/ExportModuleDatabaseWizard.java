package fr.irisa.r2d2.gecos.cseditor.wizard;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;
import fr.irisa.r2d2.gecos.cseditor.CSImages;
import fr.irisa.r2d2.gecos.cseditor.assist.WorkspaceModuleManager;
import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleArgument;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;

public class ExportModuleDatabaseWizard extends Wizard implements IExportWizard {

	private IStructuredSelection selection;
	private ExportDatabaseMainPage mainPage;
	private WizardNewFileCreationPage locationPage;

	public ExportModuleDatabaseWizard() {
		setNeedsProgressMonitor(true);
		setWindowTitle("Export Module Database"); 
	}
	
	public void addPages() {
		mainPage = new ExportDatabaseMainPage("projectSetMainPage",
				"Export Module Database", CSImages.getDescriptor(CSImages.IMG_MODULES_EXPORT_BANNER)); //$NON-NLS-1$ 
		addPage(mainPage);
		@SuppressWarnings("unchecked")
		IProject[] projects = (IProject[])selection.toList().toArray(new IProject[0]);
		mainPage.setSelectedProjects(projects);
		locationPage = new WizardNewFileCreationPage("projectSetLocationPage", null); //$NON-NLS-1$
		locationPage.setTitle("Export Module Database");
		locationPage.setImageDescriptor(CSImages.getDescriptor(CSImages.IMG_MODULES_EXPORT_BANNER));
		locationPage.setDescription("Choose the file where the module database will be exported to");
		locationPage.setFileExtension("xml");
		locationPage.setFileName("dump.xml");
		locationPage.setContainerFullPath(new Path("/"));
		addPage(locationPage);
	}

	@Override
	public boolean performFinish() {
		IFile file = locationPage.createNewFile();
        if (file == null) {
			return false;
		}
        save(file, getContents());
		return true;
	}
	
	private String getContents() {
		WorkspaceModuleManager manager = WorkspaceModuleManager.getDefault();
		StringWriter writer = new StringWriter();
		PrintWriter out = new PrintWriter(writer);
		out.println("<modules>");
		for(IProject project : mainPage.getSelectedProjects()) {
			for (IModule module : manager.getModules(project)) {
				out.print("<module ");
				out.print("name=\"" + module.getName() + "\" ");
				out.print("packageName=\"" + module.getPackageName() + "\" ");
				out.print("className=\"" + module.getClassName() + "\" ");
				if (module.getReturnType() != null)
					out.print("returnType=\"" + module.getReturnType() + "\"");
				out.println(">");
				for (IModuleConstructor cnst : module.getConstructors()) {
					out.println("<constructor> ");
					for (IModuleArgument arg : cnst.getArguments()) {
						out.println("<arg name=\""+arg.name()+"\" type=\""+arg.typeSignature()+"\"/> ");
					}
					out.println("</constructor>");
				}
				out.println("<description>" + module.getDescription() + "</description>");
				out.println("</module>");
			}
		}
		out.println("</modules>");
		out.flush();
		return writer.toString();
	}
	
	private void save(IFile file, String contents) {
		try {
			ByteArrayInputStream stream =
				new ByteArrayInputStream(contents.getBytes("UTF8")); //$NON-NLS-1$
			if (file.exists()) {
				file.setContents(stream, false, false, null);
			} else {
				file.create(stream, false, null);
			}
			stream.close();
		} catch (CoreException e) {
			CSEditorPlugin.log(e);
		} catch (IOException e) {
		}
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}

}
