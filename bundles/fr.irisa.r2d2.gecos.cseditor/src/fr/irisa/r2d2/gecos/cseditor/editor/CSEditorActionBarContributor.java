/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.editor;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.texteditor.BasicTextEditorActionContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.RetargetTextEditorAction;

import fr.irisa.r2d2.gecos.cseditor.CSMessages;

/**
 * This contributor is shared between every CSEditors. It makes
 * the connection between the active editor and the global actions.
 *  
 * @author llhours
 *
 */
public class CSEditorActionBarContributor extends
		BasicTextEditorActionContributor {

	private RetargetTextEditorAction contentAssistAction; 
    private RetargetTextEditorAction openDefinitionAction;
    private RetargetTextEditorAction toggleCommentAction;
    
    public CSEditorActionBarContributor() {
		contentAssistAction = new RetargetTextEditorAction(CSMessages
				.getResourceBundle(), "CSEditor.contentAssistProposal."); //$NON-NLS-1$
		openDefinitionAction = new RetargetTextEditorAction(CSMessages
				.getResourceBundle(), "CSEditor.openDefinition."); //$NON-NLS-1$;
		toggleCommentAction = new RetargetTextEditorAction(CSMessages
				.getResourceBundle(), "CSEditor.toggleComment."); //$NON-NLS-1$;
	}

	public void contributeToMenu(IMenuManager menu) {
        super.contributeToMenu(menu);
        IMenuManager editMenu = menu.findMenuUsingPath(
            IWorkbenchActionConstants.M_EDIT);
        if (editMenu != null) {
            editMenu.add(new Separator());
            editMenu.add(this.contentAssistAction);
        }
        IMenuManager navigateMenu= menu.findMenuUsingPath(
        		IWorkbenchActionConstants.M_NAVIGATE);
        if (navigateMenu != null) {
            navigateMenu.appendToGroup(IWorkbenchActionConstants.OPEN_EXT,
            		openDefinitionAction);
        }
    }
	
	public void setActiveEditor(IEditorPart part) {
        super.setActiveEditor(part);
        ITextEditor editor = null;
        if (part instanceof ITextEditor) {
            editor = (ITextEditor) part;
        }
        contentAssistAction.setAction(
                getAction(editor, ICSEditorActionConstants.CONTENT_ASSIST));
        openDefinitionAction.setAction(
                getAction(editor, ICSEditorActionConstants.OPEN_DEFINITION));
        toggleCommentAction.setAction(
                getAction(editor, ICSEditorActionConstants.TOGGLE_COMMENT));
    }
   
}
