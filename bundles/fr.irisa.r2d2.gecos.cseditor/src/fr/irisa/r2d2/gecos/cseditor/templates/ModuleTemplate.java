/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.templates;

import java.net.URL;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.pde.core.plugin.IMatchRules;
import org.eclipse.pde.core.plugin.IPluginBase;
import org.eclipse.pde.core.plugin.IPluginElement;
import org.eclipse.pde.core.plugin.IPluginExtension;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.core.plugin.IPluginModelFactory;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.IFieldData;
import org.eclipse.pde.ui.templates.OptionTemplateSection;
import org.eclipse.pde.ui.templates.TemplateOption;
import org.osgi.framework.Bundle;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;

/**
 * @author llhours
 *
 */
public class ModuleTemplate extends OptionTemplateSection {
	
	private static final String KEY_CLASS_NAME = "className"; //$NON-NLS-1$
	private static final String KEY_MODULE_NAME = "moduleName"; //$NON-NLS-1$
	private static final String KEY_WITH_ARG = "withArgument"; //$NON-NLS-1$
	private static final String CLASS_NAME = "SampleModule"; //$NON-NLS-1$

	public ModuleTemplate() {
		setPageCount(1);
		createOptions();
	}
	
	private void createOptions() {
		addOption(KEY_PACKAGE_NAME, "Package name:", (String) null, 0);
		addOption(KEY_CLASS_NAME, "Class name:", CLASS_NAME, 0);
		addOption(KEY_MODULE_NAME, "Module name:", "Sample", 0);
		addOption(KEY_WITH_ARG, "with argument", true, 0);
	}
	
	protected URL getInstallURL() {
		return CSEditorPlugin.getDefault().getInstallURL();
	}
	
	/* XXX should be removed and templates directory
	 * changed according to the schema version of the plugin
	 */
	protected String getTemplateDirectory() {
		return "templates"; //$NON-NLS-1$
	}
	
	public String getSectionId() {
		return "module"; //$NON-NLS-1$
	}
	
	public void addPages(Wizard wizard) {
		WizardPage page = createPage(0, "Template Module");
		page.setTitle("Sample GECOS Module");
		page.setDescription("This template creates a sample Gecos script function.");
		wizard.addPage(page);
		markPagesAdded();
	}
	
	public boolean isDependentOnParentWizard() {
		return true;
	}
	
	protected void initializeFields(IFieldData data) {
		// In a new project wizard, we don't know this yet - the
		// model has not been created
		String packageName = getFormattedPackageName(data.getId());
		initializeOption(KEY_PACKAGE_NAME, packageName);
	}
	
	public void initializeFields(IPluginModelBase model) {
		String pluginId = model.getPluginBase().getId();
		initializeOption(KEY_PACKAGE_NAME, getFormattedPackageName(pluginId)); 
	}

	private String getFormattedPackageName(String name) {
		return name + ".modules";
	}

	public String getUsedExtensionPoint() {
		return "fr.irisa.r2d2.gecos.framework.modules"; //$NON-NLS-1$
	}
	
	public void validateOptions(TemplateOption source) {
		if (source.isRequired() && source.isEmpty()) {
			flagMissingRequiredOption(source);
		} else {
			//validateContainerPage(source);
		}
	}
	
	public String[] getNewFiles() {
		return new String[0];
	}
	
	public IPluginReference[] getDependencies(String schemaVersion) {
		return new IPluginReference[] { new PluginReference("fr.irisa.r2d2.gecos.framework") };
	}
	
	protected void updateModel(IProgressMonitor monitor) throws CoreException {
		IPluginBase plugin = model.getPluginBase();
		IPluginExtension extension = createExtension("fr.irisa.r2d2.gecos.framework.modules", true); //$NON-NLS-1$
		IPluginModelFactory factory = model.getPluginFactory();
		String fullClassName =
			getStringOption(KEY_PACKAGE_NAME) + "." + getStringOption(KEY_CLASS_NAME); //$NON-NLS-1$

		IPluginElement primitiveElement = factory.createElement(extension);
		primitiveElement.setName("module"); //$NON-NLS-1$
		primitiveElement.setAttribute("name", getStringOption(KEY_MODULE_NAME)); //$NON-NLS-1$
		primitiveElement.setAttribute("class", fullClassName);
		extension.add(primitiveElement);
		if (!extension.isInTheModel())
			plugin.add(extension);			
	}

	protected ResourceBundle getPluginResourceBundle() {
		Bundle bundle = Platform.getBundle(CSEditorPlugin.getPluginId());
		return Platform.getResourceBundle(bundle);
	}
	
	private static class PluginReference implements IPluginReference {

		private String id;
		private String version;
		private int match;

		public PluginReference(String id) {
			this.match = IMatchRules.NONE;
			this.id = id;
			this.version = null;
		}

		public int getMatch() {
			return match;
		}
		
		public void setMatch(int match) throws CoreException {
			this.match = match;
		}

		public String getVersion() {
			return version;
		}
		
		public void setVersion(String version) throws CoreException {
			this.version = version;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) throws CoreException {
			this.id = id;
		}
		
	}

}
