<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
		
	<!-- This stylesheet is used to build the type database,
		 which is used by other stylesheets. It could be
		 applyed on two kind of documents : the list of modules
		 and each Eclipse manifest (plugin.xml). -->
		
	<xsl:output method="xml" indent="yes"/>
	
	<!-- the base directory where every plugins are located, relative
		 to the directory containing this file -->
	<xsl:variable name="base">../../</xsl:variable>
	
	<!-- default rules no recursion -->
	<xsl:template match="*"/>
	
	 <xsl:template match="/">
	 	<types>
	 		<xsl:apply-templates select="modules/module"/>
	 	</types>
	 </xsl:template>
	
	<!-- Emit a transform tag for every module -->
	
	<xsl:template match="module">
		<transform name="{@name}" module="{@packageName}"/>
		<xsl:call-template name="makeArg">
			<xsl:with-param name="class" select="@returnType"/>
			<xsl:with-param name="name" select="@name"/>
			<xsl:with-param name="isInput">false</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="constructor/arg"/>
	</xsl:template>
	
	<!-- Emit a reference for every non trivial type -->
	
	<xsl:template match="arg">
		<xsl:call-template name="makeArg">
			<xsl:with-param name="class" select="@class"/>
			<xsl:with-param name="name" select="../@name"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="makeArg">
		<xsl:param name="isInput">true</xsl:param>
		<xsl:param name="class" />
		<xsl:param name="name" />
		<!-- Suppress trivial types -->
		<xsl:if test="$class != 'int' and $class != 'boolean' and $class != 'void' and not (starts-with($class, 'java.'))">
			<xsl:element name="ref">
				<xsl:attribute name="type"><xsl:value-of select="$class"/></xsl:attribute>
				<xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
				<xsl:if test="$isInput = 'true'">
					<xsl:attribute name="use">true</xsl:attribute>
				</xsl:if>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>
