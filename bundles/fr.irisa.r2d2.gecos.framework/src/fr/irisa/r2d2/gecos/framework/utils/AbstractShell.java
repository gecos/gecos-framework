package fr.irisa.r2d2.gecos.framework.utils;

import static java.util.stream.Collectors.joining;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.logging.Logger;

public abstract class AbstractShell {
	
	public static final PrintStream defaultStdout = System.out;
	public static final PrintStream defaultStderr = System.err;
	
	protected boolean quiet;
	protected boolean verbose;
	protected Logger logger = Logger.getLogger("SHELL");

	
	public boolean isQuiet() {
		return quiet;
	}

	public boolean isVerbose() {
		return verbose;
	}
	
	/**
	 * @param quiet if true, disable out/err outputs capturing of the command.
	 * Otherwise, the command out/err outputs are directed to the specified out/err streams.
	 * @return this
	 */
	public AbstractShell setQuiet(boolean quiet) {
		this.quiet = quiet;
		return this;
	}
	
	/**
	 * @param verbose if true, prints logging information on the logger
	 * @return this
	 */
	public AbstractShell setVerbose(boolean verbose) {
		this.verbose = verbose;
		return this;
	}
	
	/**
	 * @param logger used to log information if verbose mode is set
	 * @return this
	 */
	public AbstractShell setLogger(Logger logger) {
		this.logger = logger;
		return this;
	}

	public static PrintStream outToFile(File file) {
		if(file == null) return null;
		try {
			return new PrintStream(file);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static PrintStream outToFile(Path file) {
		if(file == null) return null;
		return outToFile(file.toFile());
	}
	
	public static PrintStream outToStream(OutputStream stream) {
		if(stream == null) return null;
		return new PrintStream(stream);
	}
	
	public static PrintStream outToByteArray(ByteArrayOutputStream byteArrayStream) {
		return outToStream(byteArrayStream);
	}
	
	
	public abstract int successExitCode();
	
	public abstract int timeoutExitCode();
	
	/**
	 * Try to locate the full path of the specified command.
	 *
	 * @param cmd the name of command to locate
	 * @return the command full path if found, {@code null} otherwise.
	 */
	public abstract String findProgram(String cmd);
	
	protected abstract String[] makeCommand(String[] cmdArray, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr);
	
	public int rawExec(String[] command, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		String cmdStr = Arrays.stream(command).collect(joining(" "));
		if (verbose) logger.info("[SHELL][INFO] Running command: " + cmdStr);
		int res;
		Process process = null;
		try {
			ProcessBuilder processBuilder = new ProcessBuilder(command);
			if(inDir != null)
				processBuilder.directory(inDir);
			process = processBuilder.start();
			
			StreamDrainer drainerStdout = null;
			StreamDrainer drainerStderr = null;
			if(!quiet && stdout != null) {
				drainerStdout = new StreamDrainer(process.getInputStream(), stdout);
				drainerStdout.start();
			}
			if(!quiet && stderr != null) {
				drainerStderr = new StreamDrainer(process.getErrorStream(), stderr);
				drainerStderr.start();
			}
			
			res = process.waitFor();
			
			if(drainerStdout != null) drainerStdout.join();
			if(drainerStderr != null) drainerStderr.join();
			
			if(res == timeoutExitCode())
				throw new TimeoutException("Timeout after " + timeoutSec + " seconds\n"
						+ "Command is killed: " + cmdStr);
			
			if (verbose) {
				if(res == successExitCode())
					logger.info("Command finished successfully : " + cmdStr);
				else
					logger.severe("Command failed with exit code '" + res + "' : " + cmdStr);
			}
			
		} catch (Exception e) {
			throw new RuntimeException("[SHELL][ERROR] Couldn't execute command: " + cmdStr, e);
		} finally {
 	 		if (stdout != null){
 	 	 		stdout.flush();
 	 	 		stdout.close();
 	 		}
 	 		if (stderr != null){
 	 	 		stderr.flush();
 	 	 		stderr.close();
 	 		}
 	 		if (process != null){
 	 			process.destroy();
 	 		}
 		}

		return res;
	}
	
	/**
	 * @param cmd a string array containing the program and its arguments
	 * @param inDir change directory to inDir before executing. If {@code null} executes
	 * in current directory.
	 * @param timeoutSec in seconds after which a 'kill' signal is sent to the process.
	 * If {@code null} no timeout is considered.
	 * @param stdout the cmd normal output will be directed to this stream if not {@code null}.
	 * @param stderr the cmd error output will be directed to this stream if not {@code null}.
	 * 
	 * @return exit code of the command.
	 * @throws TimeoutException if timeout.
	 */
	public int exec(String[] cmd, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		String[] command = makeCommand(cmd, inDir, timeoutSec, stdout, stderr);
		return rawExec(command, inDir, timeoutSec, stdout, stderr);
	}

	/**
	 * Shorthand to {@link #exec(String[], File, Long, PrintStream, PrintStream)} by converting the specified
	 * {@code cmd} to singleton array i.e. [cmd].
	 */
	public int shell(String cmd, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		return exec(new String[] { cmd }, inDir, timeoutSec, stdout, stderr);
	}
	
	/**
	 * Shorthand for {@link #shell(String, File, Long, PrintStream, PrintStream)} with no timeout.
	 */
	public int shell(String cmd, File inDir, PrintStream stdout, PrintStream stderr) {
		return shell(cmd, inDir, null, stdout, stderr);
	}
	
	/**
	 * Shorthand for {@link #shell(String, File, Long, PrintStream, PrintStream)} with no directory change.
	 */
	public int shell(String cmd, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		return shell(cmd, null, timeoutSec, stdout, stderr);
	}
	
	/**
	 * Shorthand for {@link #shell(String, File, Long, PrintStream, PrintStream)} with no timeout nor directory change.
	 */
	public int shell(String cmd, PrintStream stdout, PrintStream stderr) {
		return shell(cmd, null, null, stdout, stderr);
	}
	
	/**
	 * Shorthand for {@link #shell(String, File, Long, PrintStream, PrintStream)} with
	 * stdout/err redirected to {@link System#out} / {@link System#err} respectively. 
	 */
	public int shell(String cmd, File inDir, Long timeoutSec) {
		return shell(cmd, inDir, timeoutSec, defaultStdout, defaultStderr);
	}
	
	/**
	 * Shorthand for {@link #shell(String, File, Long, PrintStream, PrintStream)} with no timeout nor directory change
	 * and stdout/err redirected to {@link System#out} / {@link System#err} respectively. 
	 */
	public int shell(String cmd) {
		return shell(cmd, null, null, defaultStdout, defaultStderr);
	}
	
	
	protected static class StreamDrainer extends Thread {
		private BufferedReader stream;
		private PrintStream stdout;
		
		public StreamDrainer(InputStream in, PrintStream outStream) {
			if(in == null || outStream == null)
				throw new InvalidParameterException("Parameters cannot be null!");
			this.stream = new BufferedReader(new InputStreamReader(in));
			this.stdout = outStream;
		}
		
		@Override
		public void run() {
			try {
				String line = null;
	            while ( (line = stream.readLine()) != null) {
            		stdout.println(line);
            		stdout.flush();
	            }
			} catch (Exception e) {
				throw new RuntimeException("Problem while reading stream.",e);
			} finally {
				try {
					stream.close();
				} catch (IOException e) {
					throw new RuntimeException("Problem while closing stream.",e);
				}
			}
			
			stdout.flush();
			if(stdout != defaultStdout && stdout != defaultStderr)
				stdout.close();
		}
	}

}

