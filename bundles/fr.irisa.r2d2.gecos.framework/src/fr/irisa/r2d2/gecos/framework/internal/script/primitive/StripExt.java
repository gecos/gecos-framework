/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.script.primitive;


import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.StringNode;
import fr.irisa.r2d2.parser.runtime.Span;

public class StripExt implements IScriptEvaluable {

	/**
	 * Instantiates a new strip ext.
	 */
	public StripExt() { }

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args) {
		if (args.length != 1)
			throw new RuntimeException("stripext : expect one argument");
		StringNode arg = args[0].asString();
		String file = arg.value();
		int pos = file.lastIndexOf('.');
		if (pos < 0) return arg;
		return new StringNode(span, file.substring(0, pos));
	}

}
