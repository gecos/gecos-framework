/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework;

public interface IOptionsManager {

	static final int NO_ARG = 0;
	static final int HAS_ARG = 1;
	
	static final String OTHER_ARGUMENTS = "others";
	
	/**
	 * Adds the.
	 *
	 * @param option the option
	 * @param key the key
	 * @param type the type
	 * @param helpString the help string
	 */
	void add(String option, String key, int type, String helpString);

}
