package fr.irisa.r2d2.gecos.doc.ant;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExtractDocumentation extends Task {
	
	
	private static final String DOKU_BASE = "https://no-pasaran.irisa.fr";
	private static final String DOKU_PAGE = "/dokuwiki/doku.php?id=gecos:fonctions";
	
	private static TrustManager[] trustAll = new TrustManager[] {
		new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		}
	};
	
	/**
	 * Register a new trust manager to allow anonymous SSL connection.
	 */
	private static void registerTrustManager() {
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAll, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(
					sc.getSocketFactory());
		} catch (Exception e) {
		}
	}

	private DocumentBuilder documentBuilder;
	private XPath xpathProcessor;
	private TransformerFactory transformerFactory;
	
	private String dokuBase = DOKU_BASE;
	private String dokuPage = DOKU_PAGE;
	private File outFile  = new File("help.xml");
	
	/**
	 * Fetch and prepare every pages from the index map and
	 * save the result in <code>outFile</code>.
	 */
	public void execute() throws BuildException {
		registerTrustManager();
		try {
			String pageURL = dokuBase + dokuPage;
			System.out.println("Building map from " + pageURL);
			Map<String, String> linksMap = getLinksMap(pageURL);
			Transformer helpTransformer = getTransformer(new StreamSource(new File("makehelp.xsl")));
			Document helpDoc = getDocumentBuilder().newDocument();
			Element helpNode = helpDoc.createElement("help");
			helpDoc.appendChild(helpNode);
			for (String name : linksMap.keySet()) {
				System.out.println("Preprocessing page " + name);
				helpTransformer.transform(new StreamSource(new URL(dokuBase + linksMap.get(name)).openStream()),
						new DOMResult(helpNode));
			}
			saveDocument(helpDoc, outFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setBase(String base) {
		this.dokuBase = base;
	}
	
	public void setPage(String page) {
		this.dokuPage = page;
	}
	
	public void setOut(File outFile) {
		this.outFile = outFile;
	}
	
	/**
	 * Build a map from the links of the index pages.
	 * 
	 * @return
	 * @throws IOException
	 */
	public Map<String,String> getLinksMap(String pageURL) throws IOException {
		try {
			Document doc = getDocument(new URL(pageURL));
			getXPathProcessor().setNamespaceContext(new DocumentNamespaceContext());
			XPathExpression expr = getXPathProcessor().compile(
					"//xhtml:div[@class='li']/xhtml:a[@class='wikilink1']");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			Map<String, String> linksMap = new HashMap<String, String>();
			for (int i = 0; i < nodes.getLength(); i++) {
				Element node = (Element) nodes.item(i);
				String href = node.getAttribute("href");
				String name = href.substring(href.indexOf(':')+1);
				linksMap.put(name, href);
			}
			return linksMap;
		} catch (Exception e) {
			throw new IOException("cannot get linksMap: " + e.getMessage());
		}
	}
	
	public class DocumentNamespaceContext implements NamespaceContext {

		public String getNamespaceURI(String prefix) {
			 if (prefix == null)
				 throw new NullPointerException("Null prefix");
			 if ("xhtml".equals(prefix))
				 return "http://www.w3.org/1999/xhtml";
			 if ("xml".equals(prefix))
				 return XMLConstants.XML_NS_URI;
			 return XMLConstants.NULL_NS_URI;
		}

		public String getPrefix(String namespaceURI) {
			throw new UnsupportedOperationException();
		}

		public Iterator<?> getPrefixes(String namespaceURI) {
			throw new UnsupportedOperationException();
		}

	}
	
	private Transformer getTransformer(Source xsltSource) throws IOException {
		try {
			return getTransformerFactory().newTransformer(xsltSource);
		} catch (Exception e) {
			throw new IOException("cannot create transformer");
		}
	}
	
	private void saveDocument(Document doc, File file) {
		try {
			Transformer transformer = getTransformerFactory().newTransformer();
			transformer.transform(new DOMSource(doc), new StreamResult(file));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Document getDocument(URL url) throws IOException,
			ParserConfigurationException, SAXException {
		InputStream stream = url.openStream();
		Document doc = getDocument(stream);
		stream.close();
		return doc;
	}

	private Document getDocument(InputStream stream)
			throws IOException, ParserConfigurationException, SAXException {
		DocumentBuilder builder = getDocumentBuilder();
		Document doc = builder.parse(stream);
		return doc;
	}
	
	private TransformerFactory getTransformerFactory() {
		if (transformerFactory == null) {
			transformerFactory = TransformerFactory.newInstance();
		}
		return transformerFactory;
	}

	private DocumentBuilder getDocumentBuilder()
			throws ParserConfigurationException {
		if (documentBuilder == null) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true); // never forget this!
			documentBuilder = factory.newDocumentBuilder();
		}
		return documentBuilder;
	}
	
	private XPath getXPathProcessor() {
		if (xpathProcessor == null) {
			XPathFactory factory = XPathFactory.newInstance();
			xpathProcessor = factory.newXPath();
		}
		return xpathProcessor;
	}
}
