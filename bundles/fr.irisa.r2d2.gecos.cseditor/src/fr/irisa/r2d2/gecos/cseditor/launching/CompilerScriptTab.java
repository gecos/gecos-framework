/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.launching;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.StringVariableSelectionDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import fr.irisa.r2d2.gecos.cseditor.CSImages;

public class CompilerScriptTab extends AbstractLaunchConfigurationTab
		implements CSLaunchConstants {
	
	private Text scriptFile;
	private Button browseButton;
	private Text argumentsText;
	private WorkingDirectoryBlock workingDirectoryBlock;
	
	public CompilerScriptTab() {
		workingDirectoryBlock = new WorkingDirectoryBlock();
	}

	public String getName() {
		return "Compiler Script";
	}
	
	public Image getImage() {
		return CSImages.get(CSImages.IMG_CS_FILE);
	}
	
	private class WidgetListener implements ModifyListener, SelectionListener {
		public void modifyText(ModifyEvent e) {
			updateLaunchConfigurationDialog();
		}
		public void widgetSelected(SelectionEvent e) {
			Object source = e.getSource();
			if (source == browseButton) {
				handleProjectButtonSelected();
			} else {
				updateLaunchConfigurationDialog();
			}
		}
		public void widgetDefaultSelected(SelectionEvent e) {
		}
	}

	private WidgetListener listener = new WidgetListener();
	
	public void createControl(Composite parent) {
		Font font = parent.getFont();
		Composite comp = new Composite(parent, parent.getStyle());
		GridLayout layout = new GridLayout(1, true);
		comp.setLayout(layout);
		comp.setFont(font);
		GridData gd = new GridData(GridData.FILL_BOTH);
		comp.setLayoutData(gd);
		setControl(comp);
		
		createScriptFileControl(comp);
		createArgumentsControl(comp);
		
		workingDirectoryBlock.createControl(comp);
	}

	/**
	 * Create the script file box. It's composed of a
	 * text box (scriptFile) and a button (browseButton)
	 * 
	 * @param parent
	 */
	private void createScriptFileControl(Composite parent) {
		Font font = parent.getFont();
		
		Group group= new Group(parent, SWT.NONE);
		group.setText("Script File");
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		group.setLayout(layout);
		group.setFont(font);

		scriptFile = new Text(group, SWT.SINGLE | SWT.BORDER);
		scriptFile.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		scriptFile.setFont(font);
		scriptFile.addModifyListener(listener);
		
		browseButton = createPushButton(group, "Browse...", null); //$NON-NLS-1$
		browseButton.addSelectionListener(listener);
	}
	
	private void createArgumentsControl(Composite parent) {
		Font font = parent.getFont();

		Group group = new Group(parent, SWT.NONE);
		GridLayout topLayout = new GridLayout();
		group.setLayout(topLayout);	
		GridData gd = new GridData(GridData.FILL_BOTH);
		group.setLayoutData(gd);
		group.setFont(font);
		group.setText("Arguments"); //$NON-NLS-1$
		
		argumentsText = new Text(group, SWT.MULTI | SWT.WRAP| SWT.BORDER | SWT.V_SCROLL);
		gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 40;
		gd.widthHint = 100;
		argumentsText.setLayoutData(gd);
		argumentsText.setFont(font);
		argumentsText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent evt) {
				updateLaunchConfigurationDialog();
			}
		});
		
		Button argVariableButton = createPushButton(group, "Variables...", null);
		argVariableButton.setFont(font);
		argVariableButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		argVariableButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				StringVariableSelectionDialog dialog = new StringVariableSelectionDialog(getShell());
				dialog.open();
				String variable = dialog.getVariableExpression();
				if (variable != null) {
					argumentsText.insert(variable);
				}
			}
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
	}
	
	/**
	 * Called when the <code>browseButton</code> is pressed.
	 * It opens a selection dialog a on a tree viewer which
	 * only shows .cs files of the workspace.
	 *
	 */
	protected void handleProjectButtonSelected() {
		ElementTreeSelectionDialog diag = new ElementTreeSelectionDialog(getShell(),
				new WorkbenchLabelProvider(), new WorkbenchContentProvider() {
			public Object[] getChildren(Object o) {
				Object[] children = super.getChildren(o);
				List<Object> res = new ArrayList<Object>();
				for (int i = 0; i < children.length; ++i) {
					if (children[i] instanceof IFile) {
						String ext = ((IFile) children[i]).getFileExtension();
						if (ext == null || ! ext.equals("cs"))
							continue;
					}
					res.add(children[i]);
				}
				return res.toArray();
			}
		});
		diag.setTitle("Script File Selection"); //$NON-NLS-1$
        diag.setMessage("Choose a script file");
		diag.setInput(ResourcesPlugin.getWorkspace().getRoot());
		if (diag.open() == IDialogConstants.OK_ID) {
            IResource resource = (IResource) diag.getFirstResult();
            String arg = resource.getFullPath().toString();
            String fileLoc = VariablesPlugin.getDefault().getStringVariableManager().generateVariableExpression("workspace_loc", arg); //$NON-NLS-1$
            scriptFile.setText(fileLoc);
        }
	}
	
	public void setDefaults(ILaunchConfigurationWorkingCopy config) {
		config.setAttribute(SCRIPT_FILE, ""); //$NON-NLS-1$
		config.setAttribute(SCRIPT_ARGS, ""); //$NON-NLS-1$
		workingDirectoryBlock.setDefaults(config);
	}

	public void initializeFrom(ILaunchConfiguration config) {
		try {
			scriptFile.setText(config.getAttribute(SCRIPT_FILE, "")); //$NON-NLS-1$
			argumentsText.setText(config.getAttribute(SCRIPT_ARGS, "")); //$NON-NLS-1$
		} catch (CoreException e) {
			setErrorMessage(e.getMessage());
			DebugPlugin.log(e);
		}
		workingDirectoryBlock.initializeFrom(config);
	}

	public void performApply(ILaunchConfigurationWorkingCopy config) {
		config.setAttribute(SCRIPT_FILE, getAttributeValueFrom(scriptFile));
		config.setAttribute(SCRIPT_ARGS, getAttributeValueFrom(argumentsText));
		workingDirectoryBlock.performApply(config);
	}
	
	protected String getAttributeValueFrom(Text text) {
		String content = text.getText().trim();
		if (content.length() > 0) {
			return content;
		}
		return null;
	}

}
