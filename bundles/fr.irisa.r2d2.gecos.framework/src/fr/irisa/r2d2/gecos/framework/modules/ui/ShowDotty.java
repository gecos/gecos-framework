package fr.irisa.r2d2.gecos.framework.modules.ui;

import fr.irisa.r2d2.gecos.framework.utils.AbstractShell;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;

public class ShowDotty {
	
	private String file;

	/**
	 * Instantiates a new show dotty.
	 *
	 * @param file the file
	 */
	public ShowDotty(String file) {
		this.file = file;
	}
	
	/**
	 * Compute.
	 *
	 * @return the int
	 */
	public int compute() {
		AbstractShell shell = SystemExec.createShell().setVerbose(true);
		return shell.shell("xdot " + file);
	}
	
	
 
}
