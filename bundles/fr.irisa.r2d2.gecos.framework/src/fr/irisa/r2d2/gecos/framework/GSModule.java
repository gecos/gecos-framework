package fr.irisa.r2d2.gecos.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface GSModule {

	/**
	 * Value.
	 *
	 * @return the string
	 */
	String value();
}
