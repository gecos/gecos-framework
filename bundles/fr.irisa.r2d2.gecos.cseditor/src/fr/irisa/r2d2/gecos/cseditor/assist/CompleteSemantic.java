package fr.irisa.r2d2.gecos.cseditor.assist;

import fr.irisa.r2d2.gecos.framework.IModuleIndexer;
import fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNode;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptVariable;
import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.IErrorReporter;

public class CompleteSemantic extends SemanticAnalyser {

	private int completionOffset;

	public CompleteSemantic(IErrorReporter reporter,
			IModuleIndexer moduleIndexer) {
		super(reporter, moduleIndexer);
	}

	public void completeAt(AstList<ScriptNode> statements, int offset) {
		this.completionOffset = offset;
		this.analyse(statements);
	}
	
	private void checkComplete(ScriptNode node) {
		if (completionOffset > node.from() && completionOffset <= node.to()) {
			System.out.println("Complete: " + node);
		}
	}
	
	public void visitScriptVariable(ScriptVariable scriptVariable) {
		checkComplete(scriptVariable);
		super.visitScriptVariable(scriptVariable);
	}
	
}
