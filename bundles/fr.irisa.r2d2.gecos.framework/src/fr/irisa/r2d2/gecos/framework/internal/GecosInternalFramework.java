/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import fr.irisa.r2d2.gecos.framework.IModuleIndexer;

/**
 * The main plugin class to be used in the desktop.
 */
public class GecosInternalFramework implements BundleActivator {

	public static int debugMode = 1;
	
	/**
	 * Debug mode.
	 *
	 * @return the int
	 */
	public static int debugMode() {
		return GecosInternalFramework.debugMode;
	}
	
	/**
	 * Sets the debug mode.
	 *
	 * @param debugMode the new debug mode
	 */
	public static void setDebugMode(int debugMode) {
		GecosInternalFramework.debugMode = debugMode;
	}
	
	public static final String PLUGIN_ID = "fr.irisa.r2d2.gecos.framework"; //$NON-NLS-1$
	public static final String PL_MODULES = "modules"; //$NON-NLS-1$
	public static final String PL_PRIMITIVES = "primitives"; //$NON-NLS-1$
	public static final String PL_CONFIGURATIONS = "configurations"; //$NON-NLS-1$
	
	//The shared instance.
	private static GecosInternalFramework plugin;
	
	private BundleContext context;
	private ModuleIndexer indexer;
	
	/**
	 * The constructor.
	 */
	public GecosInternalFramework() {
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation.
	 *
	 * @param context the context
	 * @throws Exception the exception
	 */
	public void start(BundleContext context) throws Exception {
		this.context = context;
	}
	
	/**
	 * This method is called when the plug-in is stopped.
	 *
	 * @param context the context
	 * @throws Exception the exception
	 */
	public void stop(BundleContext context) throws Exception {
		if (indexer != null)
			indexer.dispose();
		context = null;
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 *
	 * @return the default
	 */
	public static GecosInternalFramework getDefault() {
		return plugin;
	}
	
	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public BundleContext getContext() {
		return context;
	}

	/**
	 * Gets the module indexer.
	 *
	 * @return the module indexer
	 */
	public IModuleIndexer getModuleIndexer() {
		if (indexer == null)
			indexer = new ModuleIndexer();
		return indexer;
	}
	
}
