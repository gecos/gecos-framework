package fr.irisa.r2d2.gecos.framework.modules.ui;


import java.io.File;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class FileSelect {

	String path = "./";
	String filter = "*";
	
	/**
	 * Instantiates a new file select.
	 */
	public FileSelect() {
	}

	/**
	 * Instantiates a new file select.
	 *
	 * @param apath the apath
	 */
	public FileSelect(String apath) {
		path=apath;
	}
	
	/**
	 * Instantiates a new file select.
	 *
	 * @param apath the apath
	 * @param afilter the afilter
	 */
	public FileSelect(String apath, String afilter) {
		path=apath;
		filter = afilter;
	}
	
	/**
	 * Compute.
	 *
	 * @return the file
	 */
	public File compute() {
	    Display d=new Display();;
	    Shell s =  new Shell(d);;

	    s.setSize(400, 400);
	    
	    s.setText("A Message");
	    FileDialog fd = new FileDialog(s);
	    fd.setText("Open");
        fd.setFilterPath(path);
        String[] filterExt = { filter };
        fd.setFilterExtensions(filterExt);
        String selected = fd.open();
		File file = new File(selected);
		d.close();
		return file;
		
	}
}
