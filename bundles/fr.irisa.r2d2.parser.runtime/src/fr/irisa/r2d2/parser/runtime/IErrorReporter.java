package fr.irisa.r2d2.parser.runtime;

public interface IErrorReporter {

	void reportError(int start, int length, String msg);

	void reportWarning(int start, int length, String msg);
	
}
