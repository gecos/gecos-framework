package fr.irisa.r2d2.parser.runtime;

public interface IGLRRuleReducer extends IRuleReducer {
	
	Object merge(int sym, Object o, Object n);
	
}
