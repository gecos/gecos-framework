package fr.irisa.r2d2.gecos.framework.impl;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleArgument;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;

public class GSAbstractModuleConstructor implements IModuleConstructor {

	private IModule module;
	protected List<IModuleArgument> arguments;
	private StringBuffer description;
	private String argumentsString; //cache
	private boolean isDeprecated = false;
	
	
	/**
	 * Instantiates a new GS abstract module constructor.
	 *
	 * @param module the module
	 */
	public GSAbstractModuleConstructor(IModule module) {
		this.module = module;
		this.description = new StringBuffer();
		this.arguments = new ArrayList<IModuleArgument>();
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#getArguments()
	 */
	@Override
	public List<IModuleArgument> getArguments() {
		return arguments;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#getDescription()
	 */
	@Override
	public String getDescription() {
		return description.toString();
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#appendDescription(java.lang.String)
	 */
	@Override
	public void appendDescription(String description) {
		this.description.append(description);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#getModule()
	 */
	@Override
	public IModule getModule() {
		return this.module;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#getArgumentsString()
	 */
	@Override
	public String getArgumentsString() {
		if(argumentsString == null) {
			argumentsString = arguments.stream()
				.map(arg -> arg.toString())
				.collect(Collectors.joining(", "));
		}
		return argumentsString;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#setDeprecated(boolean)
	 */
	@Override
	public void setDeprecated(boolean b) {
		isDeprecated = b;
	} 

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleConstructor#isDeprecated()
	 */
	@Override
	public boolean isDeprecated() {
		return isDeprecated;
	}

	/**
	 * Gets the full description.
	 *
	 * @return this constructor description including its arguments description.
	 */
	@Override
	public String getFullDescription() {
		String full = getDescription();
		if(!full.isEmpty())
			full += "\n\n";
		full += "Arguments:\n";
		full += getArguments().stream()
			.filter(arg -> !arg.name().isEmpty())
			.map(arg -> "-" + arg.name() + ": " + arg.getDescription())
			.collect(joining("\n"));
		
		return full;
	}
	
}
