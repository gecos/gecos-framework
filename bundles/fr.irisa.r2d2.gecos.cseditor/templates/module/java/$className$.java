package $packageName$;

/**
 * $className$ is a sample module. When the script evaluator encounters
 * the '$moduleName$' function, it calls the compute method.
 */
public class $className$ {
	
%if withArgument
	private String _inArg;
%endif
%if withArgument
	public $className$ (String inArg) {
%else
	public $className$ () {
%endif
%if withArgument
		_inArg = inArg;
%endif	
	}
	
	public void compute() {
%if withArgument
		System.out.println("I'm a the $moduleName$ module called with argument: \""
				+ _inArg +"\".");
%else
		System.out.println("I'm a the $moduleName$ module.");
%endif
	}
}