package java_cup.runtime;

/**
 * Default Implementation for SymbolFactory, creates
 * plain old Symbols
 *
 * @version last updated 27-03-2006
 * @author Michael Petter
 */

/* *************************************************
  class DefaultSymbolFactory

  interface for creating new symbols  
 ***************************************************/
public class DefaultSymbolFactory implements SymbolFactory{
    // Factory methods
    /**
     * DefaultSymbolFactory for CUP.
     * Users are strongly encoraged to use ComplexSymbolFactory instead, since
     * it offers more detailed information about Symbols in source code.
     * Yet since migrating has always been a critical process, You have the
     * chance of still using the oldstyle Symbols.
     *
     * @deprecated as of CUP v11a
     * replaced by the new java_cup.runtime.ComplexSymbolFactory
     */
    //@deprecated 
    public DefaultSymbolFactory(){
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int, java_cup.runtime.Symbol, java_cup.runtime.Symbol, java.lang.Object)
     */
    public Symbol newSymbol(String name ,int id, Symbol left, Symbol right, Object value){
        return new Symbol(id,left,right,value);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int, java_cup.runtime.Symbol, java_cup.runtime.Symbol)
     */
    public Symbol newSymbol(String name, int id, Symbol left, Symbol right){
        return new Symbol(id,left,right);
    }
    
    /**
     * New symbol.
     *
     * @param name the name
     * @param id the id
     * @param left the left
     * @param right the right
     * @param value the value
     * @return the symbol
     */
    public Symbol newSymbol(String name, int id, int left, int right, Object value){
        return new Symbol(id,left,right,value);
    }
    
    /**
     * New symbol.
     *
     * @param name the name
     * @param id the id
     * @param left the left
     * @param right the right
     * @return the symbol
     */
    public Symbol newSymbol(String name, int id, int left, int right){
        return new Symbol(id,left,right);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#startSymbol(java.lang.String, int, int)
     */
    public Symbol startSymbol(String name, int id, int state){
        return new Symbol(id,state);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int)
     */
    public Symbol newSymbol(String name, int id){
        return new Symbol(id);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int, java.lang.Object)
     */
    public Symbol newSymbol(String name, int id, Object value){
        return new Symbol(id,value);
    }
}
