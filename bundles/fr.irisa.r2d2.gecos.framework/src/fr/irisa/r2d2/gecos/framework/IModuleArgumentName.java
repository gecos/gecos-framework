package fr.irisa.r2d2.gecos.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Deprecated //use GSArg instead
//TODO rename as GSModuleArgument
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface IModuleArgumentName {
	
	/**
	 * Name.
	 *
	 * @return the string
	 */
	public String name();
}
