/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.osgi.framework.Bundle;
import org.osgi.util.tracker.ServiceTracker;

import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;

public class ModuleIndexer implements IModuleIndexer {
	
	private Map<String, IModule> modules; //<moduleName, module>
	private ServiceTracker<?, ?> registryTracker;

	/**
	 * Instantiates a new module indexer.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ModuleIndexer() {
		modules = new HashMap<String, IModule>();
		registryTracker = new ServiceTracker(GecosInternalFramework.getDefault().getContext(),
				IExtensionRegistry.class.getName(), null);
		registryTracker.open();
		updateIndex();
	}

	/**
	 * Update index.
	 */
	private void updateIndex() {
		IExtensionRegistry registry = (IExtensionRegistry) registryTracker.getService();
		IConfigurationElement[] elements = registry.getConfigurationElementsFor(
				GecosInternalFramework.PLUGIN_ID, GecosInternalFramework.PL_MODULES);
		for (int i = 0; i < elements.length; ++i) {
			readModule(elements[i]);
		}
	}

	/**
	 * Read module.
	 *
	 * @param element the element
	 */
	private void readModule(IConfigurationElement element) {
		Bundle bundle = findBundle(element.getDeclaringExtension().getNamespaceIdentifier());
		Module module = new Module(bundle,
				element.getDeclaringExtension().getLabel(),
				element.getAttribute("name"),
				element.getAttribute("class"));
		//XXX: use annotations instead of Description field
//		IConfigurationElement[] children = element.getChildren();
//		for (int i = 0; i < children.length; ++i) {
//			if (children[i].getName().equals("description")){
//				module.setDescription(children[i].getValue());
//			}
//		}
		modules.put(module.getName(), module);
	}
	
	/**
	 * Find bundle.
	 *
	 * @param symbolicName the symbolic name
	 * @return the bundle
	 */
	private Bundle findBundle(String symbolicName) {
		Bundle[] bundles = GecosInternalFramework.getDefault().getContext().getBundles();
		for (Bundle bundle : bundles) {
			if (bundle.getSymbolicName().equals(symbolicName))
				return bundle;
		}
		return null;
	}

	/**
	 * Dispose.
	 */
	public void dispose() {
		if (registryTracker != null) {
			registryTracker.close();
			registryTracker = null;
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleIndexer#getModulesStartingBy(java.lang.String)
	 */
	@Override
	public Collection<IModule> getModulesStartingBy(String prefix) {
		String lc = prefix.toLowerCase();
		return modules.values().stream()
			.filter(mod -> mod.getName().toLowerCase().startsWith(lc))
//			.sorted((m1,m2) -> m1.getClassName().compareToIgnoreCase(m2.getClassName()))
			.collect(Collectors.toList());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleIndexer#getModulesContaining(java.lang.String)
	 */
	@Override
	public Collection<IModule> getModulesContaining(String substring) {
		String lc = substring.toLowerCase();
		return modules.values().stream()
			.filter(mod -> mod.getName().toLowerCase().contains(lc))
			.collect(Collectors.toList());
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleIndexer#getModule(java.lang.String)
	 */
	@Override
	public IModule getModule(String moduleName) {
		return modules.get(moduleName);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleIndexer#getModules()
	 */
	@Override
	public Collection<IModule> getModules() {
		return Collections.unmodifiableCollection(modules.values());
	}

}
