package fr.irisa.r2d2.gecos.framework.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;

import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.impl.GSAbstractModuleArgument;
import fr.irisa.r2d2.gecos.framework.impl.GSAbstractModuleConstructor;

public class ModuleConstructor extends GSAbstractModuleConstructor {
	
	/**
	 * Instantiates a new module constructor.
	 *
	 * @param module the module
	 * @param cnst the cnst
	 */
	public ModuleConstructor(IModule module, Constructor<?> cnst) {
		super(module);
		
		/* get Module Constructor description from annotation*/
		if(cnst.getAnnotation(Deprecated.class) != null) {
			appendDescription("Deprecated !\n");
			setDeprecated(true);
		}
		
		GSModuleConstructor cnstAnnotation = cnst.getAnnotation(GSModuleConstructor.class);
		GSArg[] argsAnnotations = null;
		if(cnstAnnotation != null) {
			appendDescription(cnstAnnotation.value());
			argsAnnotations = cnstAnnotation.args();
		}
		
		Type[] parameterTypes = cnst.getGenericParameterTypes();
		for (int i = 0; i < parameterTypes.length; i++) {
			/* parameter type */
			Type cls = parameterTypes[i];
			String paramType = cls.getTypeName();
			int idx = paramType.lastIndexOf(".");
			paramType = idx >= 0 ? paramType.substring(idx+1) : paramType;
			
			/* parameter name and description */
			String paramName = "";
			String paramInfo = "";
			if(argsAnnotations != null && i < argsAnnotations.length) {
				paramName = argsAnnotations[i].name();
				paramInfo = argsAnnotations[i].info();
			}
			
			GSAbstractModuleArgument arg = new GSAbstractModuleArgument(Signature.toString(cls), paramType, paramName);
			arg.setDescription(paramInfo);
			arguments.add(arg);
		}
	}

}
