/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.editor;

import java.util.ResourceBundle;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.TextEditorAction;

import fr.irisa.r2d2.gecos.cseditor.text.CSPartitionScanner;

public class ToggleCommentAction extends TextEditorAction {

	protected ToggleCommentAction(ResourceBundle bundle, String prefix, ITextEditor editor) {
        super(bundle, prefix, editor);
    }
    
    
    public void run() {
    	 ITextEditor editor = getTextEditor();
         if (editor == null)
        	 return;
		
        ITextOperationTarget opTarget = (ITextOperationTarget) editor.getAdapter(ITextOperationTarget.class);
        if(opTarget == null)
        	return;
       
        Boolean isCommented = isCommented(editor);
        if(isCommented == null)
        	return;
        
		int op = isCommented ? ITextOperationTarget.STRIP_PREFIX : ITextOperationTarget.PREFIX;
        
        if(opTarget.canDoOperation(op))
        	opTarget.doOperation(op);
    }
    
    /**
     * @param editor
     * @param sel
     * @return true if all lines in selection are comments
     */
    Boolean isCommented(ITextEditor editor) {
    	ISelectionProvider selProvider = editor.getSelectionProvider();
		IDocument document = editor.getDocumentProvider().getDocument(editor.getEditorInput());
		if (selProvider == null || document == null)
			return null;
		
    	ITextSelection sel = getSelection(selProvider);
    	if(sel == null)
    		return null;
    	
    	try {
	    	int startLine = sel.getStartLine();
			int endLine = sel.getEndLine();
			if(startLine < 0 || endLine < 0)
				return null;
			
			for(int i = startLine; i <= endLine; i++) {
    			IRegion line = document.getLineInformation(i);
//				if(!document.getPartition(line.getOffset()).getType().equals(CSPartitionScanner.CS_COMMENT))
//					return false;
				String lineText = document.get(line.getOffset(), line.getLength());
				if(!lineText.startsWith(CSPartitionScanner.CS_COMMENT_PREFIX)) //XXX
					return false;
	    	}
	    	return true;
			
    	} catch (BadLocationException e1) {
			e1.printStackTrace();
			return null;
		}

    }
    
    private static ITextSelection getSelection(ISelectionProvider provider) {
        ISelection selection = provider.getSelection();
        if (selection instanceof ITextSelection)
            return (ITextSelection) selection;
        return TextSelection.emptySelection();
    }
    
}
