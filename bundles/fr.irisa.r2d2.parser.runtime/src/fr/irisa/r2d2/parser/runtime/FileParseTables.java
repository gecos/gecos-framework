package fr.irisa.r2d2.parser.runtime;

import java.io.IOException;
import java.io.InputStream;


public class FileParseTables implements IParseTables {

	private short[][] actionTable;
	private short[][] reduceTable;
	private short[][] productionTable;
	private int acceptingRule;

	public FileParseTables(Class<?> cls, String filename) throws IOException {
		load(cls.getResourceAsStream(filename));
	}

	private void load(InputStream str) throws IOException {
		TableInputStream stream = new TableInputStream(str);
		int stateCount = stream.readValue();
		actionTable = new short[stateCount][];
		for (int i = 0; i < stateCount; ++i) {
			int transitionCount = stream.readValue();
			actionTable[i] = new short[transitionCount*2];
			for (int j = 0; j < actionTable[i].length; j += 2) {
				actionTable[i][j]   = (short) stream.readValue();
				actionTable[i][j+1] = (short) stream.readValue();
			}
		}
		reduceTable = new short[stateCount][];
		for (int i = 0; i < stateCount; ++i) {
			int transitionCount = stream.readValue();
			reduceTable[i] = new short[transitionCount*2];
			for (int j = 0; j < reduceTable[i].length; j += 2) {
				reduceTable[i][j]   = (short) stream.readValue();
				reduceTable[i][j+1] = (short) stream.readValue();
			}
		}
		productionTable = new short[stream.readValue()][];
		for (int i = 0; i < productionTable.length; ++i) {
			productionTable[i] = new short[2];
			productionTable[i][0] = (short) stream.readValue();
			productionTable[i][1] = (short) stream.readValue();
		}
		acceptingRule = stream.readValue();
	}


	public int acceptingProduction() {
		return acceptingRule;
	}


	public short[][] actionTable() {
		return actionTable;
	}


	public short[][] productionTable() {
		return productionTable;
	}


	public short[][] reduceTable() {
		return reduceTable;
	}

	
	public int startState() {
		return 0;
	}

}
