package fr.irisa.r2d2.parser.runtime;

public class Token implements IToken {

	private int type;
	private int start;
	private int end;
	private Object value;

	public Token(int type, int start, int length, Object value) {
		this.type = type;
		this.start = start;
		this.end = start + length;
		this.value = value;
	}

	public int end() {
		return end;
	}


	public int start() {
		return start;
	}


	public int type() {
		return type;
	}


	public Object value() {
		return value;
	}
	
	public String toString() {
		return "#" + type;
	}

}
