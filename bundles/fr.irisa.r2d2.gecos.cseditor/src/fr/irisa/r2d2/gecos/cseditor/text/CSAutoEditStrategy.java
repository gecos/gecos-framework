/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditorExtension3;
import org.eclipse.ui.texteditor.ITextEditorExtension3.InsertMode;

public class CSAutoEditStrategy extends DefaultIndentLineAutoEditStrategy {

	 public void customizeDocumentCommand(IDocument d, DocumentCommand c) {
		if (isSmartInsertMode()) {
			/*if (isNewLine(d, c)) {
				smartIndentAfterNewLine(d, c);
			} else if (c.text != null) {
				if (c.text.length() == 1) {
					switch (c.text.charAt(0)) {
					case '}': {
						smartInsertClosingBrace(d, c);
						break;
					}
					default: {
						// do nothing
					}
					}
				}
				if (store.getBoolean(SPACES_FOR_TABS)) {
					convertTabs(d, c);
				}
			}*/
		}
	}
	
	private boolean isSmartInsertMode() {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page = window.getActivePage();
			if (page != null) {
				IEditorPart part = page.getActiveEditor();
				if (part instanceof ITextEditorExtension3) {
					InsertMode insertMode = ((ITextEditorExtension3) part)
							.getInsertMode();
					return (insertMode == ITextEditorExtension3.SMART_INSERT);
				}
			}
		}
		return false;
	}
}
