/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.editor;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.texteditor.IEditorStatusLine;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.TextEditorAction;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;
import fr.irisa.r2d2.gecos.framework.IModule;

public class OpenDefinitionAction extends TextEditorAction {

    protected OpenDefinitionAction(ResourceBundle bundle, String prefix, ITextEditor editor) {
        super(bundle, prefix, editor);
    }

    public void run() {
        ITextSelection sel = getSelection();
        if (sel.getLength() > 0)
            openType(sel.getText());
    }
    
    /*public void update() {
        setEnabled(getSelection().getLength() > 0);
    }*/
    
    private ITextSelection getSelection() {
        ISelectionProvider provider = getSelectionProvider();
        if (provider != null) {
            ISelection selection = provider.getSelection();
            if (selection instanceof ITextSelection)
                return (ITextSelection) selection;
        }
        return TextSelection.emptySelection();
    }
    
    private ISelectionProvider getSelectionProvider() {
        ITextEditor editor = getTextEditor();
        if (editor != null) {
            return editor.getSelectionProvider();
        }
        return null;
    }
    
    private void openType(String funcName) {
        IModule module = CSEditorPlugin.getDefault().getModuleIndexer().getModule(funcName);
        if (module == null) return;
        
        SearchPattern pattern = SearchPattern.createPattern(module.getClassName(),
                IJavaSearchConstants.TYPE,
                IJavaSearchConstants.DECLARATIONS,
                SearchPattern.R_EXACT_MATCH);
        final List<SearchMatch> results = new ArrayList<SearchMatch>();
        SearchRequestor requestor = new SearchRequestor() {
            public void acceptSearchMatch(SearchMatch match) throws CoreException {
                results.add(match);
            }
        };
        try {
        	SearchEngine searchEngine = new SearchEngine();
            searchEngine.search(pattern, new SearchParticipant[] {
                    SearchEngine.getDefaultSearchParticipant() },
                    SearchEngine.createWorkspaceScope(), requestor, null);
            
        } catch (CoreException e) {
        }
        if (results.size() > 0) {
            SearchMatch match = results.get(0);
            IMember member = (IMember) match.getElement();
            ICompilationUnit unit = member.getCompilationUnit();
            try {
                IEditorPart javaEditor = JavaUI.openInEditor(unit);
                JavaUI.revealInEditor(javaEditor, (IJavaElement) member);
            } catch (PartInitException e) {
            } catch (JavaModelException e) {
            }
        } else {
            IEditorStatusLine statusLine = (IEditorStatusLine) getTextEditor()
                    .getAdapter(IEditorStatusLine.class);
            if (statusLine != null)
                statusLine.setMessage(
                                true,
                                "cant find: " + module.getClassName() /*CSMessages.OpenAction_error_messageBadSelection*/,
                                null);
        }
    }
}
