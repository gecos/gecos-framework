/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.launching;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.pde.ui.launcher.AbstractLaunchShortcut;
import org.eclipse.pde.ui.launcher.EclipseLaunchShortcut;
import org.eclipse.ui.IEditorPart;

import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.gecos.framework.utils.GecosApplicationConfiguration;

public class CSLaunchShortcut extends AbstractLaunchShortcut {
	
	private IFile scriptFile;

	public void launch(ISelection selection, String mode) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection)selection;
			if (!ssel.isEmpty()) {
				IFile file = null;
				Object object = ssel.getFirstElement();
				if (object instanceof IFile) {
					file = (IFile)object;
				} else if (object instanceof IAdaptable) {
					file = (IFile)((IAdaptable)object).getAdapter(IFile.class);
				}
				if (file != null)
					launch(file, mode);
			}
		}
	}

	public void launch(IEditorPart editor, String mode) {
		IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);
		launch(file, mode);
	}
	
	private void launch(final IFile file, final String mode) {
		scriptFile = file;
		final File handle = GecosFramework.getRunHandler(new File(file.getRawLocationURI()));
		if (!handle.exists())
			try {
				handle.createNewFile();
			} catch (IOException e) { }
		launch(mode);
		new Thread() {
			public void run() { 
				while (handle.exists())
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) { }
				final IProject project= file.getProject();
				final IWorkspace ws = ResourcesPlugin.getWorkspace();
				try {
					ws.run(new IWorkspaceRunnable() {
						@Override
						public void run(IProgressMonitor monitor) throws CoreException {
							project.refreshLocal(IResource.DEPTH_INFINITE, null);
						}
					}, null);
				} catch (CoreException e) { }
			}
		}.start();
	}

	@Override
	protected String getLaunchConfigurationTypeName() {
		return EclipseLaunchShortcut.CONFIGURATION_TYPE;
	}

	@Override
	protected void initializeConfiguration(ILaunchConfigurationWorkingCopy wc) {
		GecosApplicationConfiguration.initializeConfiguration(wc, scriptFile.getLocation().toOSString());
	}

	@Override
	protected boolean isGoodMatch(ILaunchConfiguration configuration) {
		try {
			String attr = configuration.getAttribute(GecosApplicationConfiguration.SCRIPT_FILE, (String)null);
			if (attr == null)
				return false;
			return attr.equals(scriptFile.getLocation().toOSString());
		} catch (CoreException e) {
		}
		return false;
	}
	
	protected String getName(ILaunchConfigurationType type) {
		return "Gecos Script " + scriptFile.getName();
	}

}
