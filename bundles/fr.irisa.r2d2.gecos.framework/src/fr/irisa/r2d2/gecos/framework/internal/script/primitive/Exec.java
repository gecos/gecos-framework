/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.script.primitive;


import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.IntegerNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.utils.AbstractShell;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import fr.irisa.r2d2.parser.runtime.Span;

public class Exec implements IScriptEvaluable {

	final private static boolean verbose = false;
	
	/**
	 * Instantiates a new exec.
	 */
	public Exec() { }

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {
		if (args.length < 1)
			throw new ScriptException("exec : expects at least one argument");
		
		String[] command = new String[args.length];
		for (int i = 0; i < args.length; i++) 
			command[i] = args[i].asString().value();
		AbstractShell shell = SystemExec.createShell().setVerbose(verbose);
		int res = shell.exec(command, null, null, AbstractShell.defaultStdout, AbstractShell.defaultStderr);
		return new IntegerNode(span, res);
	}
}
