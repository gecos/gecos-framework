/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.script.primitive;


import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.BoolNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.utils.GecosScriptLauncher;
import fr.irisa.r2d2.parser.runtime.Span;

public class CallInPlace implements IScriptEvaluable {
	
	/**
	 * Instantiates a new call in place.
	 */
	public CallInPlace() { }

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {
		if (args.length < 1)
			throw new ScriptException("call : expects one argument");
		
		String scriptPath = args[0].asString().value();
		String[] arguments = new String[args.length-1];
		for (int i = 1; i < args.length; i++) 
			arguments[i-1] = args[i].asString().value();
		boolean res = GecosScriptLauncher.launch(scriptPath,false, true, arguments);
		return new BoolNode(span, res);
	}
}
