package fr.irisa.r2d2.gecos.cseditor.text;

import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.reconciler.DirtyRegion;
import org.eclipse.jface.text.reconciler.IReconcilingStrategy;
import org.eclipse.jface.text.reconciler.IReconcilingStrategyExtension;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.MarkerAnnotation;

import fr.irisa.r2d2.parser.runtime.IErrorReporter;

public class ScriptReconcilingStrategy implements IReconcilingStrategy, IReconcilingStrategyExtension {

	private ITextEditor editor;
//	private IProgressMonitor progressMonitor;

	public ScriptReconcilingStrategy(ITextEditor editor) {
		this.editor = editor;
	}
	
	private static class ErrorReporter implements IErrorReporter {

		private IAnnotationModel annotationModel;

		public ErrorReporter(IAnnotationModel annotationModel) {
			this.annotationModel = annotationModel;
		}

		public void reportError(int start, int length, String msg) {
			ScriptProblemAnnotation annotation = new ScriptProblemAnnotation(
					ScriptProblemAnnotation.ERROR, msg);
			Position  position = new Position(start, length);
			annotationModel.addAnnotation(annotation, position);
		}

		public void reportWarning(int start, int length, String msg) {
			ScriptProblemAnnotation annotation = new ScriptProblemAnnotation(
					ScriptProblemAnnotation.WARNING, msg);
			Position  position = new Position(start, length);
			annotationModel.addAnnotation(annotation, position);
		}
		
	}
	
	private void internalReconcile() {
		ScriptDocumentProvider provider = getScriptDocumentProvider();
		IScriptCompilationUnit desc = provider.getScriptCompilationUnit(editor.getEditorInput());
		IAnnotationModel annotationModel = provider.getAnnotationModel(editor.getEditorInput());
		ErrorReporter reporter = new ErrorReporter(annotationModel);
		removeAnnotations(annotationModel);
		desc.reconcile(provider.getDocument(editor.getEditorInput()).get(), reporter);
	}

	private ScriptDocumentProvider getScriptDocumentProvider() {
		return (ScriptDocumentProvider) editor.getDocumentProvider();
	}

	private void removeAnnotations(IAnnotationModel annotationModel) {
		for (Iterator<?> it = annotationModel.getAnnotationIterator(); it.hasNext();) {
	        Annotation annotation = (Annotation) it.next();
	        if ((annotation instanceof ScriptProblemAnnotation)
	        		|| (annotation instanceof MarkerAnnotation))
	          annotationModel.removeAnnotation(annotation);
	      }
	}

	public void reconcile(IRegion partition) {
		internalReconcile();
	}

	public void reconcile(DirtyRegion dirtyRegion, IRegion subRegion) {
		internalReconcile();
	}

	public void setDocument(IDocument document) {
	}

	public void initialReconcile() {
		internalReconcile();
	}

	public void setProgressMonitor(IProgressMonitor monitor) {
//		this.progressMonitor = monitor;
	}

}
