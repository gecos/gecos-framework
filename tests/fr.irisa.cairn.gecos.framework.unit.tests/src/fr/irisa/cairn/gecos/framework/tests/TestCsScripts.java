package fr.irisa.cairn.gecos.framework.tests;

import org.junit.Assert;
import org.junit.Test;

import fr.irisa.r2d2.gecos.framework.utils.GecosScriptLauncher;

public class TestCsScripts {

	@Test
	public void test() {
		boolean res = GecosScriptLauncher.launch("test-scripts/Test.cs", false, "test-scripts");
		Assert.assertTrue("Compiler script 'test-srcipts/Test.cs' failed", res);
	}
}
