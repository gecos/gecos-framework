<?php
/***************************************************************************
 * cs.php
 * -----
 * Author: Ludovic L'Hours
 *
 * Compiler Script language file for GeSHi.
 *
 */

$language_data = array (
	'LANG_NAME' => 'Compiler Script',
	'COMMENT_SINGLE' => array(1 => '#'),
	'COMMENT_MULTI' => array(),
	'CASE_KEYWORDS' => GESHI_CAPS_NO_CHANGE,
	'QUOTEMARKS' => array('"'),
	'ESCAPE_CHAR' => '\\',
	'KEYWORDS' => array(
		1 => array(
			'while', 'done', 'changing', 'in',
			'do', 'for'
			),
		2 => array(
			'nil'
			),
		3 => array(
			'echo', 'output'
			),
		),
	'SYMBOLS' => array(
		'->', '(', ')', '{', '}', '[', ']', '=', ';', ',', '+', '-'
		),
	'CASE_SENSITIVE' => array(
		GESHI_COMMENTS => true,
		1 => false,
		2 => false,
		3 => false,
		),
	'STYLES' => array(
		'KEYWORDS' => array(
			1 => 'color: #b1b100;',
			2 => 'color: #000000; font-weight: bold;',
			3 => 'color: #000066;'
			),
		'COMMENTS' => array(
			1 => 'color: #808080; font-style: italic;'
			),
		'ESCAPE_CHAR' => array(
			0 => 'color: #000099; font-weight: bold;'
			),
		'BRACKETS' => array(
			0 => 'color: #66cc66;'
			),
		'STRINGS' => array(
			0 => 'color: #ff0000;'
			),
		'NUMBERS' => array(
			0 => 'color: #cc66cc;'
			),
		'METHODS' => array(
			1 => 'color: #202020;',
			2 => 'color: #202020;'
			),
		'SYMBOLS' => array(
			0 => 'color: #66cc66;'
			),
		'REGEXPS' => array(
			0 => 'color: #66cc66;',
			1 => 'color: #6666cc;'
			),
		'SCRIPT' => array(
			)
		),
	'URLS' => array(
		1 => '',
		2 => '',
		3 => 'http://www.opengroup.org/onlinepubs/009695399/functions/{FNAME}.html',
		4 => ''
		),
	'OOLANG' => false,
	'REGEXPS' => array(
		0 => '[\\$][0-9]+',
		1 => array(
			GESHI_SEARCH => '([a-zA-Z_][a-zA-Z0-9_]*)(\()',
			GESHI_REPLACE => '\\1',
			GESHI_MODIFIERS => '',
			GESHI_BEFORE => '',
			GESHI_AFTER => '\\2'
			)
		),
	'STRICT_MODE_APPLIES' => GESHI_NEVER,
	'SCRIPT_DELIMITERS' => array(
		),
	'HIGHLIGHT_STRICT_BLOCK' => array(
		)
);

?>
