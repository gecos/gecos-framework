/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.script;

import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.parser.runtime.Span;

public interface IScriptEvaluator {

	/**
	 * Gets the argument.
	 *
	 * @param span the span
	 * @param arg the arg
	 * @return the argument
	 */
	public abstract Node getArgument(Span span, int arg);

	/**
	 * Sets the variable.
	 *
	 * @param span the span
	 * @param name the name
	 * @param value the value
	 */
	public abstract void setVariable(Span span, String name, Node value);

	/**
	 * Gets the variable.
	 *
	 * @param span the span
	 * @param name the name
	 * @return the variable
	 */
	public abstract Node getVariable(Span span, String name);

	/**
	 * Apply.
	 *
	 * @param span the span
	 * @param name the name
	 * @param args the args
	 * @return the node
	 * @throws ScriptException the script exception
	 */
	public abstract Node apply(Span span, String name, Node[] args)
			throws ScriptException;

	/**
	 * Sets the changing.
	 *
	 * @param value the new changing
	 */
	public abstract void setChanging(boolean value);

	/**
	 * Checks if is changing.
	 *
	 * @return true, if is changing
	 */
	public abstract boolean isChanging();

	/**
	 * Do exit.
	 *
	 * @param status the status
	 */
	public abstract void doExit(int status);

	/**
	 * Gets the description.
	 *
	 * @param string the string
	 * @return the description
	 */
	public abstract String getDescription(String string);

	/**
	 * Register function.
	 *
	 * @param func the func
	 */
	public abstract void registerFunction(IFunctionFactory func);

}