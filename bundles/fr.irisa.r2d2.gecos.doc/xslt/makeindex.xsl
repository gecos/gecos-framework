<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/TR/xhtml1/strict">
		
	<!-- This stylesheet is used to build the main index HTML file.
		 It uses typedb.xml as input. It uses triky stuff as xsl:key -->
		
	<xsl:output method="html" indent="yes"/>
			
	<!-- xsl:key is used to index the transform elements by their module name -->
	<xsl:key name="trans-by-mod" match="transform" use="@module"/>
	
	<xsl:template match="types">
		<html>
			<head>
				<title>Gecos Functions</title>
				<link rel="stylesheet" type="text/css" media="screen" href="gecos-api.css"/>
			</head>
			<body>
				<div id="index">
				<h1>Gecos Functions</h1>
				<!-- This line is a kind of magic, no comment... ;-) -->
				<!-- It seems to make a set of uniq keys -->
				<!-- T1 -> m1  key(m1) = {T1,T2} -> count == 1 (supposed that order of key is fixed)
					 T2 -> m1  key(m1) = {T1,T2} -> count == 2
					 T3 -> m2  key(m2) = {T3}    -> count == 1
					 T4 -> m3  key(m3) = {T4}    -> count == 1
					 => result {T1, T3, T4}
				-->
				<xsl:for-each select="transform[count(. | key('trans-by-mod', @module)[1]) = 1]">
					<xsl:sort select="@module"/>
					<div class="module">
					<xsl:variable name="moduleDir" select="translate(@module,' ','_')"/>
					<h2><xsl:value-of select="@module"/>:</h2>
					<p><ul>
						<xsl:for-each select="key('trans-by-mod', @module)">
							<xsl:sort select="@name"/>
							<li><a href="{$moduleDir}/{@name}.html">
								<xsl:value-of select="@name"/></a></li>
						</xsl:for-each>
					</ul></p>
					</div>
				</xsl:for-each>
				</div>
			</body>
		</html>
	</xsl:template>
	
</xsl:stylesheet>
