/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class BoolNode extends Node {
	private boolean value;
	
	/**
	 * Instantiates a new bool node.
	 *
	 * @param span the span
	 * @param value the value
	 */
	public BoolNode(Span span, boolean value) {
		super(span);
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asBoolean()
	 */
	public boolean asBoolean() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asInteger()
	 */
	public Integer asInteger() {
		return value?1:0;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject()
	 */
	public Object asObject() {
		return asBoolean();
	}
}