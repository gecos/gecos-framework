/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.gecos.framework.IConfiguration;
import fr.irisa.r2d2.gecos.framework.IOptionsManager;
import fr.irisa.r2d2.parser.runtime.DefaultErrorReporter;

public class ScriptConfiguration implements IConfiguration, Version {
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IConfiguration#run(java.util.Properties)
	 */
	public int run(Properties properties) {
		
		boolean isVerbose = "true".equals(properties.getProperty("verbose"));
		String scriptFile = properties.getProperty("script-file");
		String expr = properties.getProperty("script-expr");
		String[] args = (String[]) properties.get(IOptionsManager.OTHER_ARGUMENTS);
		DefaultErrorReporter reporter = new DefaultErrorReporter(GecosFramework.err);
		
		if (isVerbose)
			GecosApplication.banner();
		
		if (scriptFile != null) {
			boolean ranWithErrors = false;
			File file = new File(scriptFile);
			try {
				ranWithErrors = GecosFramework.evaluate(scriptFile, new FileReader(file), args, reporter);
			} catch (IOException e) {
				ranWithErrors = true;
			}
			try { reporter.printDiagnostic(scriptFile); } catch (IOException e1) { }
			
			if(ranWithErrors)
				throw new RuntimeException("evaluation failed");
			return 0;
		} else if (expr != null) {
			if (GecosInternalFramework.debugMode() > 0)
				GecosFramework.err.println("expression: "+expr);
				if (GecosFramework.evaluate("(stdin)", new StringReader(expr), args, reporter)) {
					try {reporter.printDiagnostic(new StringReader(expr), scriptFile); } catch (IOException e1) { }
					throw new RuntimeException("evaluation failed");
				}
				return 0;
		}
		GecosFramework.err.println("error : no script file or expression");
		throw new RuntimeException("error : no script file or expression");
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IConfiguration#configureOptions(fr.irisa.r2d2.gecos.framework.IOptionsManager)
	 */
	public void configureOptions(IOptionsManager manager) {
		manager.add("h", "help", IOptionsManager.NO_ARG, "display help message");
		manager.add("x", "debug", IOptionsManager.NO_ARG, "set debug mode");
		manager.add("v", "verbose", IOptionsManager.NO_ARG, "set verbose mode");
		manager.add("c", "script-file", IOptionsManager.HAS_ARG, "script file to execute");
		manager.add("e", "script-expr", IOptionsManager.HAS_ARG, "expression to execute");
	}

}
