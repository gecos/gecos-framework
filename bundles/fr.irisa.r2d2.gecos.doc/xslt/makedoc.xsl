<?xml version="1.0"?>

<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/TR/xhtml1/strict" >
		
	<!-- This stylesheet is used to build the documentation HTML file
		for each module. It uses the plugin.xml file as input.
		-->
		
	<xsl:output method="html" indent="yes"/>
	
	<!-- the base directory where every plugins are located, relative
		 to the directory containing this file -->
	<xsl:variable name="base">../../</xsl:variable>
	
	<!-- default rules: no recursion -->
	<xsl:template match="*"/>

	<xsl:template match="/">
		<xsl:apply-templates select="modules/module" />
	</xsl:template>

	<xsl:template match="module">
		<xsl:variable name="filename"><xsl:value-of select="translate(@packageName,' ','_')"/>/<xsl:value-of select="@name"/>.html</xsl:variable>
		<xsl:message>Creating <xsl:value-of select="$filename"/></xsl:message>
		<xsl:result-document href="../doc/functions/{$filename}">
		<html>
			<head>
				<title>Function <xsl:value-of select="@name"/></title>
				<link rel="stylesheet" type="text/css" media="screen" href="../gecos-api.css"/>
			</head>
			<body>
			<div class="transform">
				<h2><a name="{@name}"><xsl:value-of select="@name"/></a></h2>
				<p class="cls">Class <span><xsl:value-of select="@className"/></span></p>
				<xsl:apply-templates select="constructor" />
				<div class="desc">
				<xsl:variable name="exthelp"
					select="document('help.xml')/help/transform[@name=current()/@name]"/>
				<xsl:choose>
					<xsl:when test="count($exthelp) = 0">
						<p><xsl:value-of select="description"/> </p>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="$exthelp/*"/>
					</xsl:otherwise>
				</xsl:choose>
				</div>
			</div>
			</body>
		</html>
		</xsl:result-document>
	</xsl:template>
	
	<xsl:template match="constructor">
		<p class="prototype"><xsl:if test="../@returnType">
			<xsl:call-template name="type-ref">
				<xsl:with-param name="class" select="../@returnType"/>
			</xsl:call-template><xsl:text> </xsl:text></xsl:if>
		<xsl:variable name="arguments"><xsl:apply-templates select="arg"/></xsl:variable>
		<b><xsl:value-of select="../@name"/></b>(<xsl:copy-of select="$arguments"/>)
		</p>
	</xsl:template>
	
	<xsl:template match="arg">
		<xsl:call-template name="type-ref">
			<xsl:with-param name="class" select="@type"/>
		</xsl:call-template> 
		<xsl:if test="(string-length(@name) > 0) and not (starts-with(@name, 'arg'))"><xsl:text> </xsl:text><xsl:value-of select="@name"/></xsl:if>
		<xsl:if test="not (position()=last())">, </xsl:if>
	</xsl:template>
	
	<xsl:template name="type-ref">
		<xsl:param name="class"/>
		<xsl:variable name="classShort">
			<xsl:value-of select="tokenize($class, '\.')[last()]"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$class = 'void'">
			</xsl:when>
			<xsl:when test="$class = 'int' or $class = 'boolean' or starts-with($class, 'java.')">
				<xsl:value-of select="$classShort"/>
			</xsl:when>
			<xsl:otherwise>
				<a class="type-ref" href="../types.html#{$class}"><xsl:value-of select="$classShort"/></a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>
