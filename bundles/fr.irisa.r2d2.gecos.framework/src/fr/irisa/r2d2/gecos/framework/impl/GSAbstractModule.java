package fr.irisa.r2d2.gecos.framework.impl;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;

public class GSAbstractModule implements IModule {

	protected String name;
	protected String className;
	protected String packageName;
	protected List<IModuleConstructor> constructors;
	
	protected boolean isIterative;
	protected boolean isDeprecated = false;
	protected StringBuffer description;
	protected String returnType;
	
	/**
	 * Instantiates a new GS abstract module.
	 *
	 * @param moduleName the module name
	 * @param className the class name
	 * @param packageName the package name
	 */
	public GSAbstractModule(String moduleName, String className, String packageName) {
		this.name = moduleName;      
		this.className = className;   
		this.packageName = packageName; 
		this.description = new StringBuffer();
		this.constructors = new ArrayList<IModuleConstructor>();
	}
	
	/**
	 * Sets the return type.
	 *
	 * @param returnType the new return type
	 */
	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	/**
	 * Sets the iterative.
	 *
	 * @param isIterative the new iterative
	 */
	public void setIterative(boolean isIterative) {
		this.isIterative = isIterative;
	}
	
	/**
	 * Sets the deprecated.
	 *
	 * @param isDeprecated the new deprecated
	 */
	public void setDeprecated(boolean isDeprecated) {
		this.isDeprecated = isDeprecated;
	}
	
	/**
	 * Append description.
	 *
	 * @param description the description
	 */
	public void appendDescription(String description) { //XXX change to set description
		this.description.append(description);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#getPackageName()
	 */
	@Override
	public String getPackageName() {
		return packageName;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#getClassName()
	 */
	@Override
	public String getClassName() {
		return className;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#getReturnType()
	 */
	@Override
	public String getReturnType() {
		return returnType;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#isIterative()
	 */
	@Override
	public boolean isIterative() {
		return isIterative;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#getDescription()
	 */
	@Override
	public String getDescription() {
		return description.toString().trim();
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#getConstructors()
	 */
	@Override
	public List<IModuleConstructor> getConstructors() {
		return constructors;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModule#isDeprecated()
	 */
	@Override
	public boolean isDeprecated() {
		return isDeprecated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name + " -> " + className;
	}
}
