/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class FloatNode extends Node {
	
	private double value;

	/**
	 * Instantiates a new float node.
	 *
	 * @param span the span
	 * @param value the value
	 */
	public FloatNode(Span span, double value) {
		super(span);
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject()
	 */
	public Object asObject() {
		return asFloat();
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asFloat()
	 */
	public Double asFloat() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asString()
	 */
	public StringNode asString() {
		return new StringNode(span, Double.toString(value));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#applyPlus(fr.irisa.r2d2.gecos.framework.script.nodes.Node)
	 */
	public Node applyPlus(Node right) {
		if (right instanceof IntegerNode)
			return new FloatNode(span, value + right.asFloat());
		return new StringNode(span, String.valueOf(value) + right.asString().value());
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#applyMinus(fr.irisa.r2d2.gecos.framework.script.nodes.Node)
	 */
	public Node applyMinus(Node right) {
		return new FloatNode(span, value - right.asFloat());
	}
}