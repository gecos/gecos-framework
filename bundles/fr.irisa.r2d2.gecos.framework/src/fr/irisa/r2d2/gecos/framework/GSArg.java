package fr.irisa.r2d2.gecos.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface GSArg {

	String INFO_KEY = "info";
	
	/**
	 * Name.
	 *
	 * @return the string
	 */
	String name();
	
	/**
	 * Info.
	 *
	 * @return the string
	 */
	String info() default "";
	
}