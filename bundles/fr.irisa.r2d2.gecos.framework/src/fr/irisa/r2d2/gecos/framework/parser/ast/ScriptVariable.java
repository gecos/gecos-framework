


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptVariable extends ScriptNode {

	/**
	 * @generated
	 */
	private String name;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptVariable(Span span, String name) {
		super(span);
		this.name = name;
	}
	
	/**
	 * @generated
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @generated
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptVariable(this);
	}
}