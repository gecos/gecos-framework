package fr.irisa.r2d2.gecos.framework.parser.ast;

import fr.irisa.r2d2.parser.runtime.Span;

public class ScriptIdent extends ScriptNode {

	private String value;

	public ScriptIdent(Span span, String value) {
		super(span);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

	@Override
	public void visit(IGecosVisitor visitor) {
		throw new RuntimeException("not implemented");
	}

}
