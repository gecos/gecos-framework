/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextViewerExtension2;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.MonoReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.ui.texteditor.ITextEditor;

import fr.irisa.r2d2.gecos.cseditor.CSColorProvider;
import fr.irisa.r2d2.gecos.cseditor.assist.CSContentAssistProcessor;

public class CSSourceViewerConfiguration extends SourceViewerConfiguration {

	
	private CSColorProvider colorProvider;
	private ITextEditor editor;
	private CSScanner scanner;
	
	public CSSourceViewerConfiguration(CSColorProvider colorProvider, ITextEditor editor) {
		this.colorProvider = colorProvider;
		this.editor = editor;
		this.scanner = new CSScanner(colorProvider);
	}
	
	public String getConfiguredDocumentPartitioning(ISourceViewer sourceViewer) {
		return CSPartitionScanner.CS_PARTITIONING;
	}
	
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] { IDocument.DEFAULT_CONTENT_TYPE, CSPartitionScanner.CS_COMMENT};
	}
	
    @Override
    public String[] getDefaultPrefixes(ISourceViewer sourceViewer, String contentType) {
//    	if(contentType.equals(CSPartitionScanner.CS_COMMENT)) {
//	    	return new String[] {CSPartitionScanner.CS_COMMENT_PREFIX};
//    	}
    	return new String[] {CSPartitionScanner.CS_COMMENT_PREFIX};
    }
	
	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
        ContentAssistant assistant = new ContentAssistant();
        assistant.setDocumentPartitioning(
        		getConfiguredDocumentPartitioning(sourceViewer));
        assistant.setContentAssistProcessor(
            new CSContentAssistProcessor(editor),
            IDocument.DEFAULT_CONTENT_TYPE);
        assistant.setInformationControlCreator(
            getInformationControlCreator(sourceViewer));
        assistant.enableAutoInsert(true);
        assistant.enableAutoActivation(true);
        assistant.setContextInformationPopupOrientation(
            IContentAssistant.CONTEXT_INFO_BELOW);
        assistant.setProposalPopupOrientation(
            IContentAssistant.PROPOSAL_STACKED);
        assistant.setProposalSelectorBackground(colorProvider.getColor(CSColorProvider.ASSIST));
        /*assistant.setProposalSelectorForeground(getColorManager().getColor(
            PreferenceConverter.getColor(store, PROPOSALS_FOREGROUND)));*/
        return assistant;
    }
	
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler reconciler = new PresentationReconciler();
		reconciler.setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));

		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);
		
		// use the same scanner for the comments
		reconciler.setDamager(dr, CSPartitionScanner.CS_COMMENT);
		reconciler.setRepairer(dr, CSPartitionScanner.CS_COMMENT);

		return reconciler;
	}
	
	public IAutoEditStrategy[] getAutoEditStrategies(ISourceViewer sourceViewer, String contentType) {
		return new IAutoEditStrategy[] { /*new CSAutoEditStrategy()*/ new DefaultIndentLineAutoEditStrategy() };
	}
    
    public IReconciler getReconciler(ISourceViewer sourceViewer) {
    	ScriptReconcilingStrategy strategy = new ScriptReconcilingStrategy(editor);
    	MonoReconciler reconciler = new MonoReconciler(strategy, true);
        reconciler.setProgressMonitor(new NullProgressMonitor());
		reconciler.setDelay(500);
        return reconciler;
    }
    
    public ITextHover getTextHover(ISourceViewer sourceViewer, String contentType) {
    	return getTextHover(sourceViewer, contentType, ITextViewerExtension2.DEFAULT_HOVER_STATE_MASK);
	}
    
    public ITextHover getTextHover(ISourceViewer sourceViewer, String contentType, int stateMask) {
    	return new CSTextHover(sourceViewer, editor);
	}
    
    /*public IInformationPresenter getInformationPresenter(ISourceViewer sourceViewer) {
        InformationPresenter presenter = new InformationPresenter(getInformationControlCreator(sourceViewer));
        presenter.setInformationProvider(new CSInformationProvider(editor), IDocument.DEFAULT_CONTENT_TYPE);
        return presenter;
    }*/
    
}
