package fr.irisa.r2d2.gecos.framework.utils;

import java.io.FileNotFoundException;
import java.io.PrintStream;

@SuppressWarnings("all")
public class ScriptLog {

	private static final String INDENT = "    ";
	private static ScriptLog consoleInstance;
	PrintStream logStream;
	int depth = 0;
	String indent = "";

	/**
	 * Indent.
	 */
	public void indent() {
		indent = "";

		int j = ++depth;
		for (int i = 0; i < j; i++)
			indent += INDENT;

	}

	/**
	 * De indent.
	 */
	public void deIndent() {
		indent = "";
		int j = --depth;
		for (int i = 0; i < j; i++)
			indent += INDENT;

	}

	/**
	 * Instantiates a new script log.
	 */
	public ScriptLog() {
		logStream = System.out;
	}

	/**
	 * Gets the single instance of ScriptLog.
	 *
	 * @return single instance of ScriptLog
	 */
	public static ScriptLog getInstance() {
		if (consoleInstance == null) {
			consoleInstance = new ScriptLog();
		}
		return consoleInstance;
	}

	/**
	 * Start new pass.
	 *
	 * @param string the string
	 */
	public void startNewPass(String string) {
		logStream.println(indent + "pass " + string + " {");
		indent();

	}

	/**
	 * Finish pass.
	 */
	public void finishPass() {
		deIndent();
		logStream.println(indent + "}");
	}

	/**
	 * Open brace.
	 *
	 * @param prefix the prefix
	 */
	public void openBrace(String prefix) {
		logStream.println(indent + prefix + "{");
		indent();
	}

	/**
	 * Close brace.
	 */
	public void closeBrace() {
		deIndent();
		logStream.println(indent + "}");
	}

	/**
	 * Start raw text.
	 */
	public void startRawText() {
		logStream.println(indent + "%{");
	}

	/**
	 * Prints the text.
	 *
	 * @param txt the txt
	 */
	public void printText(String txt) {
		logStream.println(indent + txt.replaceAll("\n", "\n" + indent));
	}

	/**
	 * Stop raw text.
	 */
	public void stopRawText() {
		logStream.println(indent + "}%");
	}

	/**
	 * Start script.
	 *
	 * @param filename the filename
	 */
	public void startScript(String filename) {
		try {
			logStream = new PrintStream(filename + ".glog");
			logStream.println(indent + "script " + filename + " {");
			indent();
		} catch (FileNotFoundException e) {
			System.err.println("Couldn't create logfile ");
			logStream = System.out;
		}
	}

	/**
	 * End script.
	 */
	public void endScript() {
		deIndent();
		logStream.println(indent + "}");
		logStream.flush();
		logStream.close();
	}

	public enum CommandEnum {
		StartScript, StartModule, StartLabeledGroup, StartRawText, EndScript, EndModule, EndLabeledGroup, EndRawText
	}

	public enum StateEnum {
		Idle, Script, Module, LabeledGroup, RawText
	}

	StateEnum state;
	private int labelDepth;

	/**
	 * Update FSM.
	 *
	 * @param cmd the cmd
	 */
	private void updateFSM(CommandEnum cmd) {
		switch (cmd) {
		case StartScript:
			switch (state) {
			case Idle:
				state = StateEnum.Script;
				break;
			default:
				System.err.println("Error in log state");
				break;
			}

		case EndScript:
			switch (state) {
			case Script:
				endScript();
				break;
			case RawText:
				closeBrace();
			case LabeledGroup:
				while (labelDepth > 0) {
					closeBrace();
					labelDepth--;
				}
			case Module:
				finishPass();
				endScript();
				System.err.println("Warning in log state");
				break;
			default:
				System.err.println("Error in log state");
				break;
			}

		default:
			System.err.println("Error in log state");
			break;
		}
	}

}
