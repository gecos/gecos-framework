/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/

package fr.irisa.r2d2.gecos.cseditor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

/** CSColorProvider is used to instantiate and cache
 * the color used for the syntax highlighting
 * 
 * @author llhours
 */
public class CSColorProvider {

	public static final RGB COMMENT = new RGB(  0,   0, 128);
	public static final RGB KEYWORD = new RGB(128,   0,  56);
	public static final RGB NUMBER  = new RGB(128,   0, 128);
	public static final RGB ARG     = new RGB(  0, 128,   0);
	public static final RGB STRING  = new RGB(200,   0,   0);
	public static final RGB DEFAULT = new RGB(  0,   0,   0);
	public static final RGB PRIM    = new RGB(  0, 128, 200);
	
	public static final RGB ASSIST  = new RGB(255, 255, 255);
	
	protected Map<RGB, Color> colorTable = new HashMap<RGB, Color>(10);

	public void dispose() {
		Iterator<Color> e = colorTable.values().iterator();
		while (e.hasNext())
			 e.next().dispose();
	}
	
	public Color getColor(RGB rgb) {
		Color color = colorTable.get(rgb);
		if (color == null) {
			color = new Color(Display.getCurrent(), rgb);
			colorTable.put(rgb, color);
		}
		return color;
	}
}
