/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.editor;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.ContentAssistAction;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;

import fr.irisa.r2d2.gecos.cseditor.CSColorProvider;
import fr.irisa.r2d2.gecos.cseditor.CSMessages;
import fr.irisa.r2d2.gecos.cseditor.text.CSSourceViewerConfiguration;

public class CSEditor extends TextEditor {
	
	public static final String EDITOR_ID = "fr.irisa.r2d2.gecos.cseditor.editor";
	
	public static final String EDITOR_CONTEXT = EDITOR_ID + ".context";

    private CSColorProvider colorProvider;

	public CSEditor() {
		super();
		colorProvider = new CSColorProvider();
		setSourceViewerConfiguration(
				new CSSourceViewerConfiguration(colorProvider, this));
	}
	
	protected void initializeEditor() {
        super.initializeEditor();
        //setHelpContextId(ICssEditorHelpContextIds.EDITOR);
        //setPreferenceStore(CssEditorPlugin.getDefault().getPreferenceStore());
        setEditorContextMenuId(EDITOR_CONTEXT);
        configureInsertMode(SMART_INSERT, true);
        setInsertMode(SMART_INSERT);
    }
	
	protected void createActions() {
        super.createActions();

        IAction action;

        // Content  assist action
        action = new ContentAssistAction(CSMessages.getResourceBundle(),
                "CSEditor.contentAssist.", this); //$NON-NLS-1$
        action.setActionDefinitionId(
            ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS);
        setAction(ICSEditorActionConstants.CONTENT_ASSIST, action);
        // Open Definition action
        action = new OpenDefinitionAction(CSMessages.getResourceBundle(),
                "CSEditor.openDefinition.", this); //$NON-NLS-1$
        action.setActionDefinitionId(ICSEditorActionConstants.OPEN_DEFINITION);
        setAction(ICSEditorActionConstants.OPEN_DEFINITION, action);
        // Toggle comment action 
        action = new ToggleCommentAction(CSMessages.getResourceBundle(), "CSEditor.toggleComment.", this);
        action.setActionDefinitionId(ICSEditorActionConstants.TOGGLE_COMMENT);
        setAction(ICSEditorActionConstants.TOGGLE_COMMENT, action);
    }
    
    protected void editorContextMenuAboutToShow(IMenuManager menu) {
        super.editorContextMenuAboutToShow(menu);
        menu.appendToGroup(ITextEditorActionConstants.GROUP_SAVE, new Separator(ITextEditorActionConstants.GROUP_OPEN));
        addAction(menu, ITextEditorActionConstants.GROUP_OPEN, ICSEditorActionConstants.OPEN_DEFINITION);
    }
}
