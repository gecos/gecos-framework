/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.assist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;

public class CompositeModuleIndexer implements IModuleIndexer {
	
	private List<IModuleIndexer> indexers;
	
	public CompositeModuleIndexer() {
		indexers = new ArrayList<IModuleIndexer>();
	}
	
	public void add(IModuleIndexer moduleIndexer) {
		indexers.add(moduleIndexer);
	}

	@Override
	public IModule getModule(String moduleName) {
		for (IModuleIndexer indexer : indexers) {
			IModule module = indexer.getModule(moduleName);
			if (module != null)
				return module;
		}
		return null;
	}

	@Override
	public Collection<IModule> getModulesStartingBy(String prefix) {
		return indexers.stream()
			.flatMap(idx -> idx.getModulesStartingBy(prefix).stream())
			.collect(Collectors.toList());
	}
	
	@Override
	public Collection<IModule> getModulesContaining(String substring) {
		return indexers.stream()
			.flatMap(idx -> idx.getModulesContaining(substring).stream())
			.collect(Collectors.toList());
	}

	@Override
	public Collection<IModule> getModules() {
		List<IModule> modules = new ArrayList<IModule>();
		for (IModuleIndexer indexer : indexers) {
			modules.addAll(indexer.getModules());
		}
		return modules;
	}

}
