/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.assist;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IAnnotatable;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMemberValuePair;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;
import fr.irisa.r2d2.gecos.framework.impl.GSAbstractModule;
import fr.irisa.r2d2.gecos.framework.impl.GSAbstractModuleArgument;
import fr.irisa.r2d2.gecos.framework.impl.GSAbstractModuleConstructor;

/**
 * Mainly inspired by org.eclipse.pde.internal.core.WorkspaceModelManager
 * 
 * @author llhours
 *
 */
public class WorkspaceModuleManager implements IModuleIndexer, IResourceChangeListener {

	private static final String PLUGIN_XML_FILENAME = "plugin.xml";
	private static final boolean DEBUG = false;
	
	private static WorkspaceModuleManager instance;
	
	public static WorkspaceModuleManager getDefault() {
		if (instance == null) {
			synchronized(WorkspaceModuleManager.class) {
				if (instance == null) {
					if(DEBUG) System.out.println("Create workspaceModuleManager");
					instance = new WorkspaceModuleManager();
					instance.scanWorkspace();
				}
			}
		}
		return instance;
	}
	
	private IResourceDeltaVisitor resourceChangeVisitor;
	private Map<IProject, WorkspacePluginProject> pluginProjects; // plug-in projects in the workspace that define at least one gecos module

	
	private WorkspaceModuleManager() {
		this.resourceChangeVisitor = new ResourceChangeVisitor();
		this.pluginProjects = new HashMap<IProject, WorkspacePluginProject>();
	}
	
	/**
	 * Scan the Workspace and construct a {@link WorkspacePluginProject}
	 * for each plug-in project.
	 * Workspace listen to resource changes.
	 */
	private void scanWorkspace() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IProject[] projects = workspace.getRoot().getProjects();
		for (int i = 0; i < projects.length; i++) {
			IProject project = projects[i];
			if(DEBUG) System.out.println("  scanning project: " + project.getName());
			if (project != null && project.isOpen())
				addWorkspacePluginProject(project);
		}
		workspace.addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);
	}
	
	/**
	 * If project is a plug-in project then parses its "plugin.xml" file
	 * to find and initialize all gecos module extensions.
	 * 
	 * @param project
	 * @return the constructed workspace plugin project or null if project is not valid.
	 */
	private synchronized WorkspacePluginProject addWorkspacePluginProject(IProject project) {
		if(DEBUG) System.out.println("    adding project to workspace " + project.getName());
		if(isPluginProject(project)) {
			WorkspacePluginProject model = WorkspacePluginProject.createPluginModel(project);
			if(DEBUG) System.out.println("    create pluginModel: ");
			if (model != null) {			
				pluginProjects.put(project, model);
				if(DEBUG) System.out.println("[ADD] " + project.getName());
				if(DEBUG) model.getModules().forEach(m -> System.out.println("   " + m));
				return model;
			}
		}
		else
			if(DEBUG) System.out.println("    <- NOT a PLUGIN!");
		return null;
	}
	
	private static boolean isPluginProject(IProject project) {
		return project.exists(new Path(PLUGIN_XML_FILENAME));
	}
	
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		try {
			event.getDelta().accept(this.resourceChangeVisitor);
		} catch (CoreException e) {
		}
	}

	private class ResourceChangeVisitor implements IResourceDeltaVisitor {
		@Override
		public boolean visit(IResourceDelta delta) throws CoreException {
			if (delta != null) {
				IResource resource = delta.getResource();
				if (resource instanceof IProject) {
					IProject project = (IProject) resource;
					if (delta.getKind() == IResourceDelta.ADDED
							|| (project.isOpen() && (delta.getFlags()&IResourceDelta.OPEN) != 0)) {
						addWorkspacePluginProject(project);
						return false;
					} else if (delta.getKind() == IResourceDelta.REMOVED) {
						removeWorkspacePluginProject(project);
						return false;
					}
				} else if (resource instanceof IFile) {
					IFile file = (IFile) resource;
					if (isPluginXmlFile(file)) {
						int kind = delta.getKind();
						switch (kind) {
						case IResourceDelta.ADDED:
							handlePluginFileAdded(file);
							break;
						case IResourceDelta.REMOVED:
							handlePluginFileRemoved(file);
							break;
						case IResourceDelta.CHANGED:
							handlePluginFileChanged(file, delta);
							break;
						}
					}
					else if(delta.getKind() == IResourceDelta.CHANGED) {
						handleSourceFileChanged(resource);
					}
				} else if (resource instanceof IFolder) {
					//return false; // do not go deeper than the root of the project
					if(resource.getName().equals("bin")) //do not visit "bin" folder
						return false;
				}
			}
			return true;
		}
		
		private boolean isPluginXmlFile(IFile file) {
			String name = file.getName().toLowerCase(Locale.ENGLISH);
			if (!name.equals(PLUGIN_XML_FILENAME)) //$NON-NLS-1$
				return false;
			IPath expectedPath = file.getProject().getFullPath().append(name);
			return expectedPath.equals(file.getFullPath());
		}
	}

	/**
	 * Initialize and Add the {@link WorkspacePluginProject} containing the pluginFile
	 * being added to the workspace.
	 * 
	 * @param pluginFile 
	 */
	private void handlePluginFileAdded(IFile pluginFile) {
		WorkspacePluginProject model = getWorkspacePluginProject(pluginFile);
		if (model == null) {
			addWorkspacePluginProject(pluginFile.getProject());
		}
	}
	
	/**
	 * Removes the {@link WorkspacePluginProject} containing the pluginFile
	 * being removed from the workspace.
	 * 
	 * @param pluginFile 
	 */
	private void handlePluginFileRemoved(IFile pluginFile) {
		WorkspacePluginProject model = getWorkspacePluginProject(pluginFile);
		if (model != null) {
			removeWorkspacePluginProject(pluginFile.getProject());
		}
	}
	
	/**
	 * Add/Update the {@link WorkspacePluginProject} containing the pluginFile
	 * being modified in the workspace.
	 * 
	 * @param pluginFile
	 * @param delta
	 */
	private void handlePluginFileChanged(IFile pluginFile, IResourceDelta delta) {
		if ((delta.getFlags() & IResourceDelta.CONTENT) != 0) {
			WorkspacePluginProject model = getWorkspacePluginProject(pluginFile);
			if (model == null) {
				/* in case the pluingFile did not declare any gecos Module before */
				model = addWorkspacePluginProject(pluginFile.getProject());
			} else {
				/* reload project to reflect changes */
				model.load();
			}
		}	
	}
	
	/**
	 * Update TODO the {@link WorkspacePluginProject} containing the resource
	 * being modified if it corresponds to a gecos module.
	 * 
	 * @param resource
	 */
	private void handleSourceFileChanged(IResource resource) {
		WorkspacePluginProject pluginProject = pluginProjects.get(resource.getProject());
		if(pluginProject != null) {
			IModule module = pluginProject.getModule(resource);
			if(module != null) {
				pluginProject.load(); //XXX do not reload the entire project
			}			
		}
	}
	
	private synchronized WorkspacePluginProject getWorkspacePluginProject(IFile file) {
		IProject project = file.getProject();
		IPath path = file.getProjectRelativePath();
		if (path.equals(new Path(PLUGIN_XML_FILENAME))) //$NON-NLS-1$
			return pluginProjects.get(project);
		return null;		
	}
	
	private synchronized void removeWorkspacePluginProject(IProject project) {
		pluginProjects.remove(project);
	}
	
	/**
	 * Workspace no longer listen to resource changes.
	 */
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
	}
	
	@Override 
	public synchronized Collection<IModule> getModulesStartingBy(String prefix) {
		String lc = prefix.toLowerCase();
		return pluginProjects.values().stream()
			.flatMap(proj -> proj.getModules().stream())
			.filter(mod -> mod.getName().toLowerCase().startsWith(lc))
//			.sorted((m1,m2) -> m1.getClassName().compareToIgnoreCase(m2.getClassName()))
			.collect(Collectors.toList());
	}
	
	@Override
	public Collection<IModule> getModulesContaining(String substring) {
		String lc = substring.toLowerCase();
		return pluginProjects.values().stream()
			.flatMap(proj -> proj.getModules().stream())
			.filter(mod -> mod.getName().toLowerCase().contains(lc))
			.collect(Collectors.toList());
	}
	
	@Override
	public synchronized IModule getModule(String moduleName) {
		for (WorkspacePluginProject project : pluginProjects.values()) {
			IModule module = project.getModule(moduleName);
			if(module != null)
				return module;
		}
		return null;
	}
	
	@Override
	public List<IModule> getModules() {
		List<IModule> modules = new ArrayList<IModule>();
		for (WorkspacePluginProject model : pluginProjects.values()) {
			modules.addAll(model.getModules());
		}
		return modules;
	}
	
	/**
	 * Returns an unmodifiable view of all gecos modules defined in the 
	 * specified project. If none are found returns an empty collection. 
	 * 
	 * @param project
	 * @return an unmodifiable view of gecos modules, empty if none are found.
	 */
	public Collection<IModule> getModules(IProject project) {
		WorkspacePluginProject model = pluginProjects.get(project);
		if (model == null)
			return Collections.emptyList();// new IModule[0];
		Collection<IModule> modules = model.getModules();
		return Collections.unmodifiableCollection(modules);
	}
	
	/**
	 * @return plug-in projects in the workspace that define one or more
	 * Gecos Modules.
	 */
	public Collection<IProject> getProjects() {
		return Collections.unmodifiableCollection(pluginProjects.keySet());
	}
	
	
	private static class WorkspacePluginProject {

		private IFile pluginFile;
		private IJavaProject project;
		private Map<String, IModule> name_modules_hash; // <moduleName, name>
		private Map<IResource, IModule> resource_modules_hash; //<resource containing the class definition of, Gecos module>
												 			   //XXX assume one Module definition per resource.  

		private WorkspacePluginProject(IFile pluginFile) {
			this.pluginFile = pluginFile;
			this.resource_modules_hash = new HashMap<>();
			this.name_modules_hash = new HashMap<>();
		}
		
		public static WorkspacePluginProject createPluginModel(IProject pluginProject) {
			IFile pluginFile = pluginProject.getFile(PLUGIN_XML_FILENAME);
			if (!pluginFile.exists()) // unnecessary test
				return null;
			WorkspacePluginProject model = new WorkspacePluginProject(pluginFile);
			if(DEBUG) System.out.println("    parsing plugin.xml... " + pluginFile);
			model.load();
			
			if(model.resource_modules_hash.isEmpty())
				return null;
			
			return model;
		}

		public Collection<IModule> getModules() {
			return resource_modules_hash.values();
		}
		
		public IModule getModule(IResource resource) {
			return resource_modules_hash.get(resource);
		}
		
		public IModule getModule(String moduleName) {
			return name_modules_hash.get(moduleName);
		}
		
		public void load() {
			try {
				InputStream stream = pluginFile.getContents(true);
				load(stream);
				stream.close();
			} catch (CoreException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private void load(InputStream stream) {
			try {
				SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
				resource_modules_hash.clear();
				name_modules_hash.clear();
				project = JavaCore.create(pluginFile.getProject());
				PluginXMLHandler handler = new PluginXMLHandler();
				if(DEBUG) System.out.println("       start parsing...");
				parser.parse(stream, handler);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		private class PluginXMLHandler extends DefaultHandler {

			private String currentPackageName;

			@Override
			public void startElement(String uri, String localName, String qName,
					Attributes attributes) throws SAXException {
				if (qName.equals("extension")) {
					String point = attributes.getValue("point");
					if (point != null && point.equals("fr.irisa.r2d2.gecos.framework.modules")) {
						currentPackageName = attributes.getValue("name");
						if (currentPackageName == null)
							currentPackageName = "$Unspecified$";
					}
				} else if (qName.equals("module")) {
					buildModule(attributes.getValue("name"), attributes.getValue("class"));
				} 
			}
			
			/** 
			 * Construct and initialize a Gecos module: 
			 * <li> {@link GSModule} is used to retrieve the module description
			 * <li> {@link GSModuleConstructor} is used to retrieve a module constructor description
			 * and its arguments using {@link GSArg}.
			 * */
			private IModule buildModule(String moduleName, String className) {
				try {
					if(DEBUG) System.out.println("         buildModule " + moduleName + " " + className);
					/* Find the java class implementing the module */
					IType type = project.findType(className, (IProgressMonitor)null);
					if (type == null)
						return null;
					
					/* A gecos module must have a 'compute[Iterative]' method */
					IMethod method = getComputeMethod(project, type);
					if (method == null) {
						System.err.println("[Gecos Module Indexer] Could not find '.compute()' method for Class '" +
								className + "' associated to Module '"+moduleName+"' in plugin '"+project.getProject().getName()+"'");
						return null;
					}
					boolean isIterative = method.getElementName().equals("computeIterative");
					
					/* Construct and initialize the module */
					GSAbstractModule module = new GSAbstractModule(moduleName, className, currentPackageName);
					module.setIterative(isIterative);
					module.setReturnType(method.getReturnType());
					
					if(getAnnotation(type, Deprecated.class.getSimpleName()) != null) {
						module.setDeprecated(true);
						module.appendDescription("Deprecared!\n");
					}
					IAnnotation moduleAnnotation = getAnnotation(type, GSModule.class.getSimpleName());
					if(moduleAnnotation != null) {
						for(IMemberValuePair val : moduleAnnotation.getMemberValuePairs()) {
//						if (val.getMemberName().equals(GSModule.DESCRIPTION_KEY))
							module.appendDescription((String) val.getValue());
						}
					}
					
					for (IMethod meth : type.getMethods()) {
						if (meth.isConstructor()) {
							IModuleConstructor cnst = new GSAbstractModuleConstructor(module);
							for (ILocalVariable param : meth.getParameters())
								cnst.getArguments().add(new GSAbstractModuleArgument(param.getTypeSignature(),
										Signature.toString(param.getTypeSignature()), param.getElementName())); //XXX
							
							if(getAnnotation(meth, Deprecated.class.getSimpleName()) != null) {
								cnst.setDeprecated(true);
								cnst.appendDescription("Deprecared!\n");
							}
							IAnnotation cnstAnnotation = getAnnotation(meth, GSModuleConstructor.class.getSimpleName());
							if(cnstAnnotation != null) {
								for(IMemberValuePair val : cnstAnnotation.getMemberValuePairs()) {
									switch (val.getMemberName()) {
									case GSModuleConstructor.DESCRIPTION_KEY:
										cnst.appendDescription((String) val.getValue());
										break;
									case GSModuleConstructor.ARGS_KEY:
										Object[] argAnnotations = (Object[]) val.getValue();
										for(int i=0; i< argAnnotations.length; i++) {
											IAnnotation argAnnot = (IAnnotation) argAnnotations[i];
											String info = Arrays.stream(argAnnot.getMemberValuePairs())
												.filter(p -> p.getMemberName().equals(GSArg.INFO_KEY))
												.map(p -> (String) p.getValue())
												.findFirst().orElse("");
											cnst.getArguments().get(i).setDescription(info);
										}
										break;
									default: break;
									}
								}
							}
							module.getConstructors().add(cnst);
						}
					}
					
					if(module != null) {
						resource_modules_hash.put(type.getResource(), module);
						name_modules_hash.put(module.getName(), module);
					}
					
					return module;
				} 
				catch (JavaModelException e) { 
					e.printStackTrace();
				}
				return null;
			}

			protected IAnnotation getAnnotation(IAnnotatable annotatble, String name) throws JavaModelException {
				IAnnotation anot = Arrays.stream(annotatble.getAnnotations())
					.filter(an -> an.getElementName().equals(name))
					.findFirst().orElse(null);
				return anot;
			}
			
			private IMethod getComputeMethod(IJavaProject project, IType type) throws JavaModelException {
				IMethod method = null;
				for (IMethod meth : type.getMethods()) {
					if (!meth.isConstructor() && meth.getNumberOfParameters() == 0) {
						if (meth.getElementName().equals("compute")
								|| meth.getElementName().equals("computeIterative")) {
							method = meth;
							break;
						}
					}
				}
				// if no compute method look in super type
				if (method == null) {
					String[][] superTypes = type.resolveType(Signature.getSignatureSimpleName(type.getSuperclassTypeSignature()));
					if (superTypes != null)
						for (String[] element : superTypes) {
							if (element != null) {
								type = project.findType(element[0], element[1], (IProgressMonitor) null);
								if (type != null)
									return getComputeMethod(project,type); //recursive call
							}
						}
					if (type == null)
						return null;
				}
				return method;
			}
			
		}
	}
	
}
