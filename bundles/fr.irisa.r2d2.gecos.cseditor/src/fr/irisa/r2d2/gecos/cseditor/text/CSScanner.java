/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWhitespaceDetector;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordPatternRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;

import fr.irisa.r2d2.gecos.cseditor.CSColorProvider;
import fr.irisa.r2d2.gecos.framework.parser.GecosTokenNames;
import fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser;


public class CSScanner extends RuleBasedScanner {
	
	public static String[] Primitives = SemanticAnalyser.Primitives.toArray(new String[SemanticAnalyser.Primitives.size()]);
	
	private class WhitespaceDetector implements IWhitespaceDetector {

		public boolean isWhitespace(char character) {
			return Character.isWhitespace(character);
		}
	}
	
	private class ArgDetector implements IWordDetector {

		public boolean isWordPart(char character) {
			return Character.isDigit(character);
		}
		
		public boolean isWordStart(char character) {
			return character == '$';
		}
	}
	
	private class WordDetector implements IWordDetector {

		public boolean isWordPart(char character) {
			return isLetter(character) || Character.isDigit(character);
		}
		
		public boolean isWordStart(char character) {
			return isLetter(character);
		}
		
		private boolean isLetter(char character) {
			return Character.isLetter(character) || character == '_';
		}
	}
	
	private class NumberDetector implements IWordDetector {

		public boolean isWordPart(char character) {
			return Character.isDigit(character);
		}
		
		public boolean isWordStart(char character) {
			return Character.isDigit(character);
		}
	}
	
	private class HexNumberDetector implements IWordDetector {

		public boolean isWordPart(char character) {
			return Character.isDigit(character) || isHex(character);
		}
		
		public boolean isWordStart(char character) {
			return Character.isDigit(character) || isHex(character);
		}
		
		private boolean isHex(char c) {
			return Character.toLowerCase(c) >= 'a'
				&& Character.toLowerCase(c) <= 'f';
		}
	}

	public CSScanner(CSColorProvider manager) {
		
		IToken keyword = new Token(new TextAttribute(manager.getColor(CSColorProvider.KEYWORD), null, SWT.BOLD));
		IToken prim    = new Token(new TextAttribute(manager.getColor(CSColorProvider.PRIM)));
		IToken arg     = new Token(new TextAttribute(manager.getColor(CSColorProvider.ARG)));
		IToken string  = new Token(new TextAttribute(manager.getColor(CSColorProvider.STRING)));
		IToken number  = new Token(new TextAttribute(manager.getColor(CSColorProvider.NUMBER)));
		IToken comment = new Token(new TextAttribute(manager.getColor(CSColorProvider.COMMENT)));
		IToken other   = new Token(new TextAttribute(manager.getColor(CSColorProvider.DEFAULT)));
		
		IRule[] rules = new IRule[7];
		
		WordRule wordRule = new WordRule(new WordDetector(), other);
		for (int i = 0; i < GecosTokenNames.Keywords.length; i++)
			wordRule.addWord(GecosTokenNames.Keywords[i], keyword);
		for (int i = 0; i < Primitives.length; i++)
			wordRule.addWord(Primitives[i], prim);

		rules[0] = wordRule;
		rules[1] = new EndOfLineRule("#", comment); //$NON-NLS-1$
		rules[2] = new WordRule(new ArgDetector(), arg);
		rules[3] = new WordPatternRule(new HexNumberDetector(), "0x", null, number);
		rules[4] = new WordRule(new NumberDetector(), number);
		rules[5] = new SingleLineRule("\"", "\"", string, '\\'); //$NON-NLS-2$ //$NON-NLS-1$
		rules[6] = new WhitespaceRule(new WhitespaceDetector());

		setRules(rules);
		
		setDefaultReturnToken(other);
	}
}
