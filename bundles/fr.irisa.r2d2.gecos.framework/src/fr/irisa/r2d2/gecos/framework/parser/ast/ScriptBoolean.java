


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

public class ScriptBoolean extends ScriptNode {


	private Boolean value;
	
	public ScriptBoolean(Span span, Boolean value) {
		super(span);
		this.value = value;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptBoolean(this);
	}
}