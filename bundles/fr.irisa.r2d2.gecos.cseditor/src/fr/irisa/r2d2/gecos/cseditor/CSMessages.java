/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor;

import java.util.ResourceBundle;

public class CSMessages {

	private static final String RESOURCE_BUNDLE =
        "fr.irisa.r2d2.gecos.cseditor.CSMessages"; //$NON-NLS-1$

    private static ResourceBundle fgResourceBundle =
        ResourceBundle.getBundle(RESOURCE_BUNDLE);

    public static String OpenAction_error_messageBadSelection;
    
    private CSMessages() {
        // Hidden
    }
    
    public static ResourceBundle getResourceBundle() {
        return fgResourceBundle;
    }
}
