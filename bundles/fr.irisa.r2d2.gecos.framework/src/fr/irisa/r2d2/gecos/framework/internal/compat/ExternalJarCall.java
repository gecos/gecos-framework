/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.compat;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Iterator;

import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.parser.runtime.Span;

public class ExternalJarCall implements IScriptEvaluable {

	/**
	 * Instantiates a new external jar call.
	 */
	public ExternalJarCall() { }
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args) throws ScriptException {
		if (args.length != 1)
			throw new ScriptException("expecting jar name in external");
		load(evaluator, args[0].asString().value());
		return new NullNode(span);
	}
	
	/**
	 * Load.
	 *
	 * @param evaluator the evaluator
	 * @param jarname the jarname
	 * @throws ScriptException the script exception
	 */
	@SuppressWarnings("deprecation")
	private void load(IScriptEvaluator evaluator, String jarname) throws ScriptException{
		File file = new File(jarname);
		if (! file.exists())
			throw new ScriptException(" external '" + jarname +"' does not exists");
		try {
			ClassLoader loader = new URLClassLoader(new URL[] { file.toURL() },
					getClass().getClassLoader());
			InputStream stream = loader.getResourceAsStream("contents.xml");
			TransformsReader reader = new TransformsReader(loader);
			Iterator<?> i = reader.read(new InputStreamReader(stream)).iterator();
			while (i.hasNext()) {
				FuncTransform func = (FuncTransform) i.next();
				evaluator.registerFunction(func);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new ScriptException("external: cannot load '" + jarname + "'");
		}
	}
	
}
