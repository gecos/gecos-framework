/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class MappingNode extends Node {

	private Node key;
	private Node value;

	/**
	 * Instantiates a new mapping node.
	 *
	 * @param span the span
	 * @param key the key
	 * @param value the value
	 */
	public MappingNode(Span span, Node key, Node value) {
		super(span);
		this.key = key;
		this.value = value;
	}

	/**
	 * Key.
	 *
	 * @return the node
	 */
	public Node key() {
		return key;
	}

	/**
	 * Value.
	 *
	 * @return the node
	 */
	public Node value() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asString()
	 */
	public StringNode asString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(key.asString().value());
		buffer.append(" -> ");
		buffer.append(value.asString().value());
		return new StringNode(span, buffer.toString());
	}
	
}