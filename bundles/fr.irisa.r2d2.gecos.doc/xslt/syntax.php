<?php

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_gecos extends DokuWiki_Syntax_Plugin {


	function getInfo(){
		return array(
			'author' => 'Ludovic L\'Hours',
			'email'  => 'llhours@irisa.fr',
			'date'   => '2008-01-12',
			'name'   => 'Gecos Plugin',
			'desc'   => 'Parses gecos-blocks',
			'url'    => 'http://no-pasaran.irisa.fr',
		);
	}

	/**
	* What kind of syntax are we?
	*/
	function getType(){
		return 'formatting';
	}
   
	/**
	* Where to sort in?
	*/ 
	function getSort(){
		return 100;
	}


    /**
     * Connect pattern to lexer
     */
    function connectTo($mode) {
        $this->Lexer->addEntryPattern('<tag(?=.*\x3C/tag\x3E)',$mode,'plugin_gecos');
    }

		function postConnect() {
			$this->Lexer->addExitPattern('</tag>','plugin_gecos');
		}

		/**
		 * Handle the match
		 */
    function handle($match, $state, $pos) {
			if ( $state == DOKU_LEXER_UNMATCHED ) {
				$matches = preg_split('/>/u',$match,2);
				$matches[0] = trim($matches[0]);
				if ( trim($matches[0]) == '' ) {
					$matches[0] = NULL;
				}
				return array($matches[1],$matches[0]);
			}
			return array('', '');
    }
    /**
     * Create output
     */
    function render($mode, &$renderer, $data) {
			if($mode == 'xhtml' && strlen($data[0]) > 0) {
				$renderer->doc .= '<span class="'.$data[1].'">'.$data[0].'</span>';
				return true;
			}
			return false;
		}

}

?>
