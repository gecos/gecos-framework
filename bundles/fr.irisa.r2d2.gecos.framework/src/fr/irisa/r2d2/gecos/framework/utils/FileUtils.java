package fr.irisa.r2d2.gecos.framework.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import com.google.common.io.Files;

/**
 * 
 * @author aelmouss
 */
public class FileUtils {
	
    /**
     * Copies file to the specified directory. 
     * If 'toDir' does not exist it will be created.
     * If a similar file already exist at 'tiDir' it will be 
     * overwritten if override flag is set otherwise the operation
     * fails.
     * 
     * @param file
     * @param toDir
     * @throws RuntimeException if 'file' is not found or copy failed.
     * 
     * @author aelmouss
     */
    public static void copyFile(File file, File toDir, boolean override) {
    	if(!file.exists())
    		throw new RuntimeException("File is not found!" + file);
    	if(!toDir.exists())
			toDir.mkdirs();
    	File copy = new File(toDir, file.getName());
    	if(copy.exists() && !override)
    		throw new RuntimeException("File already exists at destination! Use override. " + file);
    	if(file.equals(copy))
    		return;
    	try {
			Files.copy(file, copy);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }

    /**
     * Copy all files (exclude directories) in 'fromDir' directory passing the specified filter into
     * the 'toDir' directory.
     * <br>If 'toDir' does not exist it will be created (including parents if necessary).
     * <br>If a file already exists at 'toDir' it will be overwritten when
     * override is true.
     * 
     * @param fromDir
     * @param toDir
     * @param filter
     * @param override
     * @throws RuntimeException if 'fromDir' is not found or is not a directory.
     * 
     * @author aelmouss
     */
	public static void copyAllFiles(File fromDir, File toDir, FilenameFilter filter, boolean override) {
		if(!fromDir.exists() || !fromDir.isDirectory())
			throw new RuntimeException("Source directory is not found!" + fromDir);
		if(!toDir.exists())
			toDir.mkdirs();
		Arrays.stream(fromDir.listFiles(filter))
			.filter(f -> !f.isDirectory())
			.forEach(f -> copyFile(f, toDir, override));
	}

	/**
	 * Crash if path is a folder. Create hierarchy. Overwrite if present.
	 */
	public static void write(String path, CharSequence content) {
//		File f = new File(path);
//		if (f.exists()) {
//			if (f.isFile())
//				f.delete();
//			else 
//				throw new RuntimeException();
//		} else {
//			f.mkdirs();
//			f.delete();
//		}
//		try {
//			f.createNewFile();
//			FileWriter fw = new FileWriter(f);
//			fw.append(content);
//			fw.close();
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
		try {
			Files.write(content.toString().getBytes(), new File(path));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Recursively walk root (depth-first) and return the set of all files (excluding directories) 
	 * in the hierarchy up to depth specified by {@code maxDepth}.
	 * Skip all directories that does not pass {@code dirFilter}.
	 * Skip all files that does not pass {@code fileFilter}.
	 * If {@code maxDepth} is 0, only direct files in {@code root} are visited.
	 * {@link Integer#MAX_VALUE} can be used to specify an infinite depth.
	 * 
	 * @param rootDir the root directory
	 * @param dirFilter should return true if a tested directory shall pass
	 * @param fileFilter should return true if a tested file shall pass
	 * @param maxDepth specifies a threshold depth.
	 * 
	 * @return the Set of files, never <code>null</code>
	 * @throws RuntimeException if failed.
	 * 
	 * @author aelmouss
	 */
	public static List<File> getRecursiveFiles(File rootDir, Predicate<File> dirFilter, Predicate<File> fileFilter, int maxDepth) {
		if(!rootDir.isDirectory())
			throw new RuntimeException("The specified directory does not exist! " + rootDir);
		if(maxDepth < 0)
			throw new RuntimeException("The maximum depth cannot be negative: " + maxDepth);
		
		List<File> files = new ArrayList<>();
		for(File f : rootDir.listFiles())
			_getRecursiveFiles(f, files, dirFilter, fileFilter, maxDepth);
		return files;
	}
	
	private static List<File> _getRecursiveFiles(File file, List<File> files, Predicate<File> dirFilter, Predicate<File> fileFilter, int maxDepth) {
		if(maxDepth >= 0 && file.isDirectory() && dirFilter.test(file))
			Arrays.stream(file.listFiles()).forEach(f -> _getRecursiveFiles(f, files, dirFilter, fileFilter, maxDepth-1));
		else if(fileFilter.test(file))
			files.add(file);
		return files;
	}
	
	public static void sed(String path, CharSequence oldPattern, CharSequence newPattern) {
		File f = new File(path);
		if (!f.isFile())
			throw new RuntimeException("File not found: " + path);
		
		try {
			String content = "";
			BufferedReader reader = new BufferedReader(new FileReader(f));
			while (reader.ready()) {
				content += reader.readLine()+"\n";
			}
			reader.close();
			content = content.replace(oldPattern, newPattern);
			System.out.println(content);
			write (path,content);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * @param file1
	 * @param file2
	 * @return true if files contain the same bytes
	 * @throws RuntimeException if failed to access files.
	 * 
	 * @author aelmouss
	 */
	public static boolean areFilesEquals(File file1, File file2) {
		try {
			return Files.equal(file1, file2);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static boolean areFilesEquals(String file1, String file2) {
//		boolean areEquals = false;
//		try {
//			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(file1));
//			BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file2));
//			int int1;
//			int int2;
//			do {
//				int1 = bufferedReader1.read();
//				int2 = bufferedReader2.read();
//				areEquals = int1 == int2;
//			} while(areEquals && int1 != -1 && int2 != -1);
//			bufferedReader1.close();
//			bufferedReader2.close();
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//		return areEquals;
		
//		return DiffJava.doDiff(file1, file2);
			
		return areFilesEquals(new File(file1), new File(file2));
	}
	
	public static String basename(String file) {
		return basename(new File(file));
	}
	
	public static String basename(File file) {
		return file.getName();
	}
	
	public static String dirname(String file) {
		return dirname(new File(file));
	}
	
	public static String dirname(File file) {
		return file.getParent();
	}
	
	/**
	 * Shortcut to {@link #deleteRecursive(File)}
	 */
	public static boolean deleteRecursive(String str) {
		 return deleteRecursive(new File(str));
    }
	
	/**
	 * Delete the specified file or directory (assuming is exists).
	 * In case it is a directory its content is recursively deleted first.
	 * 
	 * @param path
	 * @return true if and only if the file or directory is successfully deleted; false otherwise
	 */
	public static boolean deleteRecursive(File path) {
        boolean ret = true;
        if (path.isDirectory()){
            for (File f : path.listFiles()){
                ret = ret && deleteRecursive(f.getAbsolutePath());
            }
        }
        return ret && path.delete();
	}
	
	/**
	 * Delete the specified directory's content (assuming is exists).
	 */
	public static boolean deleteDirContent(File dir) {
		boolean ret = true;
		if (dir.isDirectory()){
            for (File f : dir.listFiles()){
                ret = ret && deleteRecursive(f.getAbsolutePath());
            }
        }
		return ret;
	}

	//XXX
    public static String generateDeleteOnExitDirName() {
		File f = null;
		try {
			f = File.createTempFile("gecos_", "_tmpdir");
			f.delete();
			f.mkdir();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
    	String absolutePath = f.getAbsolutePath();
    	return absolutePath;
    }
    
    //XXX
    public static String generateDeleteOnExitFileName() {
    	return generateDeleteOnExitFileName("");
	}
    
    //XXX
    public static String generateDeleteOnExitFileName(String ext) {
		File f = null;
		try {
			f = File.createTempFile("gecos_", "_tmp"+ext);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
    	f.deleteOnExit();
    	String absolutePath = f.getAbsolutePath();
		return absolutePath;
	}

}
