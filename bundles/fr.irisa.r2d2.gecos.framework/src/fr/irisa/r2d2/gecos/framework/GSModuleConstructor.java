package fr.irisa.r2d2.gecos.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface GSModuleConstructor {
	
	String DESCRIPTION_KEY = "value";
	String ARGS_KEY = "args";


	/**
	 * Constructor-specific description.
	 *
	 * @return the string
	 */
	String value() default "";
	
	
	//TODO: use GSArg annotations
	
//	String description() default "";
	
	/**
	 * Array of constructor parameter annotations.
	 * The order of annotations is assumed to correspond to the order 
	 * of the constructor parameters (i.e. ith annotation is considered as 
	 * annotation for the ith parameter).
	 *
	 * @return the GS arg[]
	 */
	public GSArg[] args() default {};

}


