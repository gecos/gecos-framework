package fr.irisa.r2d2.parser.runtime;


public interface ITokenCategorizer {

	TokenCategory getCategory(IToken token);

}
