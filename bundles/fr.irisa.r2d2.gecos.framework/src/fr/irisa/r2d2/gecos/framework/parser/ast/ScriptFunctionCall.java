


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
@SuppressWarnings("all")
public class ScriptFunctionCall extends ScriptNode {

	/**
	 * @generated
	 */
	protected AstList<ScriptNode> arguments ;
	
	/**
	 * @generated
	 */
	private ScriptIdent name;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptFunctionCall(Span span, ScriptIdent name, AstList arguments) {
		super(span);
		this.name = name;
		this.arguments = arguments;
	}
	
	/**
	 * @generated
	 */
	public ScriptIdent getName() {
		return name;
	}
	
	public AstList<ScriptNode> getArguments() {
		return arguments;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptFunctionCall(this);
	}
	
}