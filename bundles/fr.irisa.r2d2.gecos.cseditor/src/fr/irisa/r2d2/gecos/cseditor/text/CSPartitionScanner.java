/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;

public class CSPartitionScanner extends RuleBasedPartitionScanner {

	public static final String CS_COMMENT = "__cs_comment_partition_content_type";
	public static final String CS_COMMENT_PREFIX = "#";
	
	public static final String[] CS_PARTITIONING_TYPES = {
		CS_COMMENT
	};

	public static final String CS_PARTITIONING = "__cs_partitioning";
	
	public CSPartitionScanner() {
		IToken commentToken = new Token(CS_COMMENT);
		IPredicateRule[] rules = new IPredicateRule[] {
			new EndOfLineRule(CS_COMMENT_PREFIX, commentToken),
			new SingleLineRule("\"", "\"", Token.UNDEFINED, '\0', true),
		};
		setPredicateRules(rules);
	}
}
