/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.irisa.r2d2.gecos.framework.IOptionsManager;

public class OptionsManager implements IOptionsManager {
	
	private static class Option {

		public String name;
		private int type;
		public String key;

		/**
		 * Instantiates a new option.
		 *
		 * @param name the name
		 * @param key the key
		 * @param type the type
		 * @param helpString the help string
		 */
		public Option(String name, String key, int type, String helpString) {
			this.name = name;
			this.type = type;
			this.key = key;
		}

		/**
		 * Checks for argument.
		 *
		 * @return true, if successful
		 */
		public boolean hasArgument() {
			return type != 0;
		}
	}
	
	private List<Option> options = new ArrayList<Option>();
	private Properties properties = new Properties();

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IOptionsManager#add(java.lang.String, java.lang.String, int, java.lang.String)
	 */
	public void add(String option, String key, int type, String helpString) {
		options.add(new Option(option, key, type, helpString));
	}
	
	/**
	 * Parses the.
	 *
	 * @param arguments the arguments
	 * @throws ParseException the parse exception
	 */
	public void parse(List<String> arguments) throws ParseException {
		List<String> others = new ArrayList<String>();
		for (int i = 0; i < arguments.size(); ++i) {
			String argument = arguments.get(i);
			if (argument.length() == 0)
				continue;
			if (argument.charAt(0) != '-') {
				others.add(argument);
				continue;
			}
			Option option = findOption(argument.substring(1));
			if (option == null)
				throw new ParseException("unknown option '" + argument + "'");
			if (option.hasArgument()) {
				if (i + 1 >= arguments.size())
					throw new ParseException("missing argument ");
				properties.setProperty(option.key, arguments.get(++i));
			} else {
				properties.setProperty(option.key, "true");
			}
		}
		properties.put(OTHER_ARGUMENTS, others.toArray(new String[others.size()]));
	}
	
	/**
	 * Find option.
	 *
	 * @param optionName the option name
	 * @return the option
	 */
	private Option findOption(String optionName) {
		for (Option option : options) {
			if (option.name.startsWith(optionName))
				return option;
		}
		return null;
	}

	/**
	 * Gets the options.
	 *
	 * @return the options
	 */
	public Properties getOptions() {
		return properties;
	}

}
