/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class IntegerNode extends Node {
	private int value;
	
	/**
	 * Instantiates a new integer node.
	 *
	 * @param span the span
	 * @param value the value
	 */
	public IntegerNode(Span span, int value) {
		super(span);
		this.value = value;	
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asBoolean()
	 */
	public boolean asBoolean() {
		return value != 0;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject()
	 */
	public Object asObject() { return asInteger(); }
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asInteger()
	 */
	public Integer asInteger() { return new Integer(value); }
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asString()
	 */
	public StringNode asString() {
		return new StringNode(span, Integer.toString(value));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#applyPlus(fr.irisa.r2d2.gecos.framework.script.nodes.Node)
	 */
	public Node applyPlus(Node right) {
		if (right instanceof IntegerNode)
			return new IntegerNode(span, value + right.asInteger());
		return new StringNode(span, String.valueOf(value) + right.asString().value());
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#applyMinus(fr.irisa.r2d2.gecos.framework.script.nodes.Node)
	 */
	public Node applyMinus(Node right) {
		return new IntegerNode(span, value - right.asInteger());
	}
}