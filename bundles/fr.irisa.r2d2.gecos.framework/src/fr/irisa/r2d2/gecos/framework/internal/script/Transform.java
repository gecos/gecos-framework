/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.script;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.ArrayNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.BoolNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.FloatNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.IntegerNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NodeFactory;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.ObjectNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.StringNode;
import fr.irisa.r2d2.gecos.framework.utils.ScriptLog;
import fr.irisa.r2d2.parser.runtime.Span;

public class Transform implements IScriptEvaluable {

//	private static final boolean debug = GecosInternalFramework.debugMode > 1;
	private static final boolean debug = false;
	
	private static class TransformConstructor {
		Constructor<?> cnst;
		Class<?>[] argsClass;
		private Class<?>[] testsClass;
		
		/**
		 * Instantiates a new transform constructor.
		 *
		 * @param cnst the cnst
		 */
		public TransformConstructor(Constructor<?> cnst) {
			this.cnst = cnst;
			this.argsClass = cnst.getParameterTypes();
			this.testsClass = new Class[argsClass.length];
			for (int i = 0; i < testsClass.length; ++i) {
				if (argsClass[i] == Integer.TYPE) {
					testsClass[i] = Integer.class;
				} else if (argsClass[i] == Boolean.TYPE) {
					testsClass[i] = Boolean.class;
				} else if (argsClass[i] == Float.TYPE) {
					testsClass[i] = Double.class;
				} else if (argsClass[i] == Double.TYPE) {
					testsClass[i] = Double.class;
				} else {
					testsClass[i] = argsClass[i];
				}
			}
		}
		
	}
	
	private String name;
	private List<TransformConstructor> constructors;
	private boolean isIterative;

	private Method method;

	/**
	 * Instantiates a new transform.
	 *
	 * @param name the name
	 * @param method the method
	 * @param isIterative the is iterative
	 */
	public Transform(String name, Method method, boolean isIterative) {
		this.name = name;
		this.isIterative = isIterative;
		this.method = method;
		this.constructors = new ArrayList<TransformConstructor>();
	}
	
	/**
	 * Adds the constructor.
	 *
	 * @param cnst the cnst
	 */
	public void addConstructor(Constructor<?> cnst) {
		constructors.add(new TransformConstructor(cnst));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {

		Object[] objArgs = new Object[args.length];

		//Error reporting
		String argStrList = "(";
		boolean first = true;
		for (Node arg : args) {
			if (first) first = false;
			else argStrList += ",";
			Object asObject = arg.asObject();
			if (asObject == null)
				throw new RuntimeException("Could not transform as object ["+arg+"]");
			Class<? extends Object> class1 = asObject.getClass();
			argStrList += class1.getSimpleName();
		}
		argStrList += ")";
		Set<Class<?>> clss = new HashSet<>();
		for(TransformConstructor constructor : constructors) {
			Class<?> declaringClass = constructor.cnst.getDeclaringClass();
			clss.add(declaringClass);
		}
		String clsList = "";
		for (Class<?> c : clss) {
			clsList += "\n  > "+c.getCanonicalName();
		}//error reporting end
		
		if (debug) System.out.println("\n\nlooking for a constructor for call for '" + name+argStrList + "' implemented with class :"+clsList);
		
		TransformConstructor tc = findConstructor(args, objArgs);
		if (tc == null) {
			throw new ScriptException("Cannot find matching constructor in call to '" + name+argStrList + "' implemented with class :"+clsList);
		}
		try {
			Object transform = tc.cnst.newInstance(objArgs);
			ScriptLog.getInstance().startNewPass(name+':'+transform.getClass().getSimpleName()); 
			Object res = method.invoke(transform, new Object[0]);
			if (isIterative) {
				evaluator.setChanging(((Boolean)res).booleanValue());
				return new NullNode(span);
			}
			ScriptLog.getInstance().finishPass();
			return NodeFactory.build(span,res);
		} catch(Exception e) {
			if (e instanceof InvocationTargetException)
				throw new ScriptException("error while invoking '" + name+ "':\n" + e.getCause().getMessage(),e.getCause());
			else
				throw new ScriptException("error while invoking '" + name+ "':\n" + e.getMessage(),e);
		}
	}

	/**
	 * Find constructor.
	 *
	 * @param args the args
	 * @param objArgs the obj args
	 * @return the transform constructor
	 */
	private TransformConstructor findConstructor(Node[] args, Object[] objArgs) {
		for (TransformConstructor tc : constructors) {
			if (testConstructorStrict(tc, args, objArgs))
				return tc;
		}
		for (TransformConstructor tc : constructors) {
			if (testConstructor(tc, args, objArgs))
				return tc;
		}
		return null;
	}

	/**
	 * testConstructor allows types that are assignable to be used.
	 * This method tests for strict match in case of overloading that is ambiguous in the testConstructor implementation. 
	 *
	 * @param tc the tc
	 * @param args the args
	 * @param objArgs the obj args
	 * @return true, if successful
	 */
	private boolean testConstructorStrict(TransformConstructor tc, Node[] args,
			Object[] objArgs) {
		if (tc.argsClass.length != args.length)
			return false;

		if (debug) {
			System.out.println("strict testing constructor for "+tc.cnst.getDeclaringClass().getSimpleName());
			System.out.println("constructor arguments : "+Arrays.asList(tc.argsClass));
			System.out.println("given arguments : "+Arrays.asList(args));
		}
		
		for (int i = 0; i < args.length; ++i) {
			if (debug)
				System.out.println("  - argument #"+i);
			/**
			 * Test for raw type arguments
			 */
			// strict integer matching
			if (tc.argsClass[i].equals(Integer.TYPE) && (args[i] instanceof IntegerNode)) {
				if (debug) System.out.println("rule 1.a");
				objArgs[i] = args[i].asInteger();
			
			// float as double matching
			} else if (tc.argsClass[i].equals(Float.TYPE) && (args[i] instanceof FloatNode)) {
				if (debug) System.out.println("rule 1.b");
				objArgs[i] = args[i].asFloat();
			
			// strict double matching matching
			} else if (tc.argsClass[i].equals(Double.TYPE) && (args[i] instanceof FloatNode)) {
				if (debug) System.out.println("rule 1.c");
				objArgs[i] = args[i].asFloat();
			
			// strict boolean matching
			} else if (tc.argsClass[i].equals(Boolean.TYPE) && (args[i] instanceof BoolNode)) {
				if (debug) System.out.println("rule 1.d");
				objArgs[i] = args[i].asBoolean();
			
			// strict string matching
			} else if (tc.argsClass[i].equals(String.class) && (args[i] instanceof StringNode)) {
				if (debug) System.out.println("rule 1.e");
				objArgs[i] = args[i].asString().value();

			
			/**
			 * Test for elipses / arrays with raw type arguments
			 */
			// single integer as integer array matching
			} else if ((tc.argsClass[i].equals(Integer[].class) || tc.argsClass[i].equals(int[].class)) && (args[i] instanceof IntegerNode)) {
				if (debug) System.out.println("rule 2.a");
				objArgs[i] = new Integer[] {args[i].asInteger()};
			
			// single float as double array matching
			} else if ((tc.argsClass[i].equals(Float[].class) || tc.argsClass[i].equals(float[].class)) && (args[i] instanceof FloatNode)) {
				if (debug) System.out.println("rule 2.b");
				objArgs[i] = new Double[] {args[i].asFloat()};
			
			// single double as double array matching
			} else if ((tc.argsClass[i].equals(Double[].class) || tc.argsClass[i].equals(double[].class)) && (args[i] instanceof FloatNode)) {
				if (debug) System.out.println("rule 2.c");
				objArgs[i] = new Double[] {args[i].asFloat()};
			
			// single boolean as boolean array matching
			} else if ((tc.argsClass[i].equals(Boolean[].class) || tc.argsClass[i].equals(boolean[].class)) && (args[i] instanceof BoolNode)) {
				if (debug) System.out.println("rule 2.d");
				objArgs[i] = new Boolean[] {args[i].asBoolean()};

			// single string as string array matching
			} else if (tc.argsClass[i].equals(String[].class) && (args[i] instanceof StringNode)) {
				if (debug) System.out.println("rule 2.e");
				objArgs[i] = new String[] {args[i].asString().value()};
			
			
			/**
			 * Test for elipses / arrays with raw type arrays
			 */
			// array node as integer array matching
			} else if ((tc.argsClass[i].equals(Integer[].class) || tc.argsClass[i].equals(int[].class)) && (args[i] instanceof ArrayNode)) {
				if (debug) System.out.println("rule 3.a");
				List<Integer> l = new ArrayList<>();
				for (Iterator<Node> it = ((ArrayNode)args[i]).getElements().iterator(); it.hasNext();)
					l.add(it.next().asInteger());
				Integer[] a = l.toArray(new Integer[l.size()]);
				objArgs[i] = a;

			// array node as double array matching
			} else if ((tc.argsClass[i].equals(Float[].class) || tc.argsClass[i].equals(float[].class)) && (args[i] instanceof ArrayNode)) {
				if (debug) System.out.println("rule 3.b");
				List<Double> l = new ArrayList<>();
				for (Iterator<Node> it = ((ArrayNode)args[i]).getElements().iterator(); it.hasNext();)
					l.add(it.next().asFloat());
				Double[] a = l.toArray(new Double[l.size()]);
				objArgs[i] = a;

			// array node as double array matching
			} else if ((tc.argsClass[i].equals(Double[].class) || tc.argsClass[i].equals(double[].class)) && (args[i] instanceof ArrayNode)) {
				if (debug) System.out.println("rule 3.c");
				List<Double> l = new ArrayList<>();
				for (Iterator<Node> it = ((ArrayNode)args[i]).getElements().iterator(); it.hasNext();)
					l.add(it.next().asFloat());
				Double[] a = l.toArray(new Double[l.size()]);
				objArgs[i] = a;

			// array node as boolean array matching
			} else if ((tc.argsClass[i].equals(Boolean[].class) || tc.argsClass[i].equals(boolean[].class)) && (args[i] instanceof ArrayNode)) {
				if (debug) System.out.println("rule 3.d");
				List<Boolean> l = new ArrayList<>();
				for (Iterator<Node> it = ((ArrayNode)args[i]).getElements().iterator(); it.hasNext();)
					l.add(it.next().asBoolean());
				Boolean[] a = l.toArray(new Boolean[l.size()]);
				objArgs[i] = a;

			// array node as string array matching
			} else if (tc.argsClass[i].equals(String[].class) && (args[i] instanceof ArrayNode)) {
				if (debug) System.out.println("rule 3.e");
				List<String> l = new ArrayList<>();
				for (Iterator<Node> it = ((ArrayNode)args[i]).getElements().iterator(); it.hasNext();)
					l.add(it.next().asString().value());
				String[] a = l.toArray(new String[l.size()]);
				objArgs[i] = a;
				
			// array node as List matching
			} else if (tc.argsClass[i].equals(List.class) && (args[i] instanceof ArrayNode)) {
				if (debug) System.out.println("rule 3.f");
				List<Object> l = new ArrayList<>();
				for (Iterator<Node> it = ((ArrayNode)args[i]).getElements().iterator(); it.hasNext();)
					l.add(it.next().asObject());
				objArgs[i] = l;

			// null/object node as object matching
			} else if (args[i] instanceof ObjectNode || args[i] instanceof NullNode) {
				if (debug) System.out.println("rule 4");
				objArgs[i] = args[i].asObject(tc.testsClass[i]);
			}
			else {
				if (debug) System.out.println("rule X: no match");
				//could not find any matching constructor
				return false;
			}
			
			//filter out the rule if type mismatch
			if (objArgs[i] != null && !tc.testsClass[i].isAssignableFrom(objArgs[i].getClass())) {	
				return false;
			}
		}
		if (debug) {
			String arglist = "";
			for (Object arg : objArgs) {
				arglist += arg.getClass().getSimpleName()+", ";
			}
			System.out.println("strict matching : "+arglist);
		}
		return true;
	}

	/**
	 * Test constructor.
	 *
	 * @param tc the tc
	 * @param args the args
	 * @param objArgs the obj args
	 * @return true, if successful
	 */
	private boolean testConstructor(TransformConstructor tc, Node[] args,
			Object[] objArgs) {
		if (tc.argsClass.length != args.length)
			return false;

		if (debug) {
			System.out.println("non strict testing constructor for "+tc.cnst.getDeclaringClass().getSimpleName());
			System.out.println("constructor arguments : "+Arrays.asList(tc.argsClass));
			System.out.println("given arguments : "+Arrays.asList(args));
		}
		
		for (int i = 0; i < args.length; ++i) {
			if (debug) System.out.println("argument #"+i);
			if (tc.argsClass[i].equals(Integer.TYPE)) {
				if (debug) System.out.println("rule 1");
				objArgs[i] = args[i].asInteger();
			} else if (tc.argsClass[i].equals(Double.TYPE)) {
				if (debug) System.out.println("rule 2");
				objArgs[i] = args[i].asFloat();
			} else if (tc.argsClass[i].equals(Float.TYPE)) {
				if (debug) System.out.println("rule 3");
				objArgs[i] = new Double(args[i].asFloat());
			} else if (tc.argsClass[i].equals(Boolean.TYPE)) {
				if (debug) System.out.println("rule 4");
				objArgs[i] = args[i].asBoolean();
			} else if (tc.argsClass[i].equals(String.class)) {
				if (debug) System.out.println("rule 5");
				objArgs[i] = args[i].asString().value();
			} else if (tc.argsClass[i].equals(String[].class)) {
				if (debug) System.out.println("rule 6");
				objArgs[i] = new String[] {args[i].asString().value()};
			} else {
				objArgs[i] = args[i].asObject(tc.testsClass[i]);
			}
			if (objArgs[i] != null && !tc.testsClass[i].isAssignableFrom(objArgs[i].getClass())) {
				return false;
			}
		}
		if (debug) System.out.println("non-strict matching");
		return true;
	}

}
