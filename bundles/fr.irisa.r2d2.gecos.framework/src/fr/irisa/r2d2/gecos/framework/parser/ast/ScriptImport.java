package fr.irisa.r2d2.gecos.framework.parser.ast;

import fr.irisa.r2d2.parser.runtime.Span;

@SuppressWarnings("all")
public class ScriptImport extends ScriptNode {

	private ScriptIdent script;

	public ScriptImport(Span span, ScriptIdent script) {
		super(span);
		this.script = script;
	}

	@Override
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptImport(this);
	}

}
