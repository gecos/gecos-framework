package fr.irisa.r2d2.gecos.framework.parser;

import java.util.HashMap;
import java.util.Map;

/**
 * @generated
 */
public abstract class GecosTokenNames implements IGecosTokens {

	/**
	 * @generated
	 */
	private static Map<Integer, String> names = new HashMap<Integer, String>();

	/**
	 * @generated not
	 */
	static {
		names.put(FOR, "for");
		names.put(FALSE, "false");
		names.put(TRUE, "true");
		names.put(IN, "in");
		names.put(DO, "do");
		names.put(DONE, "done");
		names.put(WHILE, "while");
		names.put(CHANGING, "changing");
		names.put(NIL, "nil");
		names.put(IDENT, "<IDENT>");
		names.put(STRING, "<STRING>");
		names.put(INT, "<INTEGER>");
		names.put(ARGUMENT, "$<ARGUMENT>");
		names.put(LPAR, "'('");
		names.put(RPAR, "')'");
		names.put(LBRT, "'['");
		names.put(RBRT, "']'");
		names.put(LBRC, "'{'");
		names.put(RBRC, "'}'");
		names.put(EGAL, "'='");
		names.put(SEMI, "';'");
		names.put(COMA, "','");
		names.put(PLUS, "'+'");
		names.put(MINUS, "'-'");
		names.put(ARROW, "'->'");
		names.put(FUNCTION, "function");
		names.put(RETURN, "return");
		names.put(IMPORT, "import");
		names.put(IF, "if");
		names.put(THEN, "then");
		names.put(ELSE, "else");
		names.put(FI, "fi");
	}

	public static String[] Keywords = {
			GecosTokenNames.nameOf(IGecosTokens.CHANGING),
			GecosTokenNames.nameOf(IGecosTokens.DO),
			GecosTokenNames.nameOf(IGecosTokens.DONE),
			GecosTokenNames.nameOf(IGecosTokens.FOR),
			GecosTokenNames.nameOf(IGecosTokens.FUNCTION),
			GecosTokenNames.nameOf(IGecosTokens.IMPORT),
			GecosTokenNames.nameOf(IGecosTokens.IN),
			GecosTokenNames.nameOf(IGecosTokens.NIL),
			GecosTokenNames.nameOf(IGecosTokens.RETURN),
			GecosTokenNames.nameOf(IGecosTokens.WHILE),
			GecosTokenNames.nameOf(IGecosTokens.IF),
			GecosTokenNames.nameOf(IGecosTokens.THEN),
			GecosTokenNames.nameOf(IGecosTokens.ELSE),
			GecosTokenNames.nameOf(IGecosTokens.FI),
			GecosTokenNames.nameOf(IGecosTokens.TRUE),
			GecosTokenNames.nameOf(IGecosTokens.FALSE) };

	/**
	 * Name of.
	 *
	 * @param s the s
	 * @return the string
	 * @generated 
	 */
	public static String nameOf(int s) {
		return names.get(s);
	}
}