package fr.irisa.r2d2.parser.runtime;

public interface IToken {

	/**
	 * Return the type of this token.
	 * 
	 * @return the token type
	 */
	int type();

	/**
	 * Return the start position of this token
	 * 
	 * @return the start position of the token
	 */
	int start();

	/**
	 * Return the end position of this token
	 * 
	 * @return the end position of the token
	 */
	int end();

	/**
	 * Return the semantic value associated with this token
	 * 
	 * @return the semantic value
	 */
	Object value();
}
