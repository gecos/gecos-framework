package fr.irisa.r2d2.gecos.framework.internal.script.primitive;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleArgument;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.parser.runtime.Span;

public class Dump implements IScriptEvaluable {
	
	/**
	 * Instantiates a new dump.
	 */
	public Dump() { }

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {
		try {
			PrintWriter out = new PrintWriter(new FileWriter("dump.xml"));

			IModuleIndexer indexer = GecosFramework.getModuleIndexer();

			out.println("<modules>");
			for (IModule module : indexer.getModules()) {
				out.print("<module ");
				out.print("name=\"" + module.getName() + "\" ");
				out.print("packageName=\"" + module.getPackageName() + "\" ");
				out.print("className=\"" + module.getClassName() + "\" ");
				if (module.getReturnType() != null)
					out.print("returnType=\"" + module.getReturnType() + "\"");
				out.println(">");
				for (IModuleConstructor cnst : module.getConstructors()) {
					out.println("<constructor> ");
					for (IModuleArgument arg : cnst.getArguments()) {
						out.println("<arg name=\""+arg.name()+"\" type=\""+arg.typeSignature()+"\"/> ");
					}
					out.println("</constructor>");
				}
				out.println("<description>" + module.getDescription() + "</description>");
				out.println("</module>");
			}
			out.println("</modules>");
			out.close();
		} catch (IOException e) {
			throw new ScriptException("Error while dumping database");
		}
		return new NullNode(span);
	}

}
