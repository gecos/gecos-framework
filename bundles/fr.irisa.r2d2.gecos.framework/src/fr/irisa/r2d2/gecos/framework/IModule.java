/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework;

import java.util.List;

public interface IModule {
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	String getName();
	
	/**
	 * Gets the package name.
	 *
	 * @return the package name
	 */
	String getPackageName();

	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	String getClassName();
	
	/**
	 * Gets the return type.
	 *
	 * @return the return type
	 */
	String getReturnType();
	
	/**
	 * Checks if is iterative.
	 *
	 * @return true, if is iterative
	 */
	boolean isIterative();

	/**
	 * Gets the constructors.
	 *
	 * @return the constructors
	 */
	List<IModuleConstructor> getConstructors();
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	String getDescription();

	/**
	 * Checks if is deprecated.
	 *
	 * @return true, if is deprecated
	 */
	boolean isDeprecated();

}