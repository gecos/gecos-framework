package fr.irisa.r2d2.gecos.framework.modules;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;

@GSModule("Asserts if the given boolean value is true.\n "
		+ "If not, it stops the execution without throwing "
		+ "any exception/error, and prints 'Assertion ERROR. Stoping execution.' on stderr.")
public class Assert {

	private Boolean test;
	
	/**
	 * Instantiates a new assert.
	 *
	 * @param test the test
	 */
	public Assert(Boolean test) {
		this.test = test;
	}
	
	/**
	 * Compute.
	 *
	 * @throws ScriptException the script exception
	 */
	public void compute() throws ScriptException {
		if (this.test) {
			//nothing
		} else {
			//stop execution
			throw new ScriptException("Assertion ERROR. Stoping execution");
		}
	}
}
