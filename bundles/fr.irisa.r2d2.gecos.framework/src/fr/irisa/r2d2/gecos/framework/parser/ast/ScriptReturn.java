package fr.irisa.r2d2.gecos.framework.parser.ast;

import fr.irisa.r2d2.parser.runtime.Span;

@SuppressWarnings("all")
public class ScriptReturn extends ScriptNode {

	private ScriptNode expr;

	public ScriptReturn(Span span, ScriptNode expr) {
		super(span);
		this.expr = expr;
	}

	@Override
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptReturn(this);
	}

}
