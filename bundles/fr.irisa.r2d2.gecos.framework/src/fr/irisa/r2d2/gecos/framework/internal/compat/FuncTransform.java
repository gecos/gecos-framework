/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.compat;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Vector;

import fr.irisa.r2d2.gecos.framework.script.IFunctionFactory;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.ObjectNode;
import fr.irisa.r2d2.parser.runtime.Span;

public class FuncTransform implements IScriptEvaluable, IFunctionFactory {
	
	private String name;
	private String className;
	private Vector<String> argTypes;
	private String result;
	private ClassLoader loader;
	private boolean isIterative;
	private String description;

	private Class<?> mclass;
	private Constructor<?> cnst;
	private Method method;
	@SuppressWarnings("rawtypes")
	private Class[] argsClass;
	@SuppressWarnings("rawtypes")
	private Class[] testsClass;

	/**
	 * Instantiates a new func transform.
	 *
	 * @param name the name
	 * @param loader the loader
	 * @param isIterative the is iterative
	 */
	public FuncTransform(String name, ClassLoader loader,
			boolean isIterative) {
		this.name = name;
		this.loader = loader;
		this.isIterative = isIterative;
		this.argTypes = new Vector<String>();
		this.description = "<none>";
		this.cnst = null;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getName()
	 */
	public String getName() {
		return name;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getFunction()
	 */
	public IScriptEvaluable getFunction() throws ScriptException {
		return this;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getDescription()
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the class.
	 *
	 * @param cls the new class
	 */
	public void setClass(String cls) { className = cls; }
	
	/**
	 * Sets the result.
	 *
	 * @param cls the new result
	 */
	public void setResult(String cls) { result = cls; }
	
	/**
	 * Adds the arg.
	 *
	 * @param arg the arg
	 */
	public void addArg(String arg) { argTypes.add(arg); }
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Initialize.
	 *
	 * @throws Exception the exception
	 */
	protected void initialize() throws Exception {
		mclass = loader.loadClass(className);
		argsClass = new Class[argTypes.size()];
		testsClass = new Class[argTypes.size()];
		for (int i = 0; i < argTypes.size(); ++i) {
			String n = argTypes.elementAt(i);
			if (n.equals("int")) {
				argsClass[i] = Integer.TYPE;
				testsClass[i] = Integer.class;
			} else if (n.equals("boolean")) {
				argsClass[i] = Boolean.TYPE;
				testsClass[i] = Boolean.class;
			} else {
				argsClass[i] = loader.loadClass(n);
				testsClass[i] = argsClass[i];
			}
		}
		cnst = mclass.getConstructor(argsClass);
		method = mclass.getDeclaredMethod("compute", new Class[0]);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	@SuppressWarnings("unchecked")
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {
		try {
			if (cnst == null)
				initialize();
			
			if (argTypes.size() != args.length)
				throw new ScriptException(
						"bad argument count, expecting "+argTypes.size());
				
			Object[] objArgs = new Object[args.length];
			for (int i = 0; i < objArgs.length; ++i) {
				if (argsClass[i].equals(Integer.TYPE))
					objArgs[i] = args[i].asInteger();
				else if (argsClass[i].equals(Boolean.TYPE))
					objArgs[i] = args[i].asBoolean();
				else
					objArgs[i] = args[i].asObject();
				if (objArgs[i] != null
							&& ! testsClass[i].isAssignableFrom(
								objArgs[i].getClass())) {
					throw new ScriptException("expecting "+argsClass[i]
						+" at arg #"+(i+1));
				}
			}
			Object transform = cnst.newInstance(objArgs);
			Object res = method.invoke(transform, new Object[0]);
			if (isIterative) {
				evaluator.setChanging(((Boolean)res).booleanValue());
				return new NullNode(span);
			}
			if (result != null) return new ObjectNode(span, res);
			return new NullNode(span);
		} catch(InvocationTargetException e) {
			e.getCause().printStackTrace(System.out);
			throw new ScriptException("error while invoking " + name
					+ ": " + e.getCause().getMessage());
		} catch(Exception e) {
			e.printStackTrace(System.out);
			throw new ScriptException("error while invoking " + name
					+ ":  " + e.getMessage());
		}
	}

	

}
