package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.core.resources.IResource;
import org.eclipse.ui.texteditor.ResourceMarkerAnnotationModel;

public class ScriptAnnotationModel extends ResourceMarkerAnnotationModel {

	public ScriptAnnotationModel(IResource resource) {
		super(resource);
	}
	
}
