


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptString extends ScriptNode {

	/**
	 * @generated
	 */
	private String value;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptString(Span span, String value) {
		super(span);
		this.value = value;
	}
	
	/**
	 * @generated
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * @generated
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptString(this);
	}
}