/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.launching;

import java.io.File;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.variables.IStringVariableManager;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.StringVariableSelectionDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

public class WorkingDirectoryBlock extends AbstractLaunchConfigurationTab {

	private Text fWorkingDirText;
	private Button fUseDefaultWorkingDirButton;
	private Button fWorkspaceButton;
	private Button fFileSystemButton;
	private Button fVariablesButton;
	
	private class WidgetListener extends SelectionAdapter implements ModifyListener {
		public void modifyText(ModifyEvent e) {
			updateLaunchConfigurationDialog();
		}
		public void widgetSelected(SelectionEvent e) {
			Object source= e.getSource();
			if (source == fWorkspaceButton) {
				handleWorkspaceDirBrowseButtonSelected();
			} else if (source == fFileSystemButton) {
				handleWorkingDirBrowseButtonSelected();
			} else if (source == fUseDefaultWorkingDirButton) {
				handleUseDefaultWorkingDirButtonSelected();
			} else if (source == fVariablesButton) {
				handleWorkingDirVariablesButtonSelected();
			}
		}
	}
	
	private WidgetListener fListener= new WidgetListener();
	

	public void createControl(Composite parent) {
		Font font = parent.getFont();
		
		Group group = new Group(parent, SWT.NONE);
		//PlatformUI.getWorkbench().getHelpSystem().setHelp(group, IJavaDebugHelpContextIds.WORKING_DIRECTORY_BLOCK);		
		GridLayout workingDirLayout = new GridLayout();
		workingDirLayout.numColumns = 2;
		workingDirLayout.makeColumnsEqualWidth = false;
		group.setLayout(workingDirLayout);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		group.setLayoutData(gd);
		group.setFont(font);
		setControl(group);
		
		group.setText("Working Directory"); //$NON-NLS-1$
				
		fWorkingDirText = new Text(group, SWT.SINGLE | SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		fWorkingDirText.setLayoutData(gd);
		fWorkingDirText.setFont(font);
		fWorkingDirText.addModifyListener(fListener);
		
		fUseDefaultWorkingDirButton = new Button(group,SWT.CHECK);
		fUseDefaultWorkingDirButton.setText("Use default working directory"); //$NON-NLS-1$
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fUseDefaultWorkingDirButton.setLayoutData(gd);
		fUseDefaultWorkingDirButton.setFont(font);
		fUseDefaultWorkingDirButton.addSelectionListener(fListener);
		
		Composite buttonComp = new Composite(group, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		buttonComp.setLayout(layout);
		gd = new GridData(GridData.HORIZONTAL_ALIGN_END);
		buttonComp.setLayoutData(gd);
		buttonComp.setFont(font);		
		fWorkspaceButton = createPushButton(buttonComp, "Workspace", null); //$NON-NLS-1$
		fWorkspaceButton.addSelectionListener(fListener);
		
		fFileSystemButton = createPushButton(buttonComp, "FileSystem", null); //$NON-NLS-1$
		fFileSystemButton.addSelectionListener(fListener);
		
		fVariablesButton = createPushButton(buttonComp, "Variable", null); //$NON-NLS-1$
		fVariablesButton.addSelectionListener(fListener);
	}
	
	protected void handleWorkingDirBrowseButtonSelected() {
		DirectoryDialog dialog = new DirectoryDialog(getShell());
		dialog.setMessage("Choose directory"); //$NON-NLS-1$
		String currentWorkingDir = fWorkingDirText.getText();
		if (!currentWorkingDir.trim().equals("")) { //$NON-NLS-1$
			File path = new File(currentWorkingDir);
			if (path.exists()) {
				dialog.setFilterPath(currentWorkingDir);
			}			
		}
		
		String selectedDirectory = dialog.open();
		if (selectedDirectory != null) {
			fWorkingDirText.setText(selectedDirectory);
		}		
	}
	
	protected void handleWorkspaceDirBrowseButtonSelected() {
	    IContainer currentContainer = getContainer();
		if (currentContainer == null) {
		    currentContainer = ResourcesPlugin.getWorkspace().getRoot();
		}	    
		ContainerSelectionDialog dialog = 
			new ContainerSelectionDialog(getShell(),
					currentContainer, false,
					"Choose workspace directory"); //$NON-NLS-1$
		dialog.showClosedProjects(false);
		dialog.open();
		Object[] results = dialog.getResult();		
		if ((results != null) && (results.length > 0) && (results[0] instanceof IPath)) {
			IPath path = (IPath)results[0];
			String containerName = path.makeRelative().toString();
			fWorkingDirText.setText("${workspace_loc:" + containerName + "}"); //$NON-NLS-1$ //$NON-NLS-2$
		}			
	}
	
	protected void handleUseDefaultWorkingDirButtonSelected() {
		boolean def = isDefaultWorkingDirectory(); 
		if (def) setDefaultWorkingDir();
		fWorkingDirText.setEnabled(!def);
		fWorkspaceButton.setEnabled(!def);
		fVariablesButton.setEnabled(!def);
		fFileSystemButton.setEnabled(!def);
	}
	
	protected void handleWorkingDirVariablesButtonSelected() {
		String variableText = getVariable();
		if (variableText != null) {
			fWorkingDirText.insert(variableText);
		}
	}
	
	private String getVariable() {
		StringVariableSelectionDialog dialog = new StringVariableSelectionDialog(getShell());
		dialog.open();
		return dialog.getVariableExpression();
	}
	
	protected boolean isDefaultWorkingDirectory() {
		return fUseDefaultWorkingDirButton.getSelection();
	}
	
	protected void setDefaultWorkingDir() {
		/*try {
			ILaunchConfiguration config = getLaunchConfiguration();
			if (config != null) {
				IJavaProject javaProject = JavaRuntime.getJavaProject(config);
				if (javaProject != null) {
					fWorkingDirText.setText("${workspace_loc:" + javaProject.getPath().makeRelative().toOSString() + "}"); //$NON-NLS-1$ //$NON-NLS-2$
					return;
				}
			}
		} catch (CoreException ce) {
		}*/
		fWorkingDirText.setText(System.getProperty("user.dir")); //$NON-NLS-1$
	}

	/**
	 * Set the default values of this block into the configuration.
	 * In this case : CSLaunchConstants.WORKING_DIR
	 */
	public void setDefaults(ILaunchConfigurationWorkingCopy config) {
		config.setAttribute(CSLaunchConstants.WORKING_DIR, (String)null);
	}

	public void initializeFrom(ILaunchConfiguration configuration) {
		//setLaunchConfiguration(configuration);
		try {			
			String wd = configuration.getAttribute(CSLaunchConstants.WORKING_DIR, (String)null); //$NON-NLS-1$
			fWorkingDirText.setText(""); //$NON-NLS-1$
			if (wd == null) {
				fUseDefaultWorkingDirButton.setSelection(true);
			} else {
				fWorkingDirText.setText(wd);
				fUseDefaultWorkingDirButton.setSelection(false);
			}
			handleUseDefaultWorkingDirButtonSelected();
		} catch (CoreException e) {
			setErrorMessage("Exception occurred reading configuration : " + e.getStatus().getMessage()); //$NON-NLS-1$
		}
	}

	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		String wd = null;
		if (! isDefaultWorkingDirectory()) {
			wd = getAttributeValueFrom(fWorkingDirText);
		} 
		configuration.setAttribute(CSLaunchConstants.WORKING_DIR, wd);
	}

	public String getName() {
		return "Toto";
	}
	
	protected String getAttributeValueFrom(Text text) {
		String content = text.getText().trim();
		if (content.length() > 0) {
			return content;
		}
		return null;
	}
	
	protected IContainer getContainer() {
		String path = fWorkingDirText.getText().trim();
		if (path.length() > 0) {
		    IResource res = null;
		    IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		    if (path.startsWith("${workspace_loc:")) { //$NON-NLS-1$
		        IStringVariableManager manager = VariablesPlugin.getDefault().getStringVariableManager();
			    try {
                    path = manager.performStringSubstitution(path, false);
                    @SuppressWarnings("deprecation")
					IContainer[] containers = root.findContainersForLocation(new Path(path));
                    if (containers.length > 0) {
                        res = containers[0];
                    }
                } catch (CoreException e) {
                }
			} else {	    
				res = root.findMember(path);
			}
			if (res instanceof IContainer) {
				return (IContainer)res;
			}
		}
		return null;
	}

}
