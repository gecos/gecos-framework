/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

import org.osgi.framework.Bundle;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;
import fr.irisa.r2d2.gecos.framework.impl.GSAbstractModule;
import fr.irisa.r2d2.gecos.framework.internal.script.Transform;
import fr.irisa.r2d2.gecos.framework.script.IFunctionFactory;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.parser.runtime.Span;

public class Module extends GSAbstractModule implements IFunctionFactory {

	private IScriptEvaluable transform;
	private Bundle bundle;


	/**
	 * Instantiates a new module.
	 *
	 * @param bundle the bundle
	 * @param packageName the package name
	 * @param name the name
	 * @param className the class name
	 */
	public Module(Bundle bundle, String packageName, String name, String className) {
		super(name, className, packageName);
		this.bundle = bundle;
		this.transform = null;
	}
	
	/**
	 * Transforms are now beans.
	 *
	 * @return the i script evaluable
	 */
	private IScriptEvaluable createTransform() {
		try {
			Class<?> transformClass = bundle.loadClass(className);
			Type type = null;
			Method method = null;
			isIterative = false;
			try {
				method = transformClass.getMethod("compute", new Class[0]);
				type = method.getGenericReturnType();
			} catch (NoSuchMethodException e) {
				method = transformClass.getMethod("computeIterative", new Class[0]);
				type = method.getGenericReturnType();
				if (type != Boolean.TYPE) {
					return null;
				}
				isIterative = true;
			}
			returnType = Signature.toString(type);
			Transform transform = new Transform(name, method, isIterative);
			for (Constructor<?> cnst : transformClass.getConstructors()) {
				constructors.add(new ModuleConstructor(this, cnst));
				transform.addConstructor(cnst);
			}
			
			return transform;
		} catch (ClassNotFoundException e) {
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.impl.GSAbstractModule#getReturnType()
	 */
	@Override
	public String getReturnType() {
		checkTransform();
		if (returnType == null)
			return "Ljava.lang.Object;";
		return super.getReturnType();
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.impl.GSAbstractModule#isIterative()
	 */
	@Override
	public boolean isIterative() {
		checkTransform();
		return super.isIterative();
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.impl.GSAbstractModule#getConstructors()
	 */
	@Override
	public List<IModuleConstructor> getConstructors() {
		checkTransform();
		return super.getConstructors();
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.impl.GSAbstractModule#getDescription()
	 */
	@Override
	public String getDescription() {
		if(description.length() == 0)
			_initDescription();
		return super.getDescription();
	}
	
	/**
	 * Inits the description.
	 */
	private void _initDescription() {
		Class<?> transformClass;
		try {
			transformClass = bundle.loadClass(className);
			if(transformClass.getAnnotation(Deprecated.class) != null) {
				appendDescription("Deprecated!\n");
				setDeprecated(true);
			}

			GSModule moduleAnnotation = transformClass.getAnnotation(GSModule.class);
			if(moduleAnnotation != null)
				appendDescription(moduleAnnotation.value());
//			else
//				System.err.println("WARNING: Gecos Module '" + transformClass.getName() + "' have no Description annotation.");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static class BadBeanTransform implements IScriptEvaluable {

		private String name;
		private String className;

		/**
		 * Instantiates a new bad bean transform.
		 *
		 * @param name the name
		 * @param className the class name
		 */
		public BadBeanTransform(String name, String className) {
			this.name = name;
			this.className = className;
		}

		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
		 */
		public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
				throws ScriptException {
			throw new ScriptException("transform " + name + "(" + className
					+ ") is not a proper transform");
		}

	}
	
	/**
	 * Check transform.
	 */
	private void checkTransform() {
		try {
			getFunction();
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IFunctionFactory#getFunction()
	 */
	public IScriptEvaluable getFunction() throws ScriptException {
		if (transform == null) {
			if ((transform = createTransform()) == null) {
				transform = new BadBeanTransform(name, className);
			}
		}
		return transform;
	}

}
