
package com.mycompany.example.modules;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Vector;
import java.util.regex.Pattern;

public class DirectoryFilter {

	private String directory;
	private Pattern pattern;

	public DirectoryFilter(String directory, String filter) {
		this.directory = directory;
		this.pattern = Pattern.compile(filter);
	}
	
	public Vector compute() {
		Vector list = new Vector();
		File dir = new File(directory);
		if (! dir.exists() || ! dir.isDirectory()) return list;
		String[] files = dir.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return pattern.matcher(name).matches();
			}
		});
		for (int i = 0; i < files.length; ++i)
			list.add(files[i]);
		return list;
	}
}
