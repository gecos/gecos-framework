package fr.irisa.r2d2.gecos.framework.modules;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.modules.Mute.NullPrintStream;

@GSModule("UnMute all outputs.\n"
		+ "\nSee: 'Mute' module.")
public class UnMute {

	/**
	 * Instantiates a new un mute.
	 */
	public UnMute() {}
	
	/**
	 * Unmute.
	 */
	public static void unmute() {
		if (System.out instanceof NullPrintStream) {
			System.setOut(Mute.out);
			System.setErr(Mute.err);
			Mute.out = null;
			Mute.err = null;
		}
	}
	
	/**
	 * Compute.
	 */
	public void compute() {
		unmute();
	}
}
