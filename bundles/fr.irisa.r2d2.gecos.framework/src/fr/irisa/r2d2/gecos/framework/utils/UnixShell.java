package fr.irisa.r2d2.gecos.framework.utils;

import static java.util.stream.Collectors.joining;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.security.InvalidParameterException;
import java.util.Arrays;

import com.google.common.base.Strings;


public class UnixShell extends AbstractShell {
	
	private static final int EXIT_CODE_SUCCESS = 0;
	private static final int EXIT_CODE_TIMEOUT = 137; // 128 + [timeout signal (9 for 'kill')]
	
	private static final String DEFAULT_SHELL_CMD = "/bin/bash";
	private static final String TIMEOUT_CMD = "timeout";
	
	protected String shellCmd;
	
	
	public UnixShell() {
		this(DEFAULT_SHELL_CMD);
	}
	
	public UnixShell(String shellPath) {
		super();
		this.quiet = false;
		this.verbose = false;
		
		this.shellCmd = shellPath;
		if(! new File(shellPath).isFile())
			throw new InvalidParameterException("Couldn't find shell : " + shellPath);
		
		if(findProgram(TIMEOUT_CMD) == null) {
			System.err.println("[SHELL][WARNING] did not find command 'timeout'! make sure you "
					+ "have it installed, otherwise executing commands with a timeout may not be able to execute!");
		}
	}
	
	@Override
	public int successExitCode() {
		return EXIT_CODE_SUCCESS;
	}

	@Override
	public int timeoutExitCode() {
		return EXIT_CODE_TIMEOUT;
	}
	
	@Override
	protected String[] makeCommand(String[] cmdArray, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		if(cmdArray == null || cmdArray.length == 0)
			throw new InvalidParameterException("Command cannot be null or empty!");
		
		String cmd = Arrays.stream(cmdArray).collect(joining(" "));
		if(timeoutSec != null)
			cmd = TIMEOUT_CMD + " -s kill " + timeoutSec + " " + cmd;
		
		String[] command = new String[] { shellCmd, "-c", cmd };
		
		return command;
	}

	@Override
	public String findProgram(String cmd) {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		try {
			int r = rawExec(new String[] {"which", cmd}, null, null, outToByteArray(outStream), null);
			if(r == successExitCode()) {
				String path = outStream.toString().trim();
				if(! Strings.isNullOrEmpty(path)) 
					return path;
			}
		} catch (Exception e) { 
			e.printStackTrace(); 
		}
		System.err.println("[SHELL][WARNING] did not find command: " + cmd);
		return null;
	}
}
