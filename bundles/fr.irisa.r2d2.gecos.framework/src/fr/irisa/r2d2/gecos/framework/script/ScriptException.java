/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.script;

import fr.irisa.r2d2.parser.runtime.Span;

public class ScriptException extends Exception {

	private static final long serialVersionUID = -5865203678606830941L;

	public Span span;
	public Throwable cause;
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getCause()
	 */
	public Throwable getCause() { return this.cause; }
	
	/**
	 * Instantiates a new script exception.
	 *
	 * @param span the span
	 * @param str the str
	 * @param c the c
	 */
	public ScriptException(Span span, String str, Throwable c) {
		super(str,c);
		this.span = span;
		this.cause = c;
	}
	
	/**
	 * Instantiates a new script exception.
	 *
	 * @param str the str
	 * @param c the c
	 */
	public ScriptException(String str, Throwable c) {
		this(new Span(0,0), str,c);
	}
	
	/**
	 * Instantiates a new script exception.
	 *
	 * @param str the str
	 */
	public ScriptException(String str) {
		this(new Span(0,0), str, null);
	}
	
	/**
	 * Span.
	 *
	 * @return the span
	 */
	public Span span() { return span; }
	
}
