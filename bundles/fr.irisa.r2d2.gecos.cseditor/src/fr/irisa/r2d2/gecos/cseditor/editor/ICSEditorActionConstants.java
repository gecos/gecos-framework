/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.editor;

import org.eclipse.ui.texteditor.ITextEditorActionConstants;

public interface ICSEditorActionConstants extends ITextEditorActionConstants {

	String CONTENT_ASSIST = "ContentAssist";
    String OPEN_DEFINITION = "fr.irisa.r2d2.gecos.cseditor.opendef";
	String TOGGLE_COMMENT = "fr.irisa.r2d2.gecos.cseditor.togglecomment";

}
