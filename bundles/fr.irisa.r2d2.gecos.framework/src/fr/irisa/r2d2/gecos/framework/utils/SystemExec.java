package fr.irisa.r2d2.gecos.framework.utils;

import java.io.File;
import java.io.PrintStream;

public class SystemExec {
	
	private static final AbstractShell SHELL = createShell().setVerbose(true);

	
	public static String getOsName() {
		return System.getProperty("os.name").toLowerCase();
	}
	
	public static boolean isWindowsOS() {
		return (getOsName().contains("win"));
	}

	public static boolean isMacOS() {

		return (getOsName().contains("mac"));

	}

	public static boolean isUnixOS() {
		String os = getOsName();
		return (os.contains("nix") || os.contains("nux") || os.contains("aix") );
	}
	
	public static AbstractShell createShell(String shellPath) {
		if(!isUnixOS() && !isMacOS())
			System.err.println(new RuntimeException().getStackTrace()[0] + 
					"[WARNING] Shell is not properly supported for your Operating system: " + getOsName());
		
		AbstractShell shell = shellPath == null ? new UnixShell() : new UnixShell(shellPath);
		return shell;
	}
	
	public static AbstractShell createShell() {
		return createShell(null);
	}

	public static int successExitCode() {
		return SHELL.successExitCode();
	}

	public static int timeoutExitCode() {
		return SHELL.timeoutExitCode();
	}

	public static int exec(String[] cmd, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		return SHELL.exec(cmd, inDir, timeoutSec, stdout, stderr);
	}

	public static int shell(String cmd, File inDir, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		return SHELL.shell(cmd, inDir, timeoutSec, stdout, stderr);
	}

	public static int shell(String cmd, File inDir, PrintStream stdout, PrintStream stderr) {
		return SHELL.shell(cmd, inDir, stdout, stderr);
	}

	public static int shell(String cmd, Long timeoutSec, PrintStream stdout, PrintStream stderr) {
		return SHELL.shell(cmd, timeoutSec, stdout, stderr);
	}

	public static int shell(String cmd, PrintStream stdout, PrintStream stderr) {
		return SHELL.shell(cmd, stdout, stderr);
	}

	public static int shell(String cmd, File inDir, Long timeoutSec) {
		return SHELL.shell(cmd, inDir, timeoutSec);
	}

	public static int shell(String cmd) {
		return SHELL.shell(cmd);
	}
	
}
