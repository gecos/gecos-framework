package fr.irisa.r2d2.parser.runtime;


public interface ILexer {
    
    int initialState();
    
    int getState();

    void setState(int state);
    
    IToken scan();
    
	//void setInput(ILexerInput input);
    
}
