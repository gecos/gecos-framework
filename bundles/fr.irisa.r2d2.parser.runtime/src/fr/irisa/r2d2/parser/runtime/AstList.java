
package fr.irisa.r2d2.parser.runtime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AstList<T extends AstNode> extends AstNode implements Iterable<T> {

	private List<T> elements;

	public AstList() {
		super(new Span(-1, -1));
		elements = new ArrayList<T>();
	}
	
	public AstList(T element) {
		this();
		add(element);
	}

	public void add(T t) {
		updateSpan(t);
		elements.add(t);
	}
	
	public void addAll(AstList<T> list) {
		for (T t : list)
			elements.add(t);
	}
	
	/*public void add(int index, T t) {
		updateSpan(t);
		elements.add(index, t);
	}*/
	
	public int size() {
		return elements.size();
	}

	private void updateSpan(AstNode t) {
		if (elements.size() == 0)
			span = new Span(t.span);
		else {
			if (t.from() < from())
				span = new Span(t.from(), to());
			if (t.to() > to())
				span = new Span(from(), t.to());
		}
	}
	
	public T get(int index) {
		return elements.get(index);
	}

	public Iterator<T> iterator() {
		return elements.iterator();
	}

	public static AstList<AstNode> add(AstList<AstNode> list, AstNode element) {
		AstList<AstNode> newList = new AstList<AstNode>();
		newList.addAll(list);
		newList.add(element);
		return newList;
	}
	
	public static AstList<AstNode> add(AstNode element, AstList<AstNode> list) {
		AstList<AstNode> newList = new AstList<AstNode>();
		newList.add(element);
		newList.addAll(list);
		return newList;
	}

}