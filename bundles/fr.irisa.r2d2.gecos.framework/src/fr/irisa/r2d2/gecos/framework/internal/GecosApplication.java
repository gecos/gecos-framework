/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.gecos.framework.IConfiguration;
import fr.irisa.r2d2.gecos.framework.parser.ExecutionSemantic.StopException;

public class GecosApplication implements Version, IApplication {


	/**
	 * Banner.
	 */
	public static void banner() {
		GecosFramework.out.println(PROGNAME + " version "+ VERSION);
	}
	
	/**
	 * Gets the configurations.
	 *
	 * @return the configurations
	 */
	private Map<String, IConfigurationElement> getConfigurations() {
		BundleContext context = GecosInternalFramework.getDefault().getContext();
		ServiceReference<?> ref = context.getServiceReference(IExtensionRegistry.class.getName());
		IExtensionRegistry registry = (IExtensionRegistry) context.getService(ref);
		IConfigurationElement[] elements = registry.getConfigurationElementsFor(
				GecosInternalFramework.PLUGIN_ID,
				GecosInternalFramework.PL_CONFIGURATIONS);
		Map<String, IConfigurationElement> configurations = new HashMap<String, IConfigurationElement>();
		for (int i = 0; i < elements.length; ++i) {
			configurations.put(elements[i].getAttribute("name"), elements[i]);
		}
		context.ungetService(ref);
		return configurations;
	}
	
	/**
	 * Creates the configuration.
	 *
	 * @param configName the config name
	 * @return the i configuration
	 */
	private IConfiguration createConfiguration(String configName) {
		try {
			Map<String, IConfigurationElement> configurations = getConfigurations();
			IConfigurationElement element = configurations.get(configName);
			if (element != null)
				return (IConfiguration) element.createExecutableExtension("class");
		} catch (CoreException e) {
		}
		return null;
	}
	
	/**
	 * Run.
	 *
	 * @param args the args
	 * @return the int
	 */
	private int run(String[] args) {
		List<String> arguments = new ArrayList<String>();
		Collections.addAll(arguments, args);

		// XXX suppress the pde launch argument
		if (arguments.size() > 0 && arguments.get(0).equals("-pdelaunch")) {
			arguments.remove(0);
		}
		
		if (arguments.size() == 0) {
			GecosFramework.out.println("Expecting configuration name as first argument");
			return 10;
		}
		
		// XXX temporary shortcut
		//String configName = arguments.remove(0);
		String configName = "Script";
		IConfiguration config = createConfiguration(configName);
		if (config == null) {
			GecosFramework.out.println("Unable to create configuration '"+configName+"'");
			return 11;
		}
		OptionsManager manager = new OptionsManager();
		config.configureOptions(manager);
		try {
			manager.parse(arguments);
			config.run(manager.getOptions());
		} catch(StopException e) {
			return e.getStatus();
		} catch (Exception e) {
			GecosFramework.out.println(e.getMessage());
			e.printStackTrace();
			return 12;
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {
		int res = run((String[])context.getArguments().get(IApplicationContext.APPLICATION_ARGS));
		if(res != 0) 
			return res;
		return IApplication.EXIT_OK;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		
	}

	/**
	 * External.
	 *
	 * @param args the args
	 * @return the int
	 */
	public int external(String[] args) {
		return run(args);
	}
}
