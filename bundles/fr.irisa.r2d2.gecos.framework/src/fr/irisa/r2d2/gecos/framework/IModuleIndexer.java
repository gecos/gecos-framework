/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework;

import java.util.Collection;

public interface IModuleIndexer {

	/**
	 * Returns a newly created collection of modules whose name starts
	 * with the specified prefix.
	 *
	 * @param prefix the prefix
	 * @return a newly created collection of modules, empty if none was found.
	 */
	Collection<IModule> getModulesStartingBy(String prefix);
	
	/**
	 * Returns a newly created collection of modules whose name contains
	 * the specified substring.
	 *
	 * @param substring the substring
	 * @return the modules containing
	 */
	Collection<IModule> getModulesContaining(String substring);

	/**
	 * Return the gecos module corresponding to the specified moduleName.
	 *
	 * @param moduleName the module name
	 * @return the module if found, null otherwise.
	 */
	IModule getModule(String moduleName);
	
	/**
	 * Gets the modules.
	 *
	 * @return a (read-only or newly created) collection of all indexed modules.
	 * The collection is empty if none exists.
	 */
	Collection<IModule> getModules();

}
