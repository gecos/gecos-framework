package fr.irisa.r2d2.gecos.framework.modules;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Convert a String input to Integer.\n"
		+ "\nReturn: the corresponding Integer.\n"
		+ "Fails if the string cannot be converted to an integer.\n")
public class AtoiModule {
	String str;

	/**
	 * Instantiates a new atoi module.
	 *
	 * @param str the str
	 */
	public AtoiModule(String str) {
		this.str = str;
	}

	/**
	 * Compute.
	 *
	 * @return the integer
	 */
	public Integer compute() {
		return Integer.valueOf(str);
	}
}