package fr.irisa.r2d2.gecos.cseditor.wizard;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import fr.irisa.r2d2.gecos.cseditor.assist.WorkspaceModuleManager;

public class ExportDatabaseMainPage extends WizardPage {

	private CheckboxTableViewer tableViewer;
	private Table table;
	private Set<IProject> selectedProjects = new HashSet<IProject>();

	protected ExportDatabaseMainPage(String pageName, String title, ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
		setDescription("Choose the projects whose modules will be exported");
	}
	
	public void setSelectedProjects(IProject[] sp) {
		Collection<IProject> projects = WorkspaceModuleManager.getDefault().getProjects();
		for (IProject project : sp) {
			if (projects.contains(project)) {
				selectedProjects.add(project);
			}
		}
	}
	
	public IProject[] getSelectedProjects() {
		return selectedProjects.toArray(new IProject[selectedProjects.size()]);
	}

	public void createControl(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);;
		c.setFont(parent.getFont());
		GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true, 1, 0);
		gd.widthHint= 0;
		gd.heightHint= 0;
		c.setLayoutData(gd);
		GridLayout layout= new GridLayout(1, false);
		initializeDialogUnits(parent);
		layout.horizontalSpacing= convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
		layout.verticalSpacing= convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
		layout.marginLeft= layout.marginRight= convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		layout.marginTop= layout.marginBottom= convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
		layout.marginWidth= layout.marginHeight= 0;
		c.setLayout(layout);
		initializeDialogUnits(c);
		
		addProjectSection(c);
		initializeProjects();
		updateEnablement();
		
		setControl(c);
	}
	
	private void addProjectSection(Composite composite) {
		
		table = new Table(composite, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		tableViewer = new CheckboxTableViewer(table);
		table.setLayout(new TableLayout());
		GridData data = new GridData(GridData.FILL_BOTH);
		data.heightHint = 300;
		table.setLayoutData(data);
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setLabelProvider(new WorkbenchLabelProvider());
		tableViewer.addCheckStateListener(new ICheckStateListener() {
			public void checkStateChanged(CheckStateChangedEvent event) {
				Object temp = event.getElement();
				if (temp instanceof IProject){
					IProject project = (IProject) event.getElement();
					if (event.getChecked()) {
						selectedProjects.add(project);
					} else {
						selectedProjects.remove(project);
					}
				}
				updateEnablement();
			}
		});

		Composite buttonComposite = new Composite(composite, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.marginWidth = 0;
		buttonComposite.setLayout(layout);
		data = new GridData(SWT.FILL, SWT.FILL, true, false);
		buttonComposite.setLayoutData(data);

		Button selectAll = new Button(buttonComposite, SWT.PUSH);
		data = new GridData();
		data.verticalAlignment = GridData.BEGINNING;
		data.horizontalAlignment = GridData.END;
		int widthHint = convertHorizontalDLUsToPixels(IDialogConstants.BUTTON_WIDTH);
		data.widthHint = Math.max(widthHint, selectAll.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		selectAll.setLayoutData(data);
		selectAll.setText("Select All");
		selectAll.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				tableViewer.setAllChecked(true);
				selectedProjects.clear();
				IProject[] checked = (IProject[]) tableViewer.getCheckedElements();
				for (int i = 0; i < checked.length; i++) {
					selectedProjects.add(checked[i]);
				}
				updateEnablement();
			}
		});

		Button deselectAll = new Button(buttonComposite, SWT.PUSH);
		data = new GridData();
		data.verticalAlignment = GridData.BEGINNING;
		data.horizontalAlignment = GridData.END;
		widthHint = convertHorizontalDLUsToPixels(IDialogConstants.BUTTON_WIDTH);
		data.widthHint = Math.max(widthHint, deselectAll.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		deselectAll.setLayoutData(data);
		deselectAll.setText("Deselect All");
		deselectAll.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				tableViewer.setAllChecked(false);
				selectedProjects.clear();
				updateEnablement();
			}
		});
	}
	
	private void initializeProjects() {
		tableViewer.setInput(WorkspaceModuleManager.getDefault().getProjects());
	
		// Check any necessary projects
		if (selectedProjects != null) {
			tableViewer.setCheckedElements(selectedProjects.toArray(new IProject[selectedProjects.size()]));
		}
	}
	
	private void updateEnablement() {
		boolean complete = (selectedProjects.size() != 0);
		
		if (complete) {
			setMessage(null);
		}
		setPageComplete(complete);
	}
	
}
