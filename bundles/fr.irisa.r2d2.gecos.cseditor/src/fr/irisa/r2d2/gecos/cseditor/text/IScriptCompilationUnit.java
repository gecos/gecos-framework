package fr.irisa.r2d2.gecos.cseditor.text;

import fr.irisa.r2d2.parser.runtime.IErrorReporter;

public interface IScriptCompilationUnit {

	void reconcile(String source, IErrorReporter reporter);

	void completeAt(String source, int offset);

}
