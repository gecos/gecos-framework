/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.templates;

import java.net.URL;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.pde.core.plugin.IPluginBase;
import org.eclipse.pde.core.plugin.IPluginElement;
import org.eclipse.pde.core.plugin.IPluginExtension;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.core.plugin.IPluginModelFactory;
import org.eclipse.pde.ui.templates.OptionTemplateSection;
import org.eclipse.pde.ui.templates.TemplateOption;
import org.osgi.framework.Bundle;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;

/**
 * XXX Should be moved to pde package
 * with templates/java/$className$.java 
 * 
 * @author llhours
 *
 */
public class PrimitiveTemplate extends OptionTemplateSection {
	
	public static final String KEY_CLASS_NAME = "className"; //$NON-NLS-1$
	public static final String KEY_PRIMITIVE_NAME = "primitiveName"; //$NON-NLS-1$
	public static final String CLASS_NAME = "SamplePrimitive"; //$NON-NLS-1$

	public PrimitiveTemplate() {
		setPageCount(1);
		createOptions();
	}
	
	private void createOptions() {
		addOption(
			KEY_PACKAGE_NAME,
			"Package name:",
			(String) null,
			0);
		addOption(
			KEY_CLASS_NAME,
			"Class name:",
			CLASS_NAME,
			0);
		addOption(
			KEY_PRIMITIVE_NAME,
			"Primitive name:",
			"sample",
			0);
	}
	
	protected URL getInstallURL() {
		return CSEditorPlugin.getDefault().getInstallURL();
	}
	
	/* XXX should be removed and templates directory
	 * changed according to the schema version of the plugin
	 */
	protected String getTemplateDirectory() {
		return "templates"; //$NON-NLS-1$
	}
	
	public String getSectionId() {
		return "primitive"; //$NON-NLS-1$
	}
	
	public void addPages(Wizard wizard) {
		WizardPage page = createPage(0, "Template Primitive");
		page.setTitle("Primitive titre");
		page.setDescription("Primitive desc");
		wizard.addPage(page);
		markPagesAdded();
	}
	
	public void initializeFields(IPluginModelBase model) {
		String pluginId = model.getPluginBase().getId();
		initializeOption(KEY_PACKAGE_NAME, getFormattedPackageName(pluginId)); 
	}

	private String getFormattedPackageName(String name) {
		return name + ".primitives";
	}

	public String getUsedExtensionPoint() {
		return "fr.irisa.r2d2.gecos.framework.primitives"; //$NON-NLS-1$
	}
	
	public void validateOptions(TemplateOption source) {
		if (source.isRequired() && source.isEmpty()) {
			flagMissingRequiredOption(source);
		} else {
			//validateContainerPage(source);
		}
	}
	
	public String[] getNewFiles() {
		return new String[0];
	}
	
	protected void updateModel(IProgressMonitor monitor) throws CoreException {
		IPluginBase plugin = model.getPluginBase();
		IPluginExtension extension = createExtension("fr.irisa.r2d2.gecos.framework.primitives", true); //$NON-NLS-1$
		IPluginModelFactory factory = model.getPluginFactory();
		String fullClassName =
			getStringOption(KEY_PACKAGE_NAME) + "." + getStringOption(KEY_CLASS_NAME); //$NON-NLS-1$

		IPluginElement primitiveElement = factory.createElement(extension);
		primitiveElement.setName("primitive"); //$NON-NLS-1$
		primitiveElement.setAttribute("name", getStringOption(KEY_PRIMITIVE_NAME)); //$NON-NLS-1$
		primitiveElement.setAttribute("class", fullClassName);
		extension.add(primitiveElement);
		if (!extension.isInTheModel())
			plugin.add(extension);			
	}

	protected ResourceBundle getPluginResourceBundle() {
		Bundle bundle = Platform.getBundle(CSEditorPlugin.getPluginId());
		return Platform.getResourceBundle(bundle);
	}

}
