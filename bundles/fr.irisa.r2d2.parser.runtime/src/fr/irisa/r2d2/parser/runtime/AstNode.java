package fr.irisa.r2d2.parser.runtime;

public class AstNode {

	protected Span span;

	public AstNode(Span span) {
		this.span = span;
	}
	
	public boolean isAmbiguous() {
		return false;
	}

	public int from() {
		return span.from();
	}
	
	public int to() {
		return span.to();
	}

	public Span span() {
		return span;
	}

	public Span spanTo(AstNode to) {
		return new Span(span.from(), span.to());
	}

}
