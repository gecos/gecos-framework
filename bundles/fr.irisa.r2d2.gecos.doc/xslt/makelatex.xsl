<?xml version="1.0"?>

<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xhtml="http://www.w3.org/1999/xhtml">
		
	<!-- This stylesheet is used to build the documentation LaTeX file
		from help.xml -->
		
	<xsl:output method="text" indent="false"/>
	
	<!-- default rules: no recursion -->
	<!-- <xsl:template match="*"/> -->

	<xsl:template match="/help">
		<xsl:apply-templates select="xhtml:transform" />
	</xsl:template>

	<xsl:template match="xhtml:transform">
\section{\textsf{<xsl:value-of select="@name"/>}}
		<xsl:apply-templates select="node()" />
	</xsl:template>
	
	<xsl:template match="xhtml:h2">
\subsection*{<xsl:value-of select="text()"/>}
	</xsl:template>
	
	<xsl:template match="xhtml:h3">
\subsubsection*{<xsl:value-of select="text()"/>}
	</xsl:template>
	
	<xsl:template match="xhtml:ul">
\begin{itemize}
	<xsl:apply-templates select="node()" />
\end{itemize}
	</xsl:template>
	
	<xsl:template match="xhtml:li">
\item <xsl:apply-templates select="node()" />
	</xsl:template>
	
	<xsl:template match="xhtml:code">\texttt{<xsl:apply-templates select="text()"/>}</xsl:template>
	
	<xsl:template match="xhtml:p">
		<xsl:text>
</xsl:text>
		<xsl:apply-templates select="node()" />
		<xsl:text>
</xsl:text>
	</xsl:template>
	
	<xsl:template match="xhtml:pre">
		<xsl:variable name="contents"><xsl:apply-templates select="node()" /></xsl:variable>
\begin{verbatim}
		<xsl:value-of select="$contents"/>
\end{verbatim}
	</xsl:template>
	
</xsl:stylesheet>
