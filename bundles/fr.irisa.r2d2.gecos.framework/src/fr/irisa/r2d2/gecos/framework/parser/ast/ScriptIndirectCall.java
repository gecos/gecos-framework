


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
@SuppressWarnings("all")
public class ScriptIndirectCall extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode function;
	
	/**
	 * @generated
	 */
	protected AstList arguments ;
	
	/**
	 * @generated
	 */
	public ScriptIndirectCall(Span span, ScriptNode function, AstList arguments) {
		super(span);
		this.function = function;
		this.arguments = arguments;
	}
	
	/**
	 * @generated
	 */
	public ScriptNode getFunction() {
		return function;
	}
	
	/**
	 * @generated
	 */
	public AstList<ScriptNode> getArguments() {
		return arguments;
	}
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptIndirectCall(this);
	}
}