/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.text;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultTextHover;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.texteditor.ITextEditor;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;
import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;

/**
 * Return the description of the function whose name
 * is under mouse cursor. This text is to be printed in a
 * tooltip.
 * 
 * @author llhours
 */
public class CSTextHover extends DefaultTextHover {

//	private ITextEditor editor;

	public CSTextHover(ISourceViewer sourceViewer, ITextEditor editor) {
		super(sourceViewer);
//		this.editor = editor;
	}

	@SuppressWarnings("deprecation")
	public String getHoverInfo(ITextViewer textViewer, IRegion subject) {
		try {
			IDocument doc = textViewer.getDocument();
			String funcName = doc.get(subject.getOffset(), subject.getLength());
			IModule transform = CSEditorPlugin.getDefault().getModuleIndexer().getModule(funcName);

			if (transform != null) {
				String moduleDescription = transform.getDescription();
				
				/*
				 * XXX The following is quick hack to include the corresponding constructor description.
				 * The constructor is identified based on an estimation of its number of arguments:
				 * In case of multiple matches the constructor description is omitted.
				 * Also commands spread on multiple lines are not considered.
				 * TODO use document partitioning.. need a better implementation of the CSPartitionScanner.
				 */
				IRegion lineInfo = doc.getLineInformation(doc.getLineOfOffset(subject.getOffset() + subject.getLength()));
				String line = doc.get(lineInfo.getOffset(), lineInfo.getLength());
				if(line.trim().endsWith(");")) { //single line commands only
					int nbArgs = line.split(",").length;
					List<IModuleConstructor> constructors = transform.getConstructors().stream()
						.filter(mc -> mc.getArguments().size() == nbArgs)
						.collect(toList());
					if(constructors.size() == 1)
						moduleDescription += "\n\n" + constructors.get(0).getFullDescription();
				}
				
				return moduleDescription;
			}
			//IScriptCompilationUnit cu = ((ScriptDocumentProvider)editor.getDocumentProvider())
			//		.getScriptCompilationUnit(editor.getEditorInput());
			// cu.getElementAt(subject.getOffset(), subject.getLength())
		} catch (BadLocationException e) {
		}
		return super.getHoverInfo(textViewer, subject);
	}

	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		if (textViewer != null)
			return CSTextUtils.findWord(textViewer.getDocument(), offset);
		return null;
	}

}
