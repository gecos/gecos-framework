package fr.irisa.r2d2.gecos.framework.modules.timer;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Print the elapsed time since the last (re)start of a Timer.\n"
		+ "The timer is reset.\n"
		+ "\nSee: 'TimerStart' and 'TimerStop' modules.")
public class TimerRestart {
	
	private Object ID;
	
	/**
	 * Instantiates a new timer restart.
	 *
	 * @param ID the id
	 */
	@GSModuleConstructor("-arg1: Integer ID associated with the timer.")
	public TimerRestart(int ID) {
		this.ID = new Integer(ID);
	}
	
	/**
	 * Instantiates a new timer restart.
	 *
	 * @param ID the id
	 */
	@GSModuleConstructor("-arg1: String ID associated with the timer.")
	public TimerRestart(String ID) {
		this.ID = ID;
	}
	
	/**
	 * Instantiates a new timer restart.
	 */
	@GSModuleConstructor("Use Default Timer ID (as Integer '0')")
	public TimerRestart() {
		this(0);
	}
	
	/**
	 * Compute.
	 */
	public void compute() {
		if (ID instanceof String) {
			new TimerStop((String)ID).compute();
			new TimerStart((String)ID).compute();
		} else {
			new TimerStop((Integer)ID).compute();
			new TimerStart((Integer)ID).compute();
		}
	}

}
