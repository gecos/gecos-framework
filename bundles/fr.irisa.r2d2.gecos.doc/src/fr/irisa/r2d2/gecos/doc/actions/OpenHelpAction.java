package fr.irisa.r2d2.gecos.doc.actions;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.osgi.framework.Bundle;

import fr.irisa.r2d2.gecos.framework.GecosFramework;
import fr.irisa.r2d2.gecos.framework.IModule;

public class OpenHelpAction implements IEditorActionDelegate {
	
	private static final String GECOS_DOC_PLUGIN = "fr.irisa.r2d2.gecos.doc";
	private static final String GECOS_DOC_ROOT = "/doc/functions/";
	private String functionName;

	public OpenHelpAction() {
		functionName = "";
	}

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		
	}

	public void run(IAction action) {
		if (functionName.length() > 0) {
			IModule module = GecosFramework.getModuleIndexer().getModule(functionName);
	        if (module == null)
	        	return;
	        String file = GECOS_DOC_ROOT + module.getPackageName().replace(' ', '_')
	        		+ File.separator + module.getName() + ".html";
			URL url = getResourceURL(GECOS_DOC_PLUGIN, file);
			if (url != null)
				showURL(url);
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof ITextSelection) {
			ITextSelection textSelection = (ITextSelection) selection;
			if (textSelection.getLength() > 0)
				functionName = textSelection.getText();
		} else
			functionName = "";
	}
	
	private void showURL(URL url) {
		try {
			IWorkbenchBrowserSupport support = PlatformUI.getWorkbench().getBrowserSupport();
			IWebBrowser browser = support.createBrowser(
					IWorkbenchBrowserSupport.AS_EDITOR | IWorkbenchBrowserSupport.STATUS,
					"fr.irisa.r2d2.gecos", functionName, functionName); //$NON-NLS-1$
			browser.openURL(url); 
		} catch (PartInitException e) {
		}
	}
	
	private URL getResourceURL(String bundleID, String resourcePath) {
		try {
			Bundle bundle = Platform.getBundle(bundleID);
			if (bundle != null) {
				URL entry = bundle.getEntry(resourcePath);
				if (entry != null)
					return FileLocator.toFileURL(entry);
			}
		} catch (IOException e) {
		}
		return null;
	}

}
