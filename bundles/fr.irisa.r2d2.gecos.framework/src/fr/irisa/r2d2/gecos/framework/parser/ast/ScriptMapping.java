


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptMapping extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode key;
	
	/**
	 * @generated
	 */
	protected ScriptNode value;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptMapping(Span span, ScriptNode key, ScriptNode value) {
		super(span);
		this.key = key;
		this.value = value;
	}
	
	public ScriptNode getKey() {
		return key;
	}
	
	public ScriptNode getValue() {
		return value;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptMapping(this);
	}

}