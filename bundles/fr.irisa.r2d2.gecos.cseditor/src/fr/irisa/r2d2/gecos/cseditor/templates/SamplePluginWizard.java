/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.templates;

import org.eclipse.pde.ui.IFieldData;
import org.eclipse.pde.ui.templates.ITemplateSection;
import org.eclipse.pde.ui.templates.NewPluginTemplateWizard;

public class SamplePluginWizard extends NewPluginTemplateWizard {

	public void init(IFieldData data) {
		super.init(data);
		setWindowTitle("New Sample Gecos Plugin"); 
	}
	
	@Override
	public ITemplateSection[] createTemplateSections() {
		return new ITemplateSection[] { new ModuleTemplate() };
	}
}
