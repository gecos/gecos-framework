package fr.irisa.r2d2.gecos.framework.parser;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleArgument;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;
import fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAdd;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArgument;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArray;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAssignment;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptBoolean;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptChanging;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptCondition;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptDoLoop;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFloat;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptForLoop;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunction;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunctionCall;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptImport;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptIndirectCall;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptInteger;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMapping;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMinus;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNode;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNull;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptReturn;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptString;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptSub;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptVariable;
import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.IErrorReporter;
import fr.irisa.r2d2.parser.runtime.Span;

@SuppressWarnings("all")
public class SemanticAnalyser implements IGecosVisitor {
	
	public static abstract class Type {

		/**
		 * Checks if is enumerable.
		 *
		 * @return true, if is enumerable
		 */
		public boolean isEnumerable() {
			return false;
		}

		/**
		 * Checks if is unknown.
		 *
		 * @return true, if is unknown
		 */
		public boolean isUnknown() {
			return false;
		}

		/**
		 * Checks if is compatible with.
		 *
		 * @param type the type
		 * @return true, if is compatible with
		 */
		public abstract boolean isCompatibleWith(String type);

		/**
		 * Enumerable type.
		 *
		 * @return the type
		 */
		public Type enumerableType() {
			return null;
		}
		
	}
	
	public static class UnknownType extends Type {
		
		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isUnknown()
		 */
		public boolean isUnknown() {
			return true;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return "<unknown>";
		}

		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isCompatibleWith(java.lang.String)
		 */
		@Override
		public boolean isCompatibleWith(String type) {
			return true;
		}
	}
	
	public static class ListType extends Type {

		private Type elementType;

		/**
		 * Instantiates a new list type.
		 *
		 * @param type the type
		 */
		public ListType(Type type) {
			this.elementType = type;
		}
		
		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#enumerableType()
		 */
		public Type enumerableType() {
			return elementType;
		}
		
		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isEnumerable()
		 */
		public boolean isEnumerable() {
			return true;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return "List<" + elementType + ">";
		}

		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isCompatibleWith(java.lang.String)
		 */
		@Override
		public boolean isCompatibleWith(String type) {
			//System.out.println("ListType::isCompatibleWith(" + type + ")");
			return true;
		}
		
	}
	
	public static class ObjectType extends Type {

		private String javaClass;

		/**
		 * Instantiates a new object type.
		 *
		 * @param type the type
		 */
		public ObjectType(String type) {
			this.javaClass = type;
		}
		
		/**
		 * Gets the method.
		 *
		 * @return the method
		 * @throws Exception the exception
		 */
		private Method getMethod() throws Exception {
			Class cls = getClass().getClassLoader().loadClass(javaClass);
			Method m = cls.getMethod("elements", new Class[0]);
			if (m != null)
				return m;
			return cls.getMethod("iterator", new Class[0]);
		}
		
		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isEnumerable()
		 */
		public boolean isEnumerable() {
			try {
				Method m = getMethod();
				return m != null;
			} catch (Exception e) {
			}
			return true; // if an error occur say its enumerable to avoid multiple errors report
		}
		
		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#enumerableType()
		 */
		public Type enumerableType() {
			try {
				Class cls = getMethod().getReturnType();
				if (cls == Iterator.class || cls == Enumeration.class) {
					java.lang.reflect.Type[] types = cls.getGenericInterfaces();
					if (types == null || types.length == 0)
						return new UnknownType();
					// XXX
				}
				// should not be reached
				return new UnknownType();
			} catch (Exception e) {
				return new UnknownType();
			}
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return "{" + javaClass + "}";
		}

		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isCompatibleWith(java.lang.String)
		 */
		@Override
		public boolean isCompatibleWith(String type) {
			//System.out.println("ObjectType::isCompatibleWith(" + type + ")");
			return true;
		}
		
	}
	
	public static class MapType extends Type {

		private Type keyType;
		private Type valueType;

		/**
		 * Instantiates a new map type.
		 *
		 * @param keyType the key type
		 * @param valueType the value type
		 */
		public MapType(Type keyType, Type valueType) {
			this.keyType = keyType;
			this.valueType = valueType;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return keyType + " -> " + valueType;
		}

		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isCompatibleWith(java.lang.String)
		 */
		@Override
		public boolean isCompatibleWith(String type) {
			//System.out.println("MapType::isCompatibleWith(" + type + ")");
			return true;
		}
		
	}
	
	private static class BaseType extends Type {
		
		public static final int BOOLEAN = 0;
		public static final int FLOAT      = 1;
		public static final int INTEGER  = 2;
		public static final int STRING    = 3;
		public static final int VOID       = 4;
		
		int type;

		/**
		 * Instantiates a new base type.
		 *
		 * @param type the type
		 */
		public BaseType(int type) {
			this.type = type;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return type == BOOLEAN ? "boolean" :
				type == INTEGER ? "integer" : type == STRING ? "string" : "void";
		}

		/* (non-Javadoc)
		 * @see fr.irisa.r2d2.gecos.framework.parser.SemanticAnalyser.Type#isCompatibleWith(java.lang.String)
		 */
		@Override
		public boolean isCompatibleWith(String typeStr) {
			if (type == STRING)
				return true; // everything can be converted to String
			//System.out.println("BaseType::isCompatibleWith(" + type + ")");
			return true;
		}
		
	}
	
	private static class Scope {
		private Map<String, Type> varEnv = new HashMap<String, Type>();

		/**
		 * Put.
		 *
		 * @param span the span
		 * @param name the name
		 * @param t the t
		 */
		public void put(Span span, String name, Type t) {
			varEnv.put(name, t);
		}

		/**
		 * Gets the.
		 *
		 * @param name the name
		 * @return the type
		 */
		public Type get(String name) {
			return varEnv.get(name);
		}
	}

	private IErrorReporter reporter;
	private Stack<Type> typeStack = new Stack<Type>();
	private Scope scope = new Scope();
	private IModuleIndexer moduleIndexer;
	
	/**
	 * Instantiates a new semantic analyser.
	 *
	 * @param reporter the reporter
	 * @param moduleIndexer the module indexer
	 */
	public SemanticAnalyser(IErrorReporter reporter, IModuleIndexer moduleIndexer) {
		this.reporter = reporter;
		this.moduleIndexer = moduleIndexer;
	}

	/**
	 * Analyse.
	 *
	 * @param statements the statements
	 */
	public void analyse(AstList<ScriptNode> statements) {
		typeStack.clear();
		for (ScriptNode node : statements) {
			node.visit(this);
			Type t = typeStack.pop();
			// test type == VOID
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptArgument(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArgument)
	 */
	public void visitScriptArgument(ScriptArgument scriptArgument) {
		typeStack.push(new BaseType(BaseType.STRING));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptArray(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArray)
	 */
	public void visitScriptArray(ScriptArray scriptArray) {
		Type elementType = null;
		for (ScriptNode node : scriptArray.getElements()) {
			node.visit(this);
			Type t = typeStack.pop();
			if (elementType == null)
				elementType = t;
			else {
				// XXX verify homogeneity
			}
		}
		typeStack.push(new ListType(elementType != null ? elementType : new UnknownType()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptAssignment(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAssignment)
	 */
	public void visitScriptAssignment(ScriptAssignment scriptAssignment) {
		scriptAssignment.getExpression().visit(this);
		Type t = typeStack.pop();
		scope.put(scriptAssignment.span(), scriptAssignment.getVariable().getValue(), t);
		typeStack.push(new BaseType(BaseType.VOID));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptChanging(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptChanging)
	 */
	public void visitScriptChanging(ScriptChanging scriptChanging) {
		typeStack.push(new BaseType(BaseType.BOOLEAN));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptDoLoop(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptDoLoop)
	 */
	public void visitScriptDoLoop(ScriptDoLoop scriptDoLoop) {
		scriptDoLoop.getExpr().visit(this);
		Type exprType = typeStack.pop();
		if (! exprType.isCompatibleWith("java.lang.Boolean"))
			reporter.reportWarning(scriptDoLoop.getExpr().from(), scriptDoLoop.getExpr().span().length(),
					"condition must be of type boolean, found " + exprType);
		for (ScriptNode node : scriptDoLoop.getStatements()) {
			node.visit(this);
			Type t = typeStack.pop();
			// test type == VOID
		}
		typeStack.push(new BaseType(BaseType.VOID));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptForLoop(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptForLoop)
	 */
	public void visitScriptForLoop(ScriptForLoop scriptForLoop) {
		scriptForLoop.getExpression().visit(this);
		Type et = typeStack.pop();
		Type indexType = new UnknownType();
		if (! et.isUnknown()) {
			if (! et.isEnumerable())
				reporter.reportWarning(scriptForLoop.getExpression().from(), scriptForLoop.getExpression().span().length(),
					"expression of type '" + et + "' is not enumerable");
			else {
				indexType = et.enumerableType();
			}
		}
		scope.put(scriptForLoop.getIdent().span(), scriptForLoop.getIdent().getValue(), indexType);
		for (ScriptNode node : scriptForLoop.getStatements()) {
			node.visit(this);
			Type t = typeStack.pop();
			// test type == VOID
		}
		typeStack.push(new BaseType(BaseType.VOID));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptCondition(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptCondition)
	 */
	@Override
	public void visitScriptCondition(ScriptCondition scriptCondition) {
		scriptCondition.getExpression().visit(this);
		Type exprType = typeStack.pop();
		for (ScriptNode node : scriptCondition.getThenStatements()) {
			node.visit(this);
			Type t = typeStack.pop();
		}
		for (ScriptNode node : scriptCondition.getElseStatements()) {
			node.visit(this);
			Type t = typeStack.pop();
		}
		typeStack.push(new BaseType(BaseType.VOID));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptFunctionCall(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunctionCall)
	 */
	public void visitScriptFunctionCall(ScriptFunctionCall scriptFunctionCall) {
		List<Type> argumentsType = new ArrayList<Type>();
		for (ScriptNode node : scriptFunctionCall.getArguments()) {
			node.visit(this);
			argumentsType.add(typeStack.pop());
		}
		IModule module = moduleIndexer.getModule(scriptFunctionCall.getName().getValue());
		if (module == null) {
			if (! isPrimitive(scriptFunctionCall.getName().getValue()))
				reporter.reportWarning(scriptFunctionCall.from(), scriptFunctionCall.span().length(),
						"Unknown function " + scriptFunctionCall.getName());
			typeStack.push(new UnknownType());
		} else {
			boolean fullMatch = false;
			int maxArgsMatching = -1;
			IModuleConstructor bestMatch = null;
			for (IModuleConstructor cnst : module.getConstructors()) {
				List<IModuleArgument> args = cnst.getArguments();
				if (args.size() != argumentsType.size())
					continue;
				int argsMatching = 0;
				for (int i = 0; i < args.size(); ++i) {
					if (argumentsType.get(i).isCompatibleWith(args.get(i).typeSignature())) {
						++argsMatching;
					}
				}
				if (argsMatching == args.size()) {
					fullMatch = true;
					bestMatch = cnst;
					break;
				} else if (argsMatching > maxArgsMatching) {
					bestMatch = cnst;
					maxArgsMatching = argsMatching;
				}
			}
			if (! fullMatch) {
				if (bestMatch != null) {
					List<IModuleArgument> args = bestMatch.getArguments();
					for (int i = 0; i < args.size(); ++i) {
						if (! argumentsType.get(i).isCompatibleWith(args.get(i).typeSignature())) {
							ScriptNode arg = scriptFunctionCall.getArguments().get(i);
							reporter.reportError(arg.from(), arg.span().length(),
									"expecting type " + args.get(i).typeSignature() + ", found "
									+ argumentsType.get(i));
						}
					}
				}
			}
			String returnType = module.getReturnType();
			if (returnType.equals("I") || returnType.equals("Ljava.lang.Integer;")) {
				typeStack.push(new BaseType(BaseType.INTEGER));
			} else if (returnType.equals("Z") || returnType.equals("Ljava.lang.Boolean;")) {
				typeStack.push(new BaseType(BaseType.BOOLEAN));
			} else if (returnType.equals("Ljava.lang.String;")) {
				typeStack.push(new BaseType(BaseType.STRING));
			} else if (returnType.equals("V")) {
				typeStack.push(new BaseType(BaseType.VOID));
			} else
				typeStack.push(new ObjectType(returnType));
		}
		
	}
	
	public static final List<String> Primitives = Arrays.asList(new String[] {
			"read", "echo", "basename", "stripext", "dirname",
			"output", "help", "exit", "debug", "dump", "shell",
			"exec", "call", "callInPlace" });
	
	/**
	 * Checks if is primitive.
	 *
	 * @param name the name
	 * @return true, if is primitive
	 */
	private boolean isPrimitive(String name) {
		return Primitives.contains(name);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptReturn(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptReturn)
	 */
	public void visitScriptReturn(ScriptReturn aScriptReturn) {
		// XXX verify the context: must be in a function
		// XXX set return type of function to return type of expr
		//throw new RuntimeException("not implemented");
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptFunction(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunction)
	 */
	public void visitScriptFunction(ScriptFunction aScriptFunction) {
		//throw new RuntimeException("not implemented");
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptImport(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptImport)
	 */
	public void visitScriptImport(ScriptImport aScriptImport) {
		//throw new RuntimeException("not implemented");
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptIndirectCall(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptIndirectCall)
	 */
	public void visitScriptIndirectCall(ScriptIndirectCall scriptIndirectCall) {
		reporter.reportWarning(scriptIndirectCall.from(), scriptIndirectCall.span().length(),
				"Indirect call is for wizard, DON'T USE IT !");
		typeStack.push(new UnknownType());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptInteger(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptInteger)
	 */
	public void visitScriptInteger(ScriptInteger scriptInteger) {
		typeStack.push(new BaseType(BaseType.INTEGER));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptBoolean(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptBoolean)
	 */
	public void visitScriptBoolean(ScriptBoolean scriptBoolean) {
		typeStack.push(new BaseType(BaseType.BOOLEAN));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptFloat(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFloat)
	 */
	public void visitScriptFloat(ScriptFloat scriptFloat) {
		typeStack.push(new BaseType(BaseType.FLOAT));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptMapping(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMapping)
	 */
	public void visitScriptMapping(ScriptMapping scriptMapping) {
		scriptMapping.getKey().visit(this);
		Type keyType = typeStack.pop();
		scriptMapping.getValue().visit(this);
		Type valueType = typeStack.pop();
		typeStack.push(new MapType(keyType, valueType));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptAdd(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAdd)
	 */
	public void visitScriptAdd(ScriptAdd scriptAdd) {
		scriptAdd.getLeft().visit(this);
		Type leftType = typeStack.pop();
		scriptAdd.getRight().visit(this);
		Type rightType = typeStack.pop();
		
		typeStack.push(new UnknownType());
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptSub(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptSub)
	 */
	public void visitScriptSub(ScriptSub scriptSub) {
		scriptSub.getLeft().visit(this);
		Type leftType = typeStack.pop();
		scriptSub.getRight().visit(this);
		Type rightType = typeStack.pop();
		
		typeStack.push(new UnknownType());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptMinus(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMinus)
	 */
	public void visitScriptMinus(ScriptMinus scriptMinus) {
		scriptMinus.getExpr().visit(this);
		Type t1 = typeStack.pop();
		typeStack.push(new UnknownType());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptNull(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNull)
	 */
	public void visitScriptNull(ScriptNull scriptNull) {
		typeStack.push(new UnknownType());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptString(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptString)
	 */
	public void visitScriptString(ScriptString scriptString) {
		typeStack.push(new BaseType(BaseType.STRING));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptVariable(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptVariable)
	 */
	public void visitScriptVariable(ScriptVariable scriptVariable) {
		Type t = scope.get(scriptVariable.getName());
		if (t == null) {
			reporter.reportError(scriptVariable.from(), scriptVariable.span().length(),
					"variable '" + scriptVariable.getName() + "' is not initialized");
			typeStack.push(new UnknownType());
		} else {
			typeStack.push(t);
		}
	}

}
