/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework;

import java.io.File;
import java.io.PrintStream;
import java.io.Reader;

import fr.irisa.r2d2.gecos.framework.internal.GecosInternalFramework;
import fr.irisa.r2d2.gecos.framework.parser.ExecutionSemantic;
import fr.irisa.r2d2.parser.runtime.IErrorReporter;

public class GecosFramework {
	
	public final static PrintStream err = System.err;
	public final static PrintStream out = System.out;
	
	/**
	 * Instantiates a new gecos framework.
	 */
	private GecosFramework() {
	}
	
	/**
	 * Gets the module indexer.
	 *
	 * @return the module indexer
	 */
	public static IModuleIndexer getModuleIndexer() {
		return GecosInternalFramework.getDefault().getModuleIndexer();
	}

	/**
	 * Gets the run handler.
	 *
	 * @param file the file
	 * @return the run handler
	 */
	public static File getRunHandler(File file) {
		File parentFile = file.getAbsoluteFile().getParentFile();
		String absolutePath = parentFile.getAbsolutePath();
		String name = file.getName();
		File handle = new File(absolutePath+File.separator+"."+name+".run");
		return handle;
	}
	
	/**
	 * Evaluate.
	 *
	 * @param filename the filename
	 * @param reader the reader
	 * @param arguments the arguments
	 * @param reporter the reporter
	 * @return true, if successful
	 */
	public static boolean evaluate(String filename, Reader reader, String[] arguments, IErrorReporter reporter) {
		File handle = GecosFramework.getRunHandler(new File(filename));
		handle.deleteOnExit();
		ExecutionSemantic evaluator = new ExecutionSemantic(reporter);
		evaluator.setArguments(arguments);
		evaluator.execute(filename, reader);
		return evaluator.ranWithError();
	}

}
