package fr.irisa.r2d2.gecos.framework.parser;

import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleIndexer;
import fr.irisa.r2d2.gecos.framework.internal.GecosInternalFramework;
import fr.irisa.r2d2.gecos.framework.internal.Module;
import fr.irisa.r2d2.gecos.framework.internal.script.PrimitiveFactory;
import fr.irisa.r2d2.gecos.framework.internal.script.primitive.Exit.ExitException;
import fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAdd;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArgument;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArray;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAssignment;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptBoolean;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptChanging;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptCondition;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptDoLoop;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFloat;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptForLoop;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunction;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunctionCall;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptImport;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptIndirectCall;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptInteger;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMapping;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMinus;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNode;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNull;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptReturn;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptString;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptSub;
import fr.irisa.r2d2.gecos.framework.parser.ast.ScriptVariable;
import fr.irisa.r2d2.gecos.framework.script.IFunctionFactory;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.ArrayNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.BoolNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.FloatNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.IntegerNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.MappingNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.StringNode;
import fr.irisa.r2d2.gecos.framework.utils.ScriptLog;
import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.IErrorReporter;
import fr.irisa.r2d2.parser.runtime.Span;

public class ExecutionSemantic implements IGecosVisitor, IScriptEvaluator {
	
	private IErrorReporter reporter;
	
	/**
	 * Instantiates a new execution semantic.
	 *
	 * @param reporter the reporter
	 */
	public ExecutionSemantic(IErrorReporter reporter) {
		this.reporter = reporter;
		initFunctions();
		initSpecialVariables();
	}
	
	/**
	 * Report error.
	 *
	 * @param span the span
	 * @param msg the msg
	 */
	public void reportError(Span span, String msg) {
		reporter.reportError(span.from(), span.length(), msg);
	}
	
	/**
	 * Report warning.
	 *
	 * @param span the span
	 * @param msg the msg
	 */
	public void reportWarning(Span span, String msg) {
		reporter.reportWarning(span.from(), span.length(), msg);
	}
	
	/**
	 * Inits the functions.
	 */
	private void initFunctions() {
		BundleContext context = GecosInternalFramework.getDefault().getContext();
		ServiceReference<?> ref = context.getServiceReference(IExtensionRegistry.class.getName());
		IExtensionRegistry registry = (IExtensionRegistry) context.getService(ref);
		IConfigurationElement[] elements = registry.getConfigurationElementsFor(
				GecosInternalFramework.PLUGIN_ID,
				GecosInternalFramework.PL_PRIMITIVES);
		for (int i = 0; i < elements.length; ++i) {
			readPrimitive(elements[i]);
		}
		context.ungetService(ref);
		IModuleIndexer indexer = GecosInternalFramework.getDefault().getModuleIndexer();
		for (IModule module : indexer.getModules()) {
			registerFunction((Module) module);
		}
	}

	/**
	 * Read primitive.
	 *
	 * @param element the element
	 */
	private void readPrimitive(IConfigurationElement element) {
		String name = element.getAttribute("name");
		registerFunction(new PrimitiveFactory(name, element));
	}
	
	/**
	 * Inits the special variables.
	 */
	private void initSpecialVariables() {
	}
	
	/**
	 * Execute.
	 *
	 * @param filename the filename
	 * @param reader the reader
	 */
	@SuppressWarnings("unchecked")
	public void execute(String filename, Reader reader) {
		GecosParser parser = new GecosParser(new GecosLexer(reader, reporter), reporter);
		try {
			AstList<ScriptNode> stmts = (AstList<ScriptNode>) parser.parse().value;
			ScriptLog.getInstance().startScript(filename);
			eval(stmts);
			ScriptLog.getInstance().endScript();
		} catch(ScriptException e) {
			System.err.println(e.getLocalizedMessage());
			if (GecosInternalFramework.debugMode() > 0) {
				e.printStackTrace(System.err);
			}
			reportError(e.span(), e.getMessage());
			ranWithError = true;
		} catch(StopException e) {
			ranWithError = e.status != 0;
			if (GecosInternalFramework.debugMode() > 0) {
				if (e.getCause() instanceof ExitException) {
					System.err.println("Exit primitive with return code : "+e.status);
					throw e;
				}
				if (GecosInternalFramework.debugMode() < 2) {
					if (ranWithError) {
						Throwable ex = e;
						while (!(ex instanceof InvocationTargetException) && (ex.getCause() != null))
							ex = ex.getCause();
						if (ex instanceof InvocationTargetException)
							ex = ex.getCause();
						ex.printStackTrace(System.err);
					}
				} else {
					if (ranWithError) e.printStackTrace(System.err);
				}
			}
		} catch(Exception e) {
			System.err.println(e.getLocalizedMessage());
			if (GecosInternalFramework.debugMode() > 0) {
				e.printStackTrace(System.err);
			}
			ranWithError = true;
		}
	}
	
	// ======================================
	
	/**
	 * Eval.
	 *
	 * @param node the node
	 * @return the node
	 */
	private Node eval(ScriptNode node) {
		node.visit(this);
		return pop();
	}
	
	/**
	 * Eval.
	 *
	 * @param nodes the nodes
	 * @return the node[]
	 */
	private Node[] eval(AstList<ScriptNode> nodes) {
		for (ScriptNode node : nodes) {
			node.visit(this);
		}
		return pop(nodes.size());
	}
	
	/**
	 * Pop.
	 *
	 * @param size the size
	 * @return the node[]
	 */
	private Node[] pop(int size) {
		Node[] nodes = new Node[size];
		for (int i = size - 1; i >= 0; --i) {
			nodes[i] = pop();
		}
		return nodes;
	}
	
	private Stack<Node> stack = new Stack<Node>();
	
	/**
	 * Pop.
	 *
	 * @return the node
	 */
	private Node pop() {
		return stack.pop();
	}
	
	/**
	 * Push.
	 *
	 * @param node the node
	 */
	private void push(Node node) {
		stack.push(node);
	}
	
	// ======================================
	
	private static class Frame {
//		private Frame parent;
		private Map<String, Node> variables = new HashMap<String, Node>();
		
		/**
		 * Lookup.
		 *
		 * @param name the name
		 * @return the node
		 */
		public Node lookup(String name) {
			return variables.get(name);
		}
	}
	
	private Frame currentFrame = new Frame();
	private Node[] arguments;
	private boolean isChanging;
	private boolean ranWithError = false;
	private Map<String, IFunctionFactory> functions = new HashMap<String, IFunctionFactory>();
	
	/**
	 * Sets the arguments.
	 *
	 * @param argv the new arguments
	 */
	public void setArguments(String[] argv) {
		arguments = new Node[argv.length];
		for (int i = 0; i < argv.length; ++i) {
			arguments[i] = new StringNode(new Span(0,0), argv[i]);
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#getArgument(fr.irisa.r2d2.parser.runtime.Span, int)
	 */
	public Node getArgument(Span span, int number) {
		if (number >= 1 && number <= arguments.length) {
			return arguments[number-1];
		}
		reportWarning(span, "there is no argument $" + number+". Using empty String instead.");
		return new StringNode(span,"");
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#setVariable(fr.irisa.r2d2.parser.runtime.Span, java.lang.String, fr.irisa.r2d2.gecos.framework.script.nodes.Node)
	 */
	public void setVariable(Span span, String name, Node value) {
		currentFrame.variables.put(name, value);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#getVariable(fr.irisa.r2d2.parser.runtime.Span, java.lang.String)
	 */
	public Node getVariable(Span span, String name) {
		Node res = currentFrame.lookup(name);
		if (res == null) {
			reportError(span, "undefined variable '"+name+"'");
			return new NullNode(span);
		}
		return res;
	}
	
	/**
	 * Inits the changing.
	 */
	private void initChanging() {
		isChanging = false;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#isChanging()
	 */
	public boolean isChanging() {
		return isChanging;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#setChanging(boolean)
	 */
	public void setChanging(boolean changed) {
		isChanging |= changed;
	}
	
	
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#registerFunction(fr.irisa.r2d2.gecos.framework.script.IFunctionFactory)
	 */
	public void registerFunction(IFunctionFactory factory) {
		functions.put(factory.getName(), factory);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#apply(fr.irisa.r2d2.parser.runtime.Span, java.lang.String, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(Span span, String name, Node[] args) {
		IFunctionFactory desc = functions.get(name);
		if (desc == null)  {
			reportError(span, "unknown function \'" + name + "\'");
			throw new StopException(1);
		} else {
			try {
				IScriptEvaluable function = desc.getFunction();
				return function.apply(this, span, args);
			} catch (ScriptException e) {
				reportError(span, e.getMessage());
				throw new StopException(1,e);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#getDescription(java.lang.String)
	 */
	public String getDescription(String name) {
		IFunctionFactory desc = functions.get(name);
		if (desc == null)
			return "unknown function \'" + name + "\'";
		return desc.getDescription();
	}
	
	public static class StopException extends RuntimeException {
		private static final long serialVersionUID = 5485186109368928573L;
		int status;
		Throwable cause;
		
		/* (non-Javadoc)
		 * @see java.lang.Throwable#getCause()
		 */
		public Throwable getCause() { return this.cause; }
		
		/**
		 * Gets the status.
		 *
		 * @return the status
		 */
		public int getStatus() {return this.status; }
		
		/**
		 * Instantiates a new stop exception.
		 *
		 * @param status the status
		 */
		public StopException(int status) { this.status = status; }
		
		/**
		 * Instantiates a new stop exception.
		 *
		 * @param status the status
		 * @param cause the cause
		 */
		public StopException(int status, Throwable cause) { super(cause); this.status = status; this.cause = cause; }
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator#doExit(int)
	 */
	public void doExit(int status) {
		throw new StopException(status, new ExitException());
	}

	/**
	 * Ran with error.
	 *
	 * @return true, if successful
	 */
	public boolean ranWithError() {
		return ranWithError;
	}
	
	// ==============================================

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptArgument(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArgument)
	 */
	public void visitScriptArgument(ScriptArgument scriptArgument) {
		push(getArgument(scriptArgument.span(), scriptArgument.getValue().intValue()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptArray(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptArray)
	 */
	public void visitScriptArray(ScriptArray scriptArray) {
		push(new ArrayNode(scriptArray.span(), eval(scriptArray.getElements())));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptVariable(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptVariable)
	 */
	public void visitScriptVariable(ScriptVariable scriptVariable) {
		push(getVariable(scriptVariable.span(), scriptVariable.getName()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptAssignment(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAssignment)
	 */
	public void visitScriptAssignment(ScriptAssignment scriptAssignment) {
		setVariable(scriptAssignment.span(), scriptAssignment.getVariable().getValue(), eval(scriptAssignment.getExpression()));
		push(new NullNode(scriptAssignment.span()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptChanging(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptChanging)
	 */
	public void visitScriptChanging(ScriptChanging scriptChanging) {
		push(new BoolNode(scriptChanging.span(), isChanging()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptDoLoop(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptDoLoop)
	 */
	public void visitScriptDoLoop(ScriptDoLoop scriptDoLoop) {
		do {
			initChanging();
			eval(scriptDoLoop.getStatements());
		} while(eval(scriptDoLoop.getExpr()).asBoolean());
		push(new NullNode(scriptDoLoop.span()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptForLoop(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptForLoop)
	 */
	public void visitScriptForLoop(ScriptForLoop scriptForLoop) {
		Iterable<Node> elements = eval(scriptForLoop.getExpression()).getElements();
		if (elements != null) {
			for (Node n : elements) {
				setVariable(scriptForLoop.getIdent().span(), scriptForLoop.getIdent().getValue(), n);
				eval(scriptForLoop.getStatements());
			}
		}
		push(new NullNode(scriptForLoop.span()));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptCondition(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptCondition)
	 */
	@Override
	public void visitScriptCondition(ScriptCondition scriptCondition) {
		Node eval = eval(scriptCondition.getExpression());
		if (eval.asBoolean())
			eval(scriptCondition.getThenStatements());
		else
			eval(scriptCondition.getElseStatements());
		push(new NullNode(scriptCondition.span()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptFunction(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunction)
	 */
	public void visitScriptFunction(ScriptFunction scriptFunction) {
		throw new RuntimeException("not implemented");
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptReturn(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptReturn)
	 */
	public void visitScriptReturn(ScriptReturn scriptReturn) {
		throw new RuntimeException("not complete");
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptFunctionCall(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFunctionCall)
	 */
	public void visitScriptFunctionCall(ScriptFunctionCall scriptFunctionCall) {
		Node[] arguments = eval(scriptFunctionCall.getArguments());
		push(apply(scriptFunctionCall.span(), scriptFunctionCall.getName().getValue(), arguments));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptImport(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptImport)
	 */
	public void visitScriptImport(ScriptImport scriptImport) {
		throw new RuntimeException("not implemented");
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptIndirectCall(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptIndirectCall)
	 */
	public void visitScriptIndirectCall(ScriptIndirectCall scriptIndirectCall) {
		String name = eval(scriptIndirectCall.getFunction()).asString().value();
		if (name == null) {
			reportError(scriptIndirectCall.getFunction().span(), "expect a string as name in function call");
			push(new NullNode(scriptIndirectCall.span()));
		} else {
			Node[] arguments = eval(scriptIndirectCall.getArguments());
			push(apply(scriptIndirectCall.span(), name, arguments));
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptInteger(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptInteger)
	 */
	public void visitScriptInteger(ScriptInteger scriptInteger) {
		push(new IntegerNode(scriptInteger.span(), scriptInteger.getValue().intValue()));
	}


	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptBoolean(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptBoolean)
	 */
	public void visitScriptBoolean(ScriptBoolean scriptBoolean) {
		push(new BoolNode(scriptBoolean.span(), scriptBoolean.getValue().booleanValue()));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptFloat(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptFloat)
	 */
	public void visitScriptFloat(ScriptFloat scriptFloat) {
		push(new FloatNode(scriptFloat.span(), scriptFloat.getValue().doubleValue()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptMapping(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMapping)
	 */
	public void visitScriptMapping(ScriptMapping scriptMapping) {
		push(new MappingNode(scriptMapping.span(), eval(scriptMapping.getKey()), eval(scriptMapping.getValue())));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptAdd(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptAdd)
	 */
	public void visitScriptAdd(ScriptAdd scriptAdd) {
		Node l = eval(scriptAdd.getLeft());
		Node r = eval(scriptAdd.getRight());
		push(l.applyPlus(r));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptSub(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptSub)
	 */
	public void visitScriptSub(ScriptSub scriptSub) {
		Node l = eval(scriptSub.getLeft());
		Node r = eval(scriptSub.getRight());
		push(l.applyMinus(r));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptMinus(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptMinus)
	 */
	public void visitScriptMinus(ScriptMinus scriptMinus) {
		Integer e = eval(scriptMinus.getExpr()).asInteger();
		push(new IntegerNode(scriptMinus.span(), e.intValue()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptNull(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptNull)
	 */
	public void visitScriptNull(ScriptNull scriptNull) {
		push(new NullNode(scriptNull.span()));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.parser.ast.IGecosVisitor#visitScriptString(fr.irisa.r2d2.gecos.framework.parser.ast.ScriptString)
	 */
	public void visitScriptString(ScriptString scriptString) {
		push(new StringNode(scriptString.span(), scriptString.getValue()));
	}

}
