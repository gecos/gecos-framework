

package fr.irisa.r2d2.gecos.framework.parser.ast;


/**
 * @generated
 */
public interface IGecosVisitor {

	/**
	 * @generated
	 */
	void visitScriptAssignment(ScriptAssignment aScriptAssignment);
	/**
	 * @generated
	 */
	void visitScriptFunctionCall(ScriptFunctionCall aScriptFunctionCall);
	/**
	 * @generated
	 */
	void visitScriptForLoop(ScriptForLoop aScriptForLoop);
	/**
	 * @generated
	 */
	void visitScriptDoLoop(ScriptDoLoop aScriptDoLoop);
	/**
	 * @generated
	 */
	void visitScriptMapping(ScriptMapping aScriptMapping);
	/**
	 * @generated
	 */
	void visitScriptAdd(ScriptAdd aScriptAdd);
	/**
	 * @generated
	 */
	void visitScriptSub(ScriptSub aScriptSub);
	/**
	 * @generated
	 */
	void visitScriptString(ScriptString aScriptString);
	/**
	 * @generated
	 */
	void visitScriptNull(ScriptNull aScriptNull);
	/**
	 * @generated
	 */
	void visitScriptArgument(ScriptArgument aScriptArgument);
	/**
	 * @generated
	 */
	void visitScriptChanging(ScriptChanging aScriptChanging);
	/**
	 * @generated
	 */
	void visitScriptInteger(ScriptInteger aScriptInteger);
	/**
	 * @generated
	 */
	void visitScriptMinus(ScriptMinus aScriptMinus);
	/**
	 * @generated
	 */
	void visitScriptVariable(ScriptVariable aScriptVariable);
	/**
	 * @generated
	 */
	void visitScriptArray(ScriptArray aScriptArray);
	/**
	 * @generated
	 */
	void visitScriptIndirectCall(ScriptIndirectCall aScriptIndirectCall);
	
	void visitScriptFunction(ScriptFunction aScriptFunction);
	
	void visitScriptReturn(ScriptReturn aScriptReturn);
	
	void visitScriptImport(ScriptImport aScriptImport);
	
	void visitScriptFloat(ScriptFloat scriptFloat);

	void visitScriptCondition(ScriptCondition scriptCondition);
	
	void visitScriptBoolean(ScriptBoolean scriptBoolean);
}