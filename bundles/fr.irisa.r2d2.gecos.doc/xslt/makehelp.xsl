<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
		
	<xsl:output method="xml" indent="yes"/>
	
	 <xsl:template match="/">
	 	<xsl:apply-templates select="xhtml:html/xhtml:body/xhtml:div/xhtml:div[@class='page']/xhtml:h1"/>
	 </xsl:template>
	 
	  <xsl:template match="xhtml:h1">
	 	<xsl:variable name="name" select="xhtml:a/text()"/>
	 	<transform name="{$name}">
	 		<xsl:apply-templates
				select="following-sibling::xhtml:div[@class='level1' and position() = 1]/node()"/>
	 		<xsl:apply-templates
				select="following-sibling::xhtml:h2[generate-id(following-sibling::xhtml:h1[1])
					= generate-id(current()/following-sibling::xhtml:h1[1])]"/>
	 	</transform>
	 </xsl:template>

	 <xsl:template match="xhtml:h2">
	 	<xsl:variable name="name" select="xhtml:a/text()"/>
	 	<div class="level2">
	 		<h2><xsl:value-of select="$name"/></h2>
	 		<xsl:apply-templates
				select="following-sibling::xhtml:div[@class='level2' and position() = 1]/node()"/>
	 		<xsl:apply-templates
				select="following-sibling::xhtml:h3[generate-id(following-sibling::xhtml:h2[1])
					= generate-id(current()/following-sibling::xhtml:h2[1])]"/>
	 	</div>
	 </xsl:template>

	 <xsl:template match="xhtml:h3">
	 	<xsl:variable name="name" select="xhtml:a/text()"/>
	 	<div class="level3">
			<h3><xsl:value-of select="$name"/></h3>
	 		<xsl:apply-templates
				select="following-sibling::xhtml:div[@class='level3' and position() = 1]/node()"/>
	 		<xsl:apply-templates
				select="following-sibling::xhtml:h4[generate-id(following-sibling::xhtml:h3[1])
					= generate-id(current()/following-sibling::xhtml:h3[1])]"/>
	 	</div>
	 </xsl:template>

	<xsl:template match="xhtml:span[@class='gecos-type']">
		<xsl:variable name="ref"
			select="document('typedb.xml')/types/transform[@name=current()/text()]"/>
		<a class="trans-ref" href="../{translate($ref/@module,' ','_')}/{text()}.html"><xsl:value-of select="text()"/></a>
	</xsl:template>

	<xsl:template match="xhtml:span[@class='re1']">
		<xsl:variable name="ref"
			select="document('typedb.xml')/types/transform[@name=current()/text()]"/>
		<a class="re1" href="../{translate($ref/@module,' ','_')}/{text()}.html"><xsl:value-of select="text()"/></a>
	</xsl:template>

	<!--<xsl:template match="text()">
		<xsl:value-of select="normalize-space(.)"/>
	</xsl:template>-->

	<xsl:template match="@*|node()">
		<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
