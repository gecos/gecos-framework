/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Iterator;

import fr.irisa.r2d2.parser.runtime.Span;

@SuppressWarnings("all")
public class ObjectNode extends Node {
	private Object object;
	
	/**
	 * Instantiates a new object node.
	 *
	 * @param span the span
	 * @param obj the obj
	 */
	public ObjectNode(Span span, Object obj) {
		super(span);
		this.object = obj;
	}
	
	private static class ObjectEnumeration implements Iterator {
		private Enumeration e;
		private Span span;
		
		/**
		 * Instantiates a new object enumeration.
		 *
		 * @param e the e
		 * @param span the span
		 */
		public ObjectEnumeration(Enumeration e, Span span) { this.e = e; this.span = span;}
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		public boolean hasNext() { return e.hasMoreElements(); }
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		public Object next() {
			return new ObjectNode(span, e.nextElement());
		}
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		public void remove() {
		}
	}
	
	private static class ObjectIterator implements Iterator {
		private Iterator e;
		private Span span;
		
		/**
		 * Instantiates a new object iterator.
		 *
		 * @param e the e
		 * @param span the span
		 */
		public ObjectIterator(Iterator e, Span span) { this.e = e; this.span = span; }
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		public boolean hasNext() { return e.hasNext(); }
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		public Object next() {
			return new ObjectNode(span, e.next());
		}
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		public void remove() {
		}
	}

	/**
	 * Use a special iterator to encapsulate the collection elements into
	 * ScriptNode. The enumerated objects can use the Enumeration or Iterator interfaces.
	 *
	 * @return the elements
	 */
	public Iterable<Node> getElements() {
		return new Iterable<Node>() {
			public Iterator<Node> iterator() {
				try {
					Method method = object.getClass().getMethod("elements", new Class[0]);
					return new ObjectEnumeration(
						(Enumeration)method.invoke(object, new Object[0]), span);
				} catch(Exception e) {
					// try next solution
				}
				try {
					Method method = object.getClass().getMethod("iterator", new Class[0]);
					return new ObjectIterator(
						(Iterator)method.invoke(object, new Object[0]), span);
				} catch(Exception e) {
					return null;
				}
			}
		};
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asString()
	 */
	public StringNode asString() {
		return new StringNode(span, object.toString());
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject()
	 */
	public Object asObject() {
		return object;
	}
}