package fr.irisa.r2d2.gecos.framework.parser.ast;

import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

@SuppressWarnings("all")
public class ScriptFunction extends ScriptNode {

	private ScriptIdent name;
	private AstList args;
	private AstList body;

	public ScriptFunction(Span span, ScriptIdent name, AstList args, AstList body) {
		super(span);
		this.name = name;
		this.args = args;
		this.body = body;
	}

	@Override
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptFunction(this);
	}

}
