/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/

package fr.irisa.r2d2.gecos.framework.internal.script.primitive;


import fr.irisa.r2d2.gecos.framework.parser.ExecutionSemantic;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.parser.runtime.Span;

/** FuncExit is used to ...
 * 
 * @author llhours
 */
public class Exit implements IScriptEvaluable {
	
	public static class ExitException extends Exception {
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * Instantiates a new exit.
	 */
	public Exit() { }

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable#apply(fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator, fr.irisa.r2d2.parser.runtime.Span, fr.irisa.r2d2.gecos.framework.script.nodes.Node[])
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args) throws ScriptException {
		int status = 0;
		if (args.length > 0) {
			status = args[0].asInteger().intValue();
			if (status != 0) {
				if (evaluator instanceof ExecutionSemantic) {
					((ExecutionSemantic) evaluator).reportError(span, "return code is not 0");
				}
			}
		}
		evaluator.doExit(status);
		return new NullNode(span);
	}

}
