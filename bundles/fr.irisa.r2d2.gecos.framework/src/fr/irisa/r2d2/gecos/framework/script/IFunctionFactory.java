/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.script;


public interface IFunctionFactory {

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * Gets the function.
	 *
	 * @return the function
	 * @throws ScriptException the script exception
	 */
	public abstract IScriptEvaluable getFunction() throws ScriptException;

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public abstract String getDescription();

}