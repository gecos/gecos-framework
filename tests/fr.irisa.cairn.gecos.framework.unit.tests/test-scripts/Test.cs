
debug(2);

# Framework test script


i = "5";
k = 5 + i;
echo(k);
aa = atoi(i);
m = 5 + aa;
echo(m);

if m then
	echo("m != 0");
fi;

echo("Hello I'm a Gecos Script !");

fruits = { "banana", "apple", "orange" ,""};

for fruit in fruits do
	if fruit then
		echo("Fruit: " + fruit);
	else
		echo("empty string");
	fi;
done;

sizes = { "float" -> 32 + 1, "int" -> 32, "double" -> 64 };
echo(sizes);

#prints all available Modules in the file dump.xml
dump();

res = shell("echo \"\" && echo \"$PWD\" && ls && echo \"\"");
echo ("return code for previous shell command : "+res);

# delete dump file
shell("rm ./dump.xml");

call("sub_script.cs","String argument");

res = 3;
do
	echo(res);
	res = res - 1;
while res; #stops when res == 0

#return 0;


numberOfProcessors=4;
steps = 10;
call("second.cs", numberOfProcessors, steps);
