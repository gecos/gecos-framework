package java_cup.runtime;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import fr.irisa.r2d2.parser.runtime.IErrorReporter;


public abstract class CorrectingLRParser {

	protected IErrorReporter reporter;

	/**
	 * Instantiates a new correcting LR parser.
	 */
	public CorrectingLRParser() {
	}

	/**
	 * Instantiates a new correcting LR parser.
	 *
	 * @param s the s
	 */
	public CorrectingLRParser(Scanner s) {
		this();
		setScanner(s);
	}

	/**
	 *  Table of production information (supplied by generated subclass).
	 *  This table contains one entry per production and is indexed by 
	 *  the negative-encoded values (reduce actions) in the action_table.  
	 *  Each entry has two parts, the index of the non-terminal on the 
	 *  left hand side of the production, and the number of Symbols 
	 *  on the right hand side.
	 *
	 * @return the short[][]
	 */
	public abstract short[][] production_table();

	/**
	 *  The action table (supplied by generated subclass).  This table is
	 *  indexed by state and terminal number indicating what action is to
	 *  be taken when the parser is in the given state (i.e., the given state 
	 *  is on top of the stack) and the given terminal is next on the input.  
	 *  States are indexed using the first dimension, however, the entries for 
	 *  a given state are compacted and stored in adjacent index, value pairs 
	 *  which are searched for rather than accessed directly (see get_action()).  
	 *  The actions stored in the table will be either shifts, reduces, or 
	 *  errors.  Shifts are encoded as positive values (one greater than the 
	 *  state shifted to).  Reduces are encoded as negative values (one less 
	 *  than the production reduced by).  Error entries are denoted by zero. 
	 *
	 * @return the short[][]
	 * @see CorrectingLRParser#get_action
	 */
	public abstract short[][] action_table();

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  The reduce-goto table (supplied by generated subclass).  This
	 *  table is indexed by state and non-terminal number and contains
	 *  state numbers.  States are indexed using the first dimension, however,
	 *  the entries for a given state are compacted and stored in adjacent
	 *  index, value pairs which are searched for rather than accessed 
	 *  directly (see get_reduce()).  When a reduce occurs, the handle 
	 *  (corresponding to the RHS of the matched production) is popped off 
	 *  the stack.  The new top of stack indicates a state.  This table is 
	 *  then indexed by that state and the LHS of the reducing production to 
	 *  indicate where to "shift" to. 
	 *
	 * @return the short[][]
	 * @see CorrectingLRParser#get_reduce
	 */
	public abstract short[][] reduce_table();

	/**
	 *  The index of the start state (supplied by generated subclass).
	 *
	 * @return the int
	 */
	public abstract int start_state();

	/**
	 *  The index of the start production (supplied by generated subclass).
	 *
	 * @return the int
	 */
	public abstract int start_production();

	/**
	 *  The index of the end of file terminal Symbol (supplied by generated 
	 *  subclass).
	 *
	 * @return the int
	 */
	public abstract int EOF_sym();

	/**
	 *  The index of the special error Symbol (supplied by generated subclass).
	 *
	 * @return the int
	 */
	public abstract int error_sym();

	/** Internal flag to indicate when parser should quit. */
	protected boolean _done_parsing = false;

	/** This method is called to indicate that the parser should quit.  This is 
	 *  normally called by an accept action, but can be used to cancel parsing 
	 *  early in other circumstances if desired. 
	 */
	public void done_parsing() {
		_done_parsing = true;
	}

	/** Indication of the index for top of stack (for use by actions). */
	protected int tos;

	/** The current lookahead Symbol. */
	protected Symbol cur_token;

	/** The parse stack itself. */
	protected Stack<?> stack = new Stack<Object>();

	/** Direct reference to the production table. */
	protected short[][] production_tab;

	/** Direct reference to the action table. */
	protected short[][] action_tab;

	/** Direct reference to the reduce-goto table. */
	protected short[][] reduce_tab;

	/** This is the scanner object used by the default implementation
	 *  of scan() to get Symbols.  To avoid name conflicts with existing
	 *  code, this field is private. [CSA/davidm] */
	private Scanner _scanner;

	/**
	 * Simple accessor method to set the default scanner.
	 *
	 * @param s the new scanner
	 */
	public void setScanner(Scanner s) {
		_scanner = s;
	}
	
	/**
	 * Sets the error reporter.
	 *
	 * @param reporter the new error reporter
	 */
	public void setErrorReporter(IErrorReporter reporter) {
		this.reporter = reporter;
	}

	/**
	 * Simple accessor method to get the default scanner.
	 *
	 * @return the scanner
	 */
	public Scanner getScanner() {
		return _scanner;
	}

	/**
	 *  Perform a bit of user supplied action code (supplied by generated 
	 *  subclass).  Actions are indexed by an internal action number assigned
	 *  at parser generation time.
	 *
	 * @param act_num   the internal index of the action to be performed.
	 * @param parser    the parser object we are acting for.
	 * @param stack     the parse stack of that object.
	 * @param top       the index of the top element of the parse stack.
	 * @return the symbol
	 * @throws Exception the exception
	 */
	public abstract Symbol do_action(int act_num, CorrectingLRParser parser,
			Stack<Symbol> stack, int top) throws java.lang.Exception;

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  User code for initialization inside the parser.  Typically this 
	 *  initializes the scanner.  This is called before the parser requests
	 *  the first Symbol.  Here this is just a placeholder for subclasses that 
	 *  might need this and we perform no action.   This method is normally
	 *  overridden by the generated code using this contents of the "init with"
	 *  clause as its body.
	 *
	 * @throws Exception the exception
	 */
	public void user_init() throws java.lang.Exception {
	}

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  Initialize the action object.  This is called before the parser does
	 *  any parse actions. This is filled in by generated code to create
	 *  an object that encapsulates all action code.
	 *
	 * @throws Exception the exception
	 */
	protected abstract void init_actions() throws java.lang.Exception;

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  Get the next Symbol from the input (supplied by generated subclass).
	 *  Once end of file has been reached, all subsequent calls to scan 
	 *  should return an EOF Symbol (which is Symbol number 0).  By default
	 *  this method returns getScanner().next_token(); this implementation
	 *  can be overriden by the generated parser using the code declared in
	 *  the "scan with" clause.  Do not recycle objects; every call to
	 *  scan() should return a fresh object.
	 *
	 * @return the symbol
	 * @throws Exception the exception
	 */
	public Symbol scan() throws java.lang.Exception {
		Symbol sym = getScanner().next_token();
		return (sym != null) ? sym : new Symbol(EOF_sym());
	}

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  Fetch an action from the action table.  The table is broken up into
	 *  rows, one per state (rows are indexed directly by state number).  
	 *  Within each row, a list of index, value pairs are given (as sequential
	 *  entries in the table), and the list is terminated by a default entry 
	 *  (denoted with a Symbol index of -1).  To find the proper entry in a row 
	 *  we do a linear or binary search (depending on the size of the row).  
	 *
	 * @param state the state index of the action being accessed.
	 * @param sym   the Symbol index of the action being accessed.
	 * @return the action
	 */
	protected final short get_action(int state, int sym) {
		short tag;
		int first, last, probe;
		short[] row = action_tab[state];

		/* linear search if we are < 10 entries */
		if (row.length < 20)
			for (probe = 0; probe < row.length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				tag = row[probe++];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					return row[probe];
				}
			}
		/* otherwise binary search */
		else {
			first = 0;
			last = (row.length - 1) / 2 - 1; /* leave out trailing default entry */
			while (first <= last) {
				probe = (first + last) / 2;
				if (sym == row[probe * 2])
					return row[probe * 2 + 1];
				else if (sym > row[probe * 2])
					first = probe + 1;
				else
					last = probe - 1;
			}

			/* not found, use the default at the end */
			return row[row.length - 1];
		}

		/* shouldn't happened, but if we run off the end we return the 
		default (error == 0) */
		return 0;
	}

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  Fetch a state from the reduce-goto table.  The table is broken up into
	 *  rows, one per state (rows are indexed directly by state number).  
	 *  Within each row, a list of index, value pairs are given (as sequential
	 *  entries in the table), and the list is terminated by a default entry 
	 *  (denoted with a Symbol index of -1).  To find the proper entry in a row 
	 *  we do a linear search.  
	 *
	 * @param state the state index of the entry being accessed.
	 * @param sym   the Symbol index of the entry being accessed.
	 * @return the reduce
	 */
	protected final short get_reduce(int state, int sym) {
		short tag;
		short[] row = reduce_tab[state];

		/* if we have a null row we go with the default */
		if (row == null)
			return -1;

		for (int probe = 0; probe < row.length; probe++) {
			/* is this entry labeled with our Symbol or the default? */
			tag = row[probe++];
			if (tag == sym || tag == -1) {
				/* return the next entry */
				return row[probe];
			}
		}
		/* if we run off the end we return the default (error == -1) */
		return -1;
	}

	/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

	/**
	 *  This method provides the main parsing routine.  It returns only when 
	 *  done_parsing() has been called (typically because the parser has 
	 *  accepted, or a fatal error has been reported).  See the header 
	 *  documentation for the class regarding how shift/reduce parsers operate
	 *  and how the various tables are used.
	 *
	 * @return the symbol
	 * @throws Exception the exception
	 */
	public Symbol parse() throws java.lang.Exception {
		production_tab = production_table();
		action_tab = action_table();
		reduce_tab = reduce_table();

		init_actions();
		user_init();

		LexState lexState = new LexState();
		ParseStack stack = new ParseStack(new Symbol(0, start_state()));
		Fifo fifo = new Fifo();
		fifo.put(stack, lexState);
		//ParseState state = new ParseState(lexState, stack, fifo);
		
		while (true) {
			DistanceResult da = distanceParse(lexState, stack, fifo, lookahead - fifo.size());
			if (da.distance == 0 || da.action == DistanceResult.ACCEPT)
				return normalParse(da.lexState, da.stack, da.fifo);
			fixError(fifo);
			lexState = fifo.top().lexState;
			stack = fifo.top().stack;
		}
	}
	
	private static final int lookahead = 15;
	private static final int minDelta = 3;
	private static final int minAdvance = 1;
	private static final int maxAdvance = 5;
	
	/**
	 * Try to correct the input token stream using the parser states
	 * saved in the fifo.
	 *
	 * @param fifo the fifo
	 * @throws Exception the exception
	 */
	private void fixError(Fifo fifo) throws Exception {

		List<Change> changes = new ArrayList<Change>();
		
		// try several kinds of correction at every position in the queue
		int position = fifo.size() - 1;
		for (FifoElement element : fifo) {
			tryDelete(1, element.lexState, element.stack, position, changes);
			tryDelete(2, element.lexState, element.stack, position, changes);
			tryDelete(3, element.lexState, element.stack, position, changes);
			trySubst(element.lexState, element.stack, position, changes);
			tryInsert(element.lexState, element.stack, position, changes);
			--position;
		}
		Change change = findBestChange(changes);
		reporter.reportError(change.leftPos, change.rightPos - change.leftPos,
				"syntax error: " + change.toString(this));
		FifoElement element = fifo.dropLast(change.pos);
		LexState lexState = element.lexState;
		for (int i = 0; i < change.orig.size(); ++i) {
			lexState = lexState.getNextState();
		}
		for (Symbol sym : change._new) {
			lexState = new LexState(sym, lexState);
		}
		fifo.put(element.stack, lexState);
	}
	
	/**
	 * Use some heuristics to find the best change
	 * in the list of possible change.
	 *
	 * @param changes the changes
	 * @return the best change
	 */
	private Change findBestChange(List<Change> changes) {
		List<Change> maxChanges = new ArrayList<Change>();
		int maxDistance = 0;
		for (Change change : changes) {
			if (maxChanges.isEmpty() || change.distance >= maxDistance) {
				if (change.distance > maxDistance)
					maxChanges.clear();
				maxChanges.add(change);
				maxDistance = change.distance;
			}
		}
		if (maxChanges.isEmpty())
			throw new RuntimeException("no valuable correction");
		Change deletion = null;
		for (Change change : maxChanges) {
			if (change.isInsertion())
				return change;
			if (change.isDeletion())
				deletion = change;
		}
		if (deletion != null)
			return deletion;
		return maxChanges.get(0);
	}
	
	/**
	 * Try delete.
	 *
	 * @param n the n
	 * @param lex the lex
	 * @param stack the stack
	 * @param queuePos the queue pos
	 * @param results the results
	 * @throws Exception the exception
	 */
	private void tryDelete(int n, LexState lex, ParseStack stack, int queuePos, List<Change> results) throws Exception {
		Symbol term = lex.get();
		int left = term.left;
		int right = term.right;
		List<Symbol> accum = new ArrayList<Symbol>();
		while (n > 0) {
			if (noShift(term.sym)) 
				return;
			n = n-1;
			accum.add(term);
			right = term.right;
			lex = lex.getNextState();
			term = lex.get();
		}
		tryChange(lex, stack, queuePos, left, right, accum, new ArrayList<Symbol>(), results);
	}
	
	/**
	 * Try insert.
	 *
	 * @param lex the lex
	 * @param stack the stack
	 * @param queuePos the queue pos
	 * @param results the results
	 * @throws Exception the exception
	 */
	private void tryInsert(LexState lex, ParseStack stack, int queuePos, List<Change> results) throws Exception {
		for (int t : terms(stack.symbol.parse_state)) {
			tryChange(lex, stack, queuePos,
					lex.leftPos(), lex.leftPos(),
					new ArrayList<Symbol>(),
					makeList(createToken(t, lex.leftPos())), results);
		}
	}
	
	/**
	 * Try subst.
	 *
	 * @param lex the lex
	 * @param stack the stack
	 * @param queuePos the queue pos
	 * @param results the results
	 * @throws Exception the exception
	 */
	private void trySubst(LexState lex, ParseStack stack, int queuePos, List<Change> results) throws Exception {
		Symbol orig = lex.get();
		if (! noShift(orig.sym)) {
			for (int t : terms(stack.symbol.parse_state)) {
				tryChange(lex.getNextState(), stack, queuePos,
		            				orig.left, orig.right,
		            				makeList(orig),
		            				makeList(createToken(t, orig.right)), results);
			}
		}
	}
	
	/**
	 * Make list.
	 *
	 * @param sym the sym
	 * @return the list
	 */
	private static List<Symbol> makeList(Symbol sym) {
		List<Symbol> res = new ArrayList<Symbol>();
		res.add(sym);
		return res;
	}
	
	/**
	 * Creates the token.
	 *
	 * @param t the t
	 * @param pos the pos
	 * @return the symbol
	 */
	private Symbol createToken(int t, int pos) {
		return new Symbol(t, pos, pos, errorTokenValue(t));
	}
	
	private class LexState {

		private Symbol symbol;
		private LexState next;

		/**
		 * Instantiates a new lex state.
		 *
		 * @throws Exception the exception
		 */
		public LexState() throws Exception {
			symbol = scan();
			next = null;
		}
		
		/**
		 * Left pos.
		 *
		 * @return the int
		 */
		public int leftPos() {
			return symbol.left;
		}

		/**
		 * Gets the.
		 *
		 * @return the symbol
		 */
		public Symbol get() {
			return symbol;
		}

		/**
		 * Gets the next state.
		 *
		 * @return the next state
		 * @throws Exception the exception
		 */
		public LexState getNextState() throws Exception {
			if (next == null) {
				next = new LexState();
			}
			return next;
		}

		/**
		 * Instantiates a new lex state.
		 *
		 * @param symbol the symbol
		 * @param next the next
		 */
		public LexState(Symbol symbol, LexState next) {
			this.symbol = symbol;
			this.next = next;
		}
		
	}
	
	private static class ParseStack {

		private Symbol symbol;
		private ParseStack next;

		/**
		 * Instantiates a new parses the stack.
		 *
		 * @param symbol the symbol
		 */
		public ParseStack(Symbol symbol) {
			this(symbol, null);
		}

		/**
		 * Instantiates a new parses the stack.
		 *
		 * @param symbol the symbol
		 * @param next the next
		 */
		public ParseStack(Symbol symbol, ParseStack next) {
			this.symbol = symbol;
			this.next = next;
		}

		/**
		 * Gets the symbol.
		 *
		 * @return the symbol
		 */
		public Symbol getSymbol() {
			return symbol;
		}

		/**
		 * Push.
		 *
		 * @param sym the sym
		 * @return the parses the stack
		 */
		public ParseStack push(Symbol sym) {
			return new ParseStack(sym, this);
		}

		/**
		 * Pop.
		 *
		 * @return the parses the stack
		 */
		public ParseStack pop() {
			return next;
		}
		
	}
	
	private static class Change {
		
		int pos;
		int leftPos;
		int rightPos;
		int distance;
		List<Symbol> orig;
		List<Symbol> _new;
		
		/**
		 * Instantiates a new change.
		 *
		 * @param pos the pos
		 * @param leftPos the left pos
		 * @param rightPos the right pos
		 * @param distance the distance
		 * @param orig the orig
		 * @param _new the new
		 */
		public Change(int pos, int leftPos, int rightPos, int distance,
				List<Symbol> orig, List<Symbol> _new) {
			this.pos = pos;
			this.leftPos = leftPos;
			this.rightPos = rightPos;
			this.distance = distance;
			this.orig = orig;
			this._new = _new;
		}
		
		/**
		 * Checks if is deletion.
		 *
		 * @return true, if is deletion
		 */
		public boolean isDeletion() {
			return _new.isEmpty();
		}
		
		/**
		 * Checks if is insertion.
		 *
		 * @return true, if is insertion
		 */
		public boolean isInsertion() {
			return orig.isEmpty();
		}
		
		/**
		 * Checks if is substitution.
		 *
		 * @return true, if is substitution
		 */
		@SuppressWarnings("unused")
		public boolean isSubstitution() {
			return orig.size() > 0 && _new.size() > 0;
		}
		
		/**
		 * To string.
		 *
		 * @param parser the parser
		 * @return the string
		 */
		public String toString(CorrectingLRParser parser) {
			StringBuffer buffer = new StringBuffer();
			if (orig.size() > 0 && _new.isEmpty()) {
				buffer.append("deleting ");
				buffer.append(parser.printSymbols(orig));
			} else if (orig.isEmpty() && _new.size() > 0) {
				buffer.append("inserting ");
				buffer.append(parser.printSymbols(_new));
			} else {
				buffer.append("replacing ");
				buffer.append(parser.printSymbols(orig));
				buffer.append(" with ");
				buffer.append(parser.printSymbols(_new));
			}
			return buffer.toString();
		}
		
	}
	
	/**
	 * Try change.
	 *
	 * @param lex the lex
	 * @param stack the stack
	 * @param pos the pos
	 * @param leftPos the left pos
	 * @param rightPos the right pos
	 * @param orig the orig
	 * @param _new the new
	 * @param changes the changes
	 * @throws Exception the exception
	 */
	private void tryChange(LexState lex, ParseStack stack, int pos,
			int leftPos, int rightPos, List<Symbol> orig, List<Symbol> _new, List<Change> changes) throws Exception {
		
		 for (int i = _new.size() - 1; i >= 0; --i) {
			 lex = new LexState(_new.get(i), lex);
		 }
		 
		 int distance = tryParse(lex, stack, pos + _new.size() - orig.size());
		 
		 //System.out.println("CHANGE " + printSymbols(orig)
		 // + " TO " + printSymbols(_new) + " -> " + distance);
		 
		 if (distance >= (minAdvance + keywordsDelta(_new))) { 
			 changes.add(new Change(pos, leftPos, rightPos, distance, orig, _new));
		 }
	}
	
	/**
	 * Try parse.
	 *
	 * @param lexState the lex state
	 * @param stack the stack
	 * @param queuePos the queue pos
	 * @return the int
	 * @throws Exception the exception
	 */
	private int tryParse(LexState lexState, ParseStack stack, int queuePos) throws Exception {
		DistanceResult da = distanceParse(lexState, stack, new Fifo(), queuePos + maxAdvance);
		if (da.action == DistanceResult.ACCEPT)
			System.out.println("try is accepting " + da.distance);
		if (da.action == DistanceResult.ACCEPT && (maxAdvance - da.distance) >= 0)
			return maxAdvance;
		return maxAdvance - da.distance;
	}
	
	/**
	 * Keywords delta.
	 *
	 * @param _new the new
	 * @return the int
	 */
	private int keywordsDelta(List<Symbol> _new) {
		for (Symbol t : _new) {
			if (isKeyword(t.sym))
				return minDelta;
		}
		return 0;
	}
	
	/**
	 * Terms.
	 *
	 * @param state the state
	 * @return the int[]
	 */
	private int[] terms(int state) {
		short[] row = action_tab[state];
		int[] result = new int[row.length];
		int index = 0;
		for (int i = 0; i < row.length; i += 2) {
			if (row[i] < 0)
				return allTerms();
			if (row[i + 1] != 0) {
				result[index++] = row[i];
			}
		}
		int[] res = new int[index];
		System.arraycopy(result, 0, res, 0, index);
		return res; 
	}
	
	/**
	 * All terms.
	 *
	 * @return the int[]
	 */
	protected int[] allTerms() {
		return new int[0];
	}
	
	/**
	 * Checks if is keyword.
	 *
	 * @param t the t
	 * @return true, if is keyword
	 */
	protected boolean isKeyword(int t) {
		return false;
	}
	
	/**
	 * No shift.
	 *
	 * @param t the t
	 * @return true, if successful
	 */
	protected boolean noShift(int t) {
		return t == 0; // don't shift EOF
	}
	
	/**
	 * Error token value.
	 *
	 * @param t the t
	 * @return the object
	 */
	protected Object errorTokenValue(int t) {
		return null;
	}
	
	/**
	 * Error token name.
	 *
	 * @param t the t
	 * @return the string
	 */
	protected String errorTokenName(int t) {
		return null;
	}
	
	/**
	 * Normal parse.
	 *
	 * @param lexState the lex state
	 * @param stack the stack
	 * @param fifo the fifo
	 * @return the symbol
	 * @throws Exception the exception
	 */
	private Symbol normalParse(LexState lexState, ParseStack stack, Fifo fifo) throws Exception {
		if (stack.getSymbol().parse_state < 0)
			return stack.getSymbol(); // early accept
		_done_parsing = false;
		while (true) {
			Symbol cur_token = lexState.get();
			int act = get_action(stack.getSymbol().parse_state, cur_token.sym);

			if (act > 0) {
				int state = act - 1;
				cur_token = new Symbol(cur_token.sym, cur_token.left, cur_token.right, cur_token.value);
				cur_token.parse_state = state;
				cur_token.used_by_parser = true;
				stack = stack.push(cur_token);
				lexState = lexState.getNextState();
				fifo.putAndGet(stack, lexState); 
			}
			else if (act < 0) {
				int rule = (-act) - 1;

				int lhs_sym_num = production_tab[(-act) - 1][0];
				int handle_size = production_tab[(-act) - 1][1];

				Stack<Symbol> children = new Stack<Symbol>();
				for (int i = 0; i < handle_size; i++) {
					children.add(0, stack.symbol);
					stack = stack.pop();
				}
				children.add(0, stack.getSymbol()); // dummy symbol for the positions

				Symbol lhs_sym = do_action(rule, this, children, handle_size);
				
				int state = get_reduce(stack.getSymbol().parse_state, lhs_sym_num);
				if (state < 0) {
					System.out.println("bad goto state in reduce");
				}
				lhs_sym.parse_state = state;
				lhs_sym.used_by_parser = true;
				stack = stack.push(lhs_sym);
				if (_done_parsing)
					return lhs_sym;
			}
			else if (act == 0) {
				fixError(fifo);
				lexState = fifo.top().lexState;
				stack = fifo.top().stack;
			}
		}
	}
	
	private static class DistanceResult {
		
		public static final int DISTANCE = 0;
		public static final int ERROR = 1;
		public static final int ACCEPT = 2;
		
		LexState lexState;
		ParseStack stack;
		Fifo fifo;
		int action;
		int distance;
		
		/**
		 * Instantiates a new distance result.
		 *
		 * @param lexState the lex state
		 * @param stack the stack
		 * @param fifo the fifo
		 * @param distance the distance
		 * @param action the action
		 */
		public DistanceResult(LexState lexState, ParseStack stack, Fifo fifo, int distance, int action) {
			this.lexState = lexState;
			this.stack = stack;
			this.fifo = fifo;
			this.distance = distance;
			this.action = action;
		}
	}
	
	private static class FifoElement {
		
		LexState lexState;
		ParseStack stack;

		/**
		 * Instantiates a new fifo element.
		 *
		 * @param stack the stack
		 * @param lexState the lex state
		 */
		public FifoElement(ParseStack stack, LexState lexState) {
			this.stack = stack;
			this.lexState = lexState;
		}
		
	}
	
	private static class Fifo extends ArrayList<FifoElement> {
		private static final long serialVersionUID = 1L;

		/**
		 * Put.
		 *
		 * @param stack the stack
		 * @param lexState the lex state
		 */
		public void put(ParseStack stack, LexState lexState) {
			add(new FifoElement(stack, lexState));
		}

		/**
		 * Put and get.
		 *
		 * @param stack the stack
		 * @param lexState the lex state
		 */
		public void putAndGet(ParseStack stack, LexState lexState) {
			remove(0);
			put(stack, lexState);
		}

		/**
		 * Drop last.
		 *
		 * @param pos the pos
		 * @return the fifo element
		 */
		public FifoElement dropLast(int pos) {
			FifoElement e = null;
			while (pos-- >= 0) {
				e = remove(size()-1);
			}
			return e;
		}
		
		/**
		 * Top.
		 *
		 * @return the fifo element
		 */
		public FifoElement top() {
			return get(size()-1);
		}

	}
	
	/**
	 * Distance parse.
	 *
	 * @param lexState the lex state
	 * @param stack the stack
	 * @param fifo the fifo
	 * @param distance the distance
	 * @return the distance result
	 * @throws Exception the exception
	 */
	private DistanceResult distanceParse(LexState lexState, ParseStack stack, Fifo fifo, int distance) throws Exception {
		//System.out.println("=== Distance parsing ("+distance+") ===");
		_done_parsing = false;
		while (distance > 0) {
			Symbol cur_token = lexState.get();
			int act = get_action(stack.getSymbol().parse_state, cur_token.sym);

			if (act > 0) {
				int state = act - 1;
				//System.out.println("SHIFT " + cur_token + " to " + state);
				cur_token = new Symbol(cur_token.sym, cur_token.left, cur_token.right, cur_token.value);
				cur_token.parse_state = state;
				cur_token.used_by_parser = true;
				stack = stack.push(cur_token);
				lexState = lexState.getNextState();
				fifo.put(stack, lexState); 
				--distance;
			} else if (act < 0) {
				int rule = (-act) - 1;
				
				//System.out.println("REDUCE " + rule);

				int lhs_sym_num = production_tab[rule][0];
				int handle_size = production_tab[rule][1];

				Stack<Symbol> children = new Stack<Symbol>();
				for (int i = 0; i < handle_size; i++) {
					children.add(0, stack.symbol);
					stack = stack.pop();
				}
				children.add(0, stack.getSymbol()); // dummy symbol for the positions
				
				Symbol lhs_sym = do_action(rule, this, children, handle_size);

				int state = get_reduce(stack.getSymbol().parse_state, lhs_sym_num);
				if (state < 0) {
					System.out.println("bad goto state in reduce");
				}
				lhs_sym.parse_state = state;
				lhs_sym.used_by_parser = true;
				stack = stack.push(lhs_sym);
				if (_done_parsing)
					return new DistanceResult(lexState, stack, fifo, distance, DistanceResult.ACCEPT);
			} else if (act == 0) {
				return new DistanceResult(lexState, stack, fifo, distance, DistanceResult.ERROR);
			}
		}	
		return new DistanceResult(lexState, stack, fifo, 0, DistanceResult.DISTANCE);
	}
	
	/**
	 * Prints the symbols.
	 *
	 * @param symbols the symbols
	 * @return the string
	 */
	private String printSymbols(List<Symbol> symbols) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[ ");
		for (Symbol sym : symbols) {
			buffer.append(printSymbol(sym));
			buffer.append(" ");
		}
		buffer.append("]");
		return buffer.toString();
	}
	
	/**
	 * Prints the symbol.
	 *
	 * @param sym the sym
	 * @return the string
	 */
	private String printSymbol(Symbol sym) {
		if (sym.value != null)
			return sym.value.toString();
		Object t = errorTokenName(sym.sym);
		if (t != null)
			return t.toString();
		return sym.toString();
	}

	/**
	 *  Utility function: unpacks parse tables from strings.
	 *
	 * @param sa the sa
	 * @return the short[][]
	 */
	protected static short[][] unpackFromStrings(String[] sa) {
		// Concatenate initialization strings.
		StringBuffer sb = new StringBuffer(sa[0]);
		for (int i = 1; i < sa.length; i++)
			sb.append(sa[i]);
		int n = 0; // location in initialization string
		int size1 = (((int) sb.charAt(n)) << 16) | ((int) sb.charAt(n + 1));
		n += 2;
		short[][] result = new short[size1][];
		for (int i = 0; i < size1; i++) {
			int size2 = (((int) sb.charAt(n)) << 16) | ((int) sb.charAt(n + 1));
			n += 2;
			result[i] = new short[size2];
			for (int j = 0; j < size2; j++)
				result[i][j] = (short) (sb.charAt(n++) - 2);
		}
		return result;
	}
}
