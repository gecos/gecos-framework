


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public abstract class ScriptBinary extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode left;
	
	/**
	 * @generated
	 */
	protected ScriptNode right;
	
	/**
	 * @generated
	 */
	public ScriptBinary(Span span) {
		super(span);
	}
	
	public ScriptNode getLeft() {
		return left;
	}
	
	public ScriptNode getRight() {
		return right;
	}
	
}