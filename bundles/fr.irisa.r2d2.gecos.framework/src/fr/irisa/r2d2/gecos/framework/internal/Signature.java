package fr.irisa.r2d2.gecos.framework.internal;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class Signature {

	/**
	 * To string.
	 *
	 * @param type the type
	 * @return the string
	 */
	public static String toString(Type type) {
		StringBuffer buffer = new StringBuffer();
		toString(type, buffer);
		return buffer.toString();
	}

	/**
	 * To string.
	 *
	 * @param type the type
	 * @param buffer the buffer
	 */
	@SuppressWarnings("rawtypes")
	private static void toString(Type type, StringBuffer buffer) {
		if (type instanceof ParameterizedType) {
			ParameterizedType pType = (ParameterizedType) type;
			buffer.append("L");
			buffer.append(((Class)pType.getRawType()).getCanonicalName());
			buffer.append("<");
			for (Type t : pType.getActualTypeArguments()) {
				toString(t, buffer);
			}
			buffer.append(">;");
		} else if (type instanceof Class) {
			Class cls = (Class) type;
			if (cls == Integer.TYPE) {
				buffer.append("I");
			} else if (cls == Boolean.TYPE) {
				buffer.append("Z");
			} else if (cls == Short.TYPE) {
				buffer.append("S");
			} else if (cls == Long.TYPE) {
				buffer.append("J");
			} else if (cls == Float.TYPE) {
				buffer.append("F");
			} else if (cls == Double.TYPE) {
				buffer.append("D");
			} else if (cls == Byte.TYPE) {
				buffer.append("B");
			} else if (cls == Character.TYPE) {
				buffer.append("C");
			}else if (cls == Void.TYPE) {
				buffer.append("V");
			} else {
				buffer.append("L");
				buffer.append(cls.getCanonicalName());
				buffer.append(";");
			}
		}
	}

}
