/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.assist;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ContextInformation;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateProposal;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.texteditor.ITextEditor;

import fr.irisa.r2d2.gecos.cseditor.CSEditorPlugin;
import fr.irisa.r2d2.gecos.cseditor.CSImages;
import fr.irisa.r2d2.gecos.cseditor.text.CSScanner;
import fr.irisa.r2d2.gecos.cseditor.text.CSTextUtils;
import fr.irisa.r2d2.gecos.cseditor.text.IScriptCompilationUnit;
import fr.irisa.r2d2.gecos.cseditor.text.ScriptDocumentProvider;
import fr.irisa.r2d2.gecos.framework.IModule;
import fr.irisa.r2d2.gecos.framework.IModuleConstructor;

public class CSContentAssistProcessor implements IContentAssistProcessor {

	private CSContextType contextType;
    private IContextInformationValidator validator;
	private ITextEditor editor;

	public CSContentAssistProcessor(ITextEditor editor) {
		contextType = new CSContextType();
		this.editor = editor;
	}

	@Override
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer,
			int offset) {
		ScriptDocumentProvider provider = getScriptDocumentProvider();
		IScriptCompilationUnit desc = provider.getScriptCompilationUnit(editor.getEditorInput());
		desc.completeAt(provider.getDocument(editor.getEditorInput()).get(), offset);
		
		List<ICompletionProposal> retVal = new ArrayList<ICompletionProposal>();
		IDocument document = viewer.getDocument();
		String prefix = getPrefix(document, offset);
//		proposeKeywords(document, offset, prefix, retVal);
		proposeTemplates(document, offset, prefix, retVal);
		proposePrimitives(document, offset, prefix, retVal);
        proposeFunctions(document, offset, prefix, retVal);
		// XXX proposals should be sorted
		return retVal.toArray(new ICompletionProposal[retVal.size()]);
	}
	
	private ScriptDocumentProvider getScriptDocumentProvider() {
		return (ScriptDocumentProvider) editor.getDocumentProvider();
	}

	@Override
    public IContextInformation[] computeContextInformation(ITextViewer viewer, int offset) {
		return new IContextInformation[0];
	}
	
	@Override
	public char[] getCompletionProposalAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public char[] getContextInformationAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getErrorMessage() {
		return null;
	}

	public IContextInformationValidator getContextInformationValidator() {
        if (validator == null)
            validator = new CSParameterListValidator();
		return validator;
    }
	
	/**
	 * The the prefix part of an identifier. This part is just
	 * before the index position of the document.
	 * 
	 * @param document	the document to analyse
	 * @param offset	the offset to consider
	 * @return			the prefix of the identifier
	 */
	private String getPrefix(IDocument document, int offset) {
        try {
            int startPos = offset;
            while (startPos > 0) {
                char c = document.getChar(startPos - 1);
                if (! CSTextUtils.isIdentifierPart(c))
                    break;
                startPos--;
            }
            if (startPos < offset) {
                return document.get(startPos, offset - startPos).toLowerCase();
            }
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        return ""; //$NON-NLS-1$
    }
	
//	private void proposeKeywords(IDocument document, int offset,
//			String prefix, List<ICompletionProposal> proposals) {
//		for (int i = 0; i < GecosTokenNames.Keywords.length; ++i) {
//			String keyword = GecosTokenNames.Keywords[i];
//            if (keyword.startsWith(prefix)) {
//                ICompletionProposal proposal = new CompletionProposal(
//                		keyword, offset - prefix.length(), prefix.length(),
//                    keyword.length(), null, keyword, null, null);
//                proposals.add(proposal);
//            }
//        }
//	}
	
	private void proposePrimitives(IDocument document, int offset,
			String prefix, List<ICompletionProposal> proposals) {
		Image icon = CSImages.get(CSImages.IMG_PRIM_RULE);
		for (int i = 0; i < CSScanner.Primitives.length; ++i) {
			String keyword = CSScanner.Primitives[i];
            if (keyword.startsWith(prefix)) {
                ICompletionProposal proposal = new CompletionProposal(
                		keyword, offset - prefix.length(), prefix.length(),
                    keyword.length(), icon, keyword, null, null);
                proposals.add(proposal);
            }
        }
	}
	
	private void proposeTemplates(IDocument document, int offset, String prefix, List<ICompletionProposal> proposals) {
		TemplateContext context = contextType.createContext(document, offset, prefix);
		
		Template[] templates = CSEditorPlugin.getDefault().getTemplateStore().getTemplates();
		
		for (int i = 0; i < templates.length; ++i) {
			if (context.canEvaluate(templates[i])) {
				IRegion region= new Region(offset - prefix.length(), prefix.length());
				proposals.add(new TemplateProposal(templates[i], context, region,
						CSImages.get(CSImages.IMG_OBJS_TEMPLATE)));
			}
		}
	}
    
	/**
	 * @param document
	 * @param offset
	 * @param prefix
	 * @param proposals
	 */
    private void proposeFunctions(IDocument document, int offset, String prefix, final List<ICompletionProposal> proposals) {
    	String prefixLc = prefix.toLowerCase();
    	CSEditorPlugin.getDefault().getModuleIndexer().getModulesContaining(prefixLc).stream()
			.sorted((mod1,mod2) -> sortModules(mod1, mod2, prefixLc))
			.forEach(mod -> {
				/* add module proposal */
				ICompletionProposal modProposal = getModuleComplitionProposal(mod, offset, prefix);
				proposals.add(modProposal);
				
				/* add its constructors */
				mod.getConstructors().stream()
					.sorted(this::sortModuleConstructors)
					.map(cnst -> getModuleConstructorComplitionProposal(cnst, offset, prefix))
					.forEach(proposals::add);
			});
    }

	private ICompletionProposal getModuleComplitionProposal(IModule mod, int offset, String prefix) {
		Image icon = mod.isDeprecated()? CSImages.get(CSImages.IMG_OBJS_FUNC_DEPRECATED):
					 mod.isIterative() ? CSImages.get(CSImages.IMG_OBJS_FUNC_ITER): 
						 				 CSImages.get(CSImages.IMG_OBJS_FUNC);
		String rep = mod.getName();
		ICompletionProposal modProposal = new CompletionProposal(
				rep, offset-prefix.length(), prefix.length(),
				rep.length(), icon, rep+":", null,
				mod.getDescription());
		return modProposal;
	}
	
	private ICompletionProposal getModuleConstructorComplitionProposal(IModuleConstructor cnst, int offset, String prefix) {
		Image cnstIcon = !cnst.isDeprecated() ? null : CSImages.get(CSImages.IMG_OBJS_FUNC_CNST_DEPRECATED); 
			
		String name = cnst.getModule().getName();
		String typedArgs = cnst.getArgumentsString();
		
		String displayString = name + "(" + typedArgs + ")";
		IContextInformation context = new ContextInformation(null, typedArgs);
		String description = cnst.getFullDescription();
		
		String noTypedArgs = cnst.getArguments().stream()
			.map(a -> a.name())
			.collect(joining(", "));
		String cnstRep = name + "(" + noTypedArgs + ");";
		
		ICompletionProposal proposal = new CompletionProposal(
				cnstRep/*+"()"*/, offset-prefix.length(), prefix.length(),
				name.length()+1, cnstIcon, displayString, context,
				description);
		
		return proposal;
	}

	/**
     * Sort first the module whose name contains the specified @{code prefix}
     * closest to the beginning, otherwise compare by name lexicographical order.
     */
	private int sortModules(IModule m1, IModule m2, String prefix) {
		String m1NameLc = m1.getName().toLowerCase();
		String m2NameLc = m2.getName().toLowerCase();
		int m1Idx = m1NameLc.indexOf(prefix);
		int m2Idx = m2NameLc.indexOf(prefix);
		
		return 	m1Idx < m2Idx ? -1 :
				m1Idx > m2Idx ?  1 :
				m1NameLc.compareTo(m2NameLc);
	}

	/**
     * Sort first the constructor with maximum number of arguments
     */
	private int sortModuleConstructors(IModuleConstructor c1, IModuleConstructor c2) {
		return Integer.compare(c2.getArguments().size(), c1.getArguments().size());
	}
    
//	private String makeContextString(IModuleConstructor cnst) {
//		return cnst.getArguments().stream()
//			.map(arg -> makeDisplayString(arg))
//			.collect(Collectors.joining(","));
//	}
//
//	private String makeDisplayString(IModuleArgument argument) {
//		try {
//			String type = Signature.toString(argument.type());
//			if (argument.name().length() > 0) {
//				return Signature.getSimpleName(type) + " " + argument.name();
//			} else {
//				return Signature.getSimpleName(type);
//			}
//		} catch (IllegalArgumentException e) {
//			System.out.println(argument.type());
//		}
//		return "$error$";
//	}

}
