package fr.irisa.r2d2.gecos.framework.modules;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Mute all outputs till next call to 'UnMute'.\n"
		+ "That means that the functions called between \n"
		+ "'Mute' and 'UnMute' will not print anything.")
public class Mute {

	static class NullPrintStream extends PrintStream {
		
		/**
		 * Instantiates a new null print stream.
		 */
		public NullPrintStream() {
			super(new OutputStream() {
				@Override
				public void write(int b) throws IOException {
					return;
				}
			});
		}
	}
	
	private static PrintStream nullStream = new NullPrintStream();
	
	/**
	 * Instantiates a new mute.
	 */
	public Mute() {}
	
	public static PrintStream out = null;
	public static PrintStream err = null;
	
	/**
	 * Mute.
	 */
	public static void mute() {
		if (!(System.out instanceof NullPrintStream)) {
			out = System.out;
			err = System.err;
			System.setOut(nullStream);
			System.setErr(nullStream);
		}
	}
	
	/**
	 * Compute.
	 */
	public void compute() {
		mute();
	}
}
