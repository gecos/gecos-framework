/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.ant;

//import org.apache.tools.ant.Task;
//import org.apache.tools.ant.BuildException;

import java.util.ArrayList;
import java.util.List;

/**
 *  GecosTask is used to start gecos
 * XXX completement à revoir
 * @author llhours
 */

public class GecosTask {
	
//	private String inputFile = null;
	private List<Arg> args = new ArrayList<Arg>();
	
	/**
	 * Instantiates a new gecos task.
	 */
	public GecosTask() { }
	
	/*public void execute() throws BuildException {
		if (inputFile == null)
			throw new BuildException("the script option must be used");

		String[] nargs = new String[args.size()+2];
		nargs[0] = "-c";
		nargs[1] = inputFile;
		for (int i = 2; i < nargs.length; ++i)
			nargs[i] = ((Arg) args.get(i-2)).value;
		
		//int result = new Main().run(nargs);
		int result = 0;
		System.out.println("result: " + result);
	}*/
	
	/**
	 * Sets the script.
	 *
	 * @param file the new script
	 */
	public void setScript(String file) {
//		this.inputFile = file;
	}
	
	/**
	 * Creates the arg.
	 *
	 * @return the arg
	 */
	public Arg createArg() {
		Arg arg = new Arg();
		args.add(arg);
		return arg;
	}
	
	public class Arg {
		public String value;
		
		/**
		 * Sets the value.
		 *
		 * @param value the new value
		 */
		public void setValue(String value) { this.value = value; }
	}
	
}
