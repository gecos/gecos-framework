


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
//@SuppressWarnings("all")
public class ScriptCondition extends ScriptNode {

	/**
	 * @generated
	 */
	protected ScriptNode expr;
	
	/**
	 * @generated
	 */
	protected AstList<ScriptNode> thenStatements ;

	protected AstList<ScriptNode> elseStatements ;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptCondition(Span span, ScriptNode expr, AstList<ScriptNode> thenStatements, AstList<ScriptNode> elseStatements) {
		super(span);
		this.expr = expr;
		this.thenStatements = thenStatements;
		this.elseStatements = elseStatements;
	}

	public ScriptCondition(Span span, ScriptNode expr, AstList<ScriptNode> thenStatements) {
		this(span, expr, thenStatements, new AstList<ScriptNode>());
	}
	
	public AstList<ScriptNode> getThenStatements() {
		return thenStatements;
	}
	
	public AstList<ScriptNode> getElseStatements() {
		return elseStatements;
	}
	
	public ScriptNode getExpression() {
		return expr;
	}
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptCondition(this);
	}

}