/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.assist;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateException;
import org.eclipse.jface.text.templates.TemplateTranslator;

public class CSContext extends TemplateContext {

	private String prefix;

	public CSContext(CSContextType type, IDocument document,
			int offset, String prefix) {
		super(type);
		this.prefix = prefix;
	}

	public TemplateBuffer evaluate(Template template)
			throws BadLocationException, TemplateException {
		if (!canEvaluate(template))
			throw new TemplateException("Who lala"); 
		
		TemplateTranslator translator = new TemplateTranslator();
		TemplateBuffer buffer = translator.translate(template);
		getContextType().resolve(buffer, this);
		
		return buffer;
	}

	public boolean canEvaluate(Template template) {
		return template.matches(prefix, getContextType().getId())
//			&& prefix.length() != 0
			&& template.getName().toLowerCase().startsWith(prefix.toLowerCase());
	}

}
