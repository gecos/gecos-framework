


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public class ScriptChanging extends ScriptNode {

	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptChanging(Span span) {
		super(span);
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptChanging(this);
	}
}