<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/TR/xhtml1/strict" >
		
	<!-- This stylesheet is used to build the type cross-reference HTML file.
		It uses the typedb.xml database. -->
		
	<xsl:output method="xml" indent="yes"/>
			
	<xsl:key name="ref-by-type" match="ref" use="@type"/>
	<xsl:key name="trans-by-mod" match="transform" use="@name"/>
	
	<!-- XXX should be an XSL 2.0 function, but not so simple... -->
	<xsl:template name="makeRef">
		<xsl:param name="name"/><xsl:value-of
				select="translate(key('trans-by-mod', $name)[1]/@module,' ','_')" />/<xsl:value-of select="$name" />.html</xsl:template>

	<xsl:template match="types">
		<html>
			<head>
				<title>Types Cross Reference</title>
				<link rel="stylesheet" type="text/css" media="screen" href="gecos-api.css"/>
			</head>
			<body>
				<div id="crossref">
				<h1>Type Cross Reference</h1>
				
				<xsl:for-each select="ref[count(. | key('ref-by-type', @type)[1]) = 1]">
					<div class="xref">
					<h2><a name="{@type}"><xsl:value-of select="@type"/></a></h2>
					<xsl:variable name="elements" select="key('ref-by-type', @type)"/>
					<xsl:variable name="file">
						<xsl:call-template name="makeRef">
							<xsl:with-param name="name" select="@name"/>
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:if test="count($elements[not (@use='true')]) > 0">
						<p class="def">Definitions:
						<ul>
						<xsl:for-each select="$elements[not (@use = 'true')]">
							<li><a href="{$file}"><xsl:value-of select="@name"/></a></li>
						</xsl:for-each>
						</ul></p>
					</xsl:if>
					
					<xsl:if test="count($elements[@use='true']) > 0">
						<p class="use">Usages:
						<ul>
						<xsl:for-each select="$elements[@use='true']">
							<li><a href="{$file}"><xsl:value-of select="@name"/></a></li>
						</xsl:for-each>
						</ul></p>
					</xsl:if>
					</div>
				</xsl:for-each>
				</div>
			</body>
		</html>
	</xsl:template>
	
</xsl:stylesheet>
