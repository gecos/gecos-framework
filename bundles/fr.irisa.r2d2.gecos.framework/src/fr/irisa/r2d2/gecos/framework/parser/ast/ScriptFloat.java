package fr.irisa.r2d2.gecos.framework.parser.ast;

import fr.irisa.r2d2.parser.runtime.Span;

public class ScriptFloat extends ScriptNode {

	private double value;

	public ScriptFloat(Span span, double value) {
		super(span);
		this.value = value;
	}
	
	public Double getValue() {
		return value;
	}

	@Override
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptFloat(this);
	}

}
