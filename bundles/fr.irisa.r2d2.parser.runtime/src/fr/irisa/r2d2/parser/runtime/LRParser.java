package fr.irisa.r2d2.parser.runtime;

import java.util.Stack;

public class LRParser extends BaseParser {
	
	private static class Symbol {
	    
	    public int parseState;
		public Object object;

	    public Symbol(int state, Object obj) {
	    	this.parseState = state;
	    	this.object = obj;
	    }
	    
	    public Symbol(int state) {
			this(state, null);
	    }
	    
	    public String toString() {
	    	return "Symbol(" + parseState + ", " + object +")";
	    }

	}
    
    protected Stack<Symbol> stack = new Stack<Symbol>();
    protected IToken curToken;
    private boolean doneParsing;
	private int startState;
	private int acceptingRule;
	protected ILexer lexer;
	private IRuleReducer ruleReducer;
    
    public LRParser(IParseTables tables,
    		ILexer lexer, IRuleReducer ruleReducer) {
    	super(tables);
    	this.lexer = lexer;
        this.startState = tables.startState();
        this.acceptingRule = tables.acceptingProduction();
        this.ruleReducer = ruleReducer;
    }
    
    protected void doneParsing() {
    	doneParsing = true;
    }
    
    public Object parse() throws java.lang.Exception {

        stack.clear();
        stack.push(new Symbol(startState));
        curToken = lexer.scan();
        
        doneParsing = false;
        
        while(!doneParsing) {
            short act = getAction(((Symbol) stack.peek()).parseState, curToken.type());

            if (isShift(act)) {
                stack.push(new Symbol(toShift(act), curToken));
                curToken = lexer.scan();
            } else if (isReduce(act)) {
            	act = toReduce(act);
                short lhs_sym_num = productionTab[act][0];
                short handle_size = productionTab[act][1];

                Object[] values = new Object[handle_size];
                for (int i = 0; i < handle_size; i++) {
                	values[handle_size - i - 1] = stack.pop().object;
                }
                if (act == acceptingRule) {
        			doneParsing();
        			return values[0];
        		} else {
        			Object semValue = ruleReducer.reduce(act, values);
        			act = getReduce(((Symbol) stack.peek()).parseState, lhs_sym_num);
        			stack.push(new Symbol(act, semValue));
        		}
            } else if (act == 0) {
            	// finally if the entry is zero, we have an error
                // call user syntax error reporting routine
                // try to error recover */
                if (!errorRecovery()) {
                    // if that fails give up with a fatal syntax error
                	syntaxError(curToken);
                }
            }
        }
        return null;
    }

	private boolean errorRecovery() {
		panicMode();
		return true;
	}
		
	@SuppressWarnings("unchecked")
	private void panicMode() {
		Stack<Symbol> saveStack = stack;
		stack = (Stack<Symbol>) stack.clone();
		int act;
		if (stack.size() > 1) {
			do {
				stack.pop();
				act = getAction(((Symbol) stack.peek()).parseState, curToken.type());
			} while (act == 0 && stack.size() > 1);
		}
		if (stack.size() == 1) {
			System.err.println("discard " + curToken);
			stack = saveStack;
			curToken = lexer.scan();
		} else {
			for (int i = saveStack.size()-1; i >= stack.size(); --i)
			System.err.println("discard " + ((Symbol)saveStack.get(i)).object);
		}
	}
	
	private void syntaxError(IToken token) {
        System.out.println("Error on: " + token + " -> " + token.type());
        System.out.println("state: " + ((Symbol)stack.peek()).parseState);
        for (int i = stack.size()-1 ; i >= 0; --i) {
        	System.out.println(((Symbol)stack.elementAt(i)).object);
        }
		throw new RuntimeException("Parse Error");
    }

}
