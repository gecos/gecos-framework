/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.irisa.r2d2.parser.runtime.Span;

public class ArrayNode extends Node {
	private Node[] elements;
	
	/**
	 * Instantiates a new array node.
	 *
	 * @param span the span
	 * @param elements the elements
	 */
	public ArrayNode(Span span, Node[] elements) {
		super(span);
		this.elements = elements;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#getElements()
	 */
	public Iterable<Node> getElements() {
		return new Iterable<Node>() {
			public Iterator<Node> iterator() {
				List<Node> nodes = new ArrayList<Node>();
				Collections.addAll(nodes, elements);
				return nodes.iterator();
			}
		};
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject(java.lang.Class)
	 */
	public Object asObject(Class<?> cls) {
		if (Map.class.isAssignableFrom(cls)) {
			return asMap();
		}
		return asObject();
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asObject()
	 */
	public Object asObject() {
		if (isMappingArray()) {
			return asMap();
		}
		List<Object> v = new ArrayList<Object>();
		for (int i = 0; i < elements.length; ++i) {
			v.add(elements[i].asObject());
		}
		return v;
	}
	
	/**
	 * As map.
	 *
	 * @return the map
	 */
	public Map<Object, Object> asMap() {
		Map<Object, Object> v = new HashMap<Object, Object>();
		for (int i = 0; i < elements.length; ++i) {
			Node node = elements[i];
			if (node instanceof MappingNode) {
				MappingNode mapping = (MappingNode) node;
				v.put(mapping.key().asObject(), mapping.value().asObject());
			} else
				v.put(String.valueOf(i), node.asObject());
		}
		return v;
	}

	/**
	 * Checks if is mapping array.
	 *
	 * @return true, if is mapping array
	 */
	private boolean isMappingArray() {
		for (Node node : elements) {
			if (node instanceof MappingNode)
				return true;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.script.nodes.Node#asString()
	 */
	public StringNode asString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append('{');
		for (int i = 0; i < elements.length; ++i) {
			if (i != 0) buffer.append(", ");
			buffer.append(elements[i].asString().value());
		}
		buffer.append('}');
		return new StringNode(span, buffer.toString());
	}
	
}