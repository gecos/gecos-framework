


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstNode;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
public abstract class ScriptNode extends AstNode {

	/**
	 * @generated
	 */
	public ScriptNode(Span span) {
		super(span);
	}
	
	public abstract void visit(IGecosVisitor visitor);
	
}