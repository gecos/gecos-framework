/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.assist;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.TextPresentation;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationPresenter;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;

public class CSParameterListValidator implements IContextInformationValidator, IContextInformationPresenter {

    private IContextInformation contextInfo;
    private ITextViewer viewer;
    private int startPosition;
    private int currentParameter;

    public void install(IContextInformation info, ITextViewer viewer, int offset) {
        this.contextInfo = info;
        this.viewer = viewer;
        this.startPosition = offset;
        this.currentParameter = -1;
    }
    
    /**
     * Look in the document for the end delimiter, skiping
     * escaped characters (ie. "\a").
     * 
     * @param document	the document to analyse
     * @param start		the start index
     * @param end		the end position of the docuemnt
     * @param ch		the string delimiter
     * @return			the end delimiter position
     * @throws BadLocationException
     */
    private int getStringEnd(IDocument document, int start, int end, char ch) throws BadLocationException {
        while (start < end) {
            char curr = document.getChar(start++);
            if (curr == '\\') {
                // ignore escaped characters
                start++;
            } else if (curr == ch) {
                return start;
            }
        }
        return end;
    }
    
    /**
     * Look in the document between start and end, for specials charaters.
     * 
     * @param document			the document to analyse
     * @param start				the start index of the search
     * @param end				the end index of the search
     * @param increments		nest incrementing characters
     * @param decrements		nest decrementing characters
     * @param considerNesting
     * @return
     * @throws BadLocationException
     */
    private int getCharCount(IDocument document, int start, int end, String increments, String decrements, boolean considerNesting) throws BadLocationException {

        assert((increments.length() != 0 || decrements.length() != 0) && !increments.equals(decrements));

        int nestingLevel= 0;
        int charCount= 0;
        while (start < end) {
            char curr = document.getChar(start++);
            switch (curr) {
                case '#':
                  	// skip comments
                    start= end;
                    break;
           
                case '"':
                	// skip strings
                    start = getStringEnd(document, start, end, curr);
                    break;
                    
                default:
                    if (considerNesting) {
                        if ('(' == curr)
                            ++ nestingLevel;
                        else if (')' == curr)
                            -- nestingLevel;

                        if (nestingLevel != 0)
                            break;
                    }

                    if (increments.indexOf(curr) >= 0) {
                        ++ charCount;
                    }

                    if (decrements.indexOf(curr) >= 0) {
                        -- charCount;
                    }
            }
        }
        return charCount;
    }

    public boolean isContextInformationValid(int offset) {
        try {
            if (offset < startPosition)
                return false;

            IDocument document = viewer.getDocument();
            IRegion line = document.getLineInformationOfOffset(offset);

            if (offset < line.getOffset() || offset >= document.getLength())
                return false;
            
            return getCharCount(document, startPosition, offset, "(", ")", false) >= 0;  //$NON-NLS-1$//$NON-NLS-2$;
        } catch (BadLocationException e) {
            return false;
        }
    }

    public boolean updatePresentation(int offset, TextPresentation presentation) {
        int current = -1;

        try {
            current = getCharCount(viewer.getDocument(), startPosition, offset, ",", "", true);  //$NON-NLS-1$//$NON-NLS-2$
        } catch (BadLocationException x) {
            return false;
        }

        if (currentParameter != -1) {
            if (current == currentParameter)
                return false;
        }

        presentation.clear();
        currentParameter = current;

        String s = contextInfo.getInformationDisplayString();
        int start = 0;
        int occurrences = 0;
        while (occurrences < currentParameter) {
            int found = s.indexOf(',', start);
            if (found == -1)
                break;
            start = found + 1;
            ++occurrences;
        }

        if (occurrences < currentParameter) {
            presentation.addStyleRange(new StyleRange(0, s.length(), null, null, SWT.NORMAL));
            return true;
        }

        if (start == -1)
            start = 0;

        int end = s.indexOf(',', start);
        if (end == -1)
            end = s.length();

        if (start > 0)
            presentation.addStyleRange(new StyleRange(0, start, null, null, SWT.NORMAL));

        if (end > start)
            presentation.addStyleRange(new StyleRange(start, end - start, null, null, SWT.BOLD));

        if (end < s.length())
            presentation.addStyleRange(new StyleRange(end, s.length() - end, null, null, SWT.NORMAL));

        return true;
    }

}
