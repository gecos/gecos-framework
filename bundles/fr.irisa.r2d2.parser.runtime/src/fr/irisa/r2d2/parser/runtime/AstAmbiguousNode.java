package fr.irisa.r2d2.parser.runtime;

public class AstAmbiguousNode extends AstNode {

	private AstAmbiguousNode alternative;
	
	public AstAmbiguousNode(Span span) {
		super(span);
	}
	
	public void setAlternative(AstAmbiguousNode other) {
		other.alternative = alternative;
		alternative = other;
	}
	
	public boolean isAmbiguous() {
		return alternative != null;
	}

}
