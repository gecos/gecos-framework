/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;

public class CSTextUtils {

	public static boolean isIdentifierPart(char c) {
		return Character.isLetterOrDigit(c) || c == '_' || c == '.';
	}

	public static boolean isWhitespace(char c) {
		return Character.isWhitespace(c);
	}
    
    public static IRegion findWord(IDocument document, int offset) {

        int start= -1;
        int end= -1;

        try {

            int pos= offset;
            char c;

            while (pos >= 0) {
                c= document.getChar(pos);
                if (! isIdentifierPart(c))
                    break;
                --pos;
            }

            start= pos;

            pos= offset;
            int length= document.getLength();

            while (pos < length) {
                c= document.getChar(pos);
                if (! isIdentifierPart(c))
                    break;
                ++pos;
            }

            end= pos;

        } catch (BadLocationException x) {
        	return null;
        }
        if (start == offset && end == offset)
        	return new Region(offset, 0);
        else if (start == offset)
        	return new Region(start, end - start);
        return new Region(start + 1, end - start - 1);
    }

}
