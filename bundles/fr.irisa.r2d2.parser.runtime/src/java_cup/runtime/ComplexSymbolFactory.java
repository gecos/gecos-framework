package java_cup.runtime;

/**
 * Default Implementation for SymbolFactory, creates
 * plain old Symbols
 *
 * @version last updated 27-03-2006
 * @author Michael Petter
 */

/* *************************************************
  class DefaultSymbolFactory

  interface for creating new symbols  
 ***************************************************/
public class ComplexSymbolFactory implements SymbolFactory{
    public static class Location {
        private String unit="unknown";
        private int line, column;
        
        /**
         * Instantiates a new location.
         *
         * @param unit the unit
         * @param line the line
         * @param column the column
         */
        public Location(String unit, int line, int column){
            this.unit=unit;
            this.line=line;
            this.column=column;
        }
        
        /**
         * Instantiates a new location.
         *
         * @param line the line
         * @param column the column
         */
        public Location(int line, int column){
            this.line=line;
            this.column=column;
        }
        
        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        public String toString(){
            return unit+":"+line+"/"+column;
        }
        
        /**
         * Gets the column.
         *
         * @return the column
         */
        public int getColumn(){
            return column;
        }
        
        /**
         * Gets the line.
         *
         * @return the line
         */
        public int getLine(){
            return line;
        }
        
        /**
         * Gets the unit.
         *
         * @return the unit
         */
        public String getUnit(){
            return unit;
        }
    }
    /**
     * ComplexSymbol with detailed Location Informations and a Name
     */
    public static class ComplexSymbol extends Symbol {
        protected String name;
        protected Location xleft,xright;
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         */
        public ComplexSymbol(String name, int id) {
            super(id);
            this.name=name;
        }
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         * @param value the value
         */
        public ComplexSymbol(String name, int id, Object value) {
            super(id,value);
            this.name=name;
        }
        
        /* (non-Javadoc)
         * @see java_cup.runtime.Symbol#toString()
         */
        public String toString(){
            if (xleft==null || xright==null) return "Symbol: "+name;
            return "Symbol: "+name+" ("+xleft+" - "+xright+")";
        }
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         * @param state the state
         */
        public ComplexSymbol(String name, int id, int state) {
            super(id,state);
            this.name=name;
        }
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         * @param left the left
         * @param right the right
         */
        public ComplexSymbol(String name, int id, Symbol left, Symbol right) {
            super(id,left,right);
            this.name=name;
            if (left!=null)  this.xleft = ((ComplexSymbol)left).xleft;
            if (right!=null) this.xright= ((ComplexSymbol)right).xright;
        }
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         * @param left the left
         * @param right the right
         */
        public ComplexSymbol(String name, int id, Location left, Location right) {
            super(id);
            this.name=name;
            this.xleft=left;
            this.xright=right;
        }
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         * @param left the left
         * @param right the right
         * @param value the value
         */
        public ComplexSymbol(String name, int id, Symbol left, Symbol right, Object value) {
            super(id,value);
            this.name=name;
            if (left!=null)  this.xleft = ((ComplexSymbol)left).xleft;
            if (right!=null) this.xright= ((ComplexSymbol)right).xright;
        }
        
        /**
         * Instantiates a new complex symbol.
         *
         * @param name the name
         * @param id the id
         * @param left the left
         * @param right the right
         * @param value the value
         */
        public ComplexSymbol(String name, int id, Location left, Location right, Object value) {
            super(id,value);
            this.name=name;
            this.xleft=left;
            this.xright=right;
        }
        
        /**
         * Gets the left.
         *
         * @return the left
         */
        public Location getLeft(){
            return xleft;
        }
        
        /**
         * Gets the right.
         *
         * @return the right
         */
        public Location getRight(){
            return xright;
        }
    }


    /**
     * New symbol.
     *
     * @param name the name
     * @param id the id
     * @param left the left
     * @param right the right
     * @param value the value
     * @return the symbol
     */
    // Factory methods
    public Symbol newSymbol(String name, int id, Location left, Location right, Object value){
        return new ComplexSymbol(name,id,left,right,value);
    }
    
    /**
     * New symbol.
     *
     * @param name the name
     * @param id the id
     * @param left the left
     * @param right the right
     * @return the symbol
     */
    public Symbol newSymbol(String name, int id, Location left, Location right){
        return new ComplexSymbol(name,id,left,right);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int, java_cup.runtime.Symbol, java_cup.runtime.Symbol, java.lang.Object)
     */
    public Symbol newSymbol(String name, int id, Symbol left, Symbol right, Object value){
        return new ComplexSymbol(name,id,left,right,value);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int, java_cup.runtime.Symbol, java_cup.runtime.Symbol)
     */
    public Symbol newSymbol(String name, int id, Symbol left, Symbol right){
        return new ComplexSymbol(name,id,left,right);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int)
     */
    public Symbol newSymbol(String name, int id){
        return new ComplexSymbol(name,id);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#newSymbol(java.lang.String, int, java.lang.Object)
     */
    public Symbol newSymbol(String name, int id, Object value){
        return new ComplexSymbol(name,id,value);
    }
    
    /* (non-Javadoc)
     * @see java_cup.runtime.SymbolFactory#startSymbol(java.lang.String, int, int)
     */
    public Symbol startSymbol(String name, int id, int state){
        return new ComplexSymbol(name,id,state);
    }
}
