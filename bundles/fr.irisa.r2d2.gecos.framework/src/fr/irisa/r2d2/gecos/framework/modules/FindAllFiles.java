package fr.irisa.r2d2.gecos.framework.modules;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Find all files in the directory path passed as argument.\n "
		+ "The file extension can be use as a matching pattern "
		+ "(wildcard are not supported i.e. use '.c' not '*.c')")
public class FindAllFiles  {

	File root;
	String filter;
	GecosStringList list;
	
	public class GecosStringList extends ArrayList<String>{
		private static final long serialVersionUID = 1L;

		/**
		 * Instantiates a new gecos string list.
		 */
		public GecosStringList() {
		}
		
		/**
		 * Elements.
		 *
		 * @return the list
		 */
		public List<String> elements() {
			return this;
		}
	}

	/**
	 * Instantiates a new find all files.
	 *
	 * @param path the path
	 * @param filter the filter
	 */
	public FindAllFiles(String path, String filter) {
		this.root= new File(path);
		list = new GecosStringList();
		this.filter=filter;
	}

	/**
	 * Compute.
	 *
	 * @return the gecos string list
	 */
	public GecosStringList compute() {
		visitAllFiles(root);
		return list;
	}

	/**
	 * Process.
	 *
	 * @param dir the dir
	 */
	private void process(File dir) {
		if (dir.getName().endsWith(filter)) {
			list.add(dir.getAbsolutePath());
		}
	}

	/**
	 * Visit all files.
	 *
	 * @param dir the dir
	 */
	// Process only files under dir
	private void visitAllFiles(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				visitAllFiles(new File(dir, children[i]));
			}
		} else {
			process(dir);
		}
	}

}

