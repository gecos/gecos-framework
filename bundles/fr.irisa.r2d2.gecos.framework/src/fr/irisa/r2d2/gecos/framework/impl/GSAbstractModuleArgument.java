package fr.irisa.r2d2.gecos.framework.impl;

import fr.irisa.r2d2.gecos.framework.IModuleArgument;

public class GSAbstractModuleArgument implements IModuleArgument {

	private String type;
    private String typeSignature;
    private String name;
	private String description;

    /**
     * Instantiates a new GS abstract module argument.
     *
     * @param typeSignature the type signature
     * @param typeName the type name
     * @param argName the arg name
     */
    public GSAbstractModuleArgument(String typeSignature, String typeName, String argName) {
        this.typeSignature = typeSignature;
        this.name = argName;
        this.type = typeName;
    }

    /* (non-Javadoc)
     * @see fr.irisa.r2d2.gecos.framework.IModuleArgument#name()
     */
    @Override
	public String name() {
		return name;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleArgument#typeSignature()
	 */
	@Override
	public String typeSignature() {
		return typeSignature;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleArgument#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.IModuleArgument#setDescription(java.lang.String)
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return type + " " + name;
	}

}
