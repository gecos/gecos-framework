package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public class NodeFactory {

	/**
	 * Builds the.
	 *
	 * @param span the span
	 * @param object the object
	 * @return the node
	 */
	public static Node build(Span span, Object object) {
		
		if (object == null)	return new NullNode(span);
		
		if (object instanceof Boolean) 		return booleanNode(span,(Boolean)object);
		
		if (object instanceof Integer)		return integerNode(span,(Integer)object);
		if (object instanceof Character) 	return integerNode(span,(Character)object);
		if (object instanceof Long) 		return integerNode(span,(Long)object);
		if (object instanceof Short) 		return integerNode(span,(Short)object);

		if (object instanceof Float) 		return floatNode(span,(Float)object);
		if (object instanceof Double) 		return floatNode(span,(Double)object);

		if (object instanceof String)		return stringNode(span,(String)object);
		
		//default is object node
		return new ObjectNode(span, object);
	}
	
	/**
	 * Boolean node.
	 *
	 * @param span the span
	 * @param object the object
	 * @return the node
	 */
	static Node booleanNode(Span span, Boolean object) {
		return new BoolNode(span, object.booleanValue());
	}
	
	/**
	 * String node.
	 *
	 * @param span the span
	 * @param object the object
	 * @return the node
	 */
	static Node stringNode(Span span, String object) {
		return new StringNode(span, object);
	}

	/**
	 * Float node.
	 *
	 * @param span the span
	 * @param f the f
	 * @return the node
	 */
	static Node floatNode(Span span, Float f) {
		return new FloatNode(span, (double) f.floatValue());
	}
	
	/**
	 * Float node.
	 *
	 * @param span the span
	 * @param f the f
	 * @return the node
	 */
	static Node floatNode(Span span, Double f) {
		return new FloatNode(span, f.doubleValue());
	}

	/**
	 * Integer node.
	 *
	 * @param span the span
	 * @param i the i
	 * @return the node
	 */
	static Node integerNode(Span span, Integer i) {
		return new IntegerNode(span, i.intValue());
	}

	/**
	 * Integer node.
	 *
	 * @param span the span
	 * @param i the i
	 * @return the node
	 */
	static Node integerNode(Span span, Character i) {
		return new IntegerNode(span, (int) i.charValue());
	}

	/**
	 * Integer node.
	 *
	 * @param span the span
	 * @param i the i
	 * @return the node
	 */
	static Node integerNode(Span span, Long i) {
		return new IntegerNode(span, (int) i.longValue());
	}

	/**
	 * Integer node.
	 *
	 * @param span the span
	 * @param i the i
	 * @return the node
	 */
	static Node integerNode(Span span, Short i) {
		return new IntegerNode(span, (int) i.shortValue());
	}
}
