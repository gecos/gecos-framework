/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.compat;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class TransformsReader extends DefaultHandler {

	public static final String MODULE_URI
			= "http://www.jcomp.org/ns/module";

	private FuncTransform current;
	private ClassLoader loader;
	private List<FuncTransform> functions;
	private boolean isDescription;

	/**
	 * Instantiates a new transforms reader.
	 *
	 * @param loader the loader
	 */
	public TransformsReader(ClassLoader loader) {
		this.loader = loader;
		this.functions = new ArrayList<FuncTransform>();
		this.isDescription = false;
	}

	/**
	 * Read.
	 *
	 * @param reader the reader
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<FuncTransform> read(Reader reader) throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		SAXParser parser = factory.newSAXParser();
		try {
			parser.parse(new InputSource(reader), this);
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
		return functions;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(String uri,
        String localName,
        String qName,
        Attributes attributes)
            throws SAXException {
		if (! uri.equals(MODULE_URI))
			throw new SAXException("bad namespace URI");
		if (localName.equals("transform")) {
			boolean iterative = false;
			String str = attributes.getValue("iterative");
			if (str != null) iterative = (Boolean.valueOf(str)).booleanValue();
			current = new FuncTransform(attributes.getValue("name"),
					loader, iterative);
		} else if (localName.equals("class")) {
			current.setClass(attributes.getValue("name"));
		} else if (localName.equals("in") || qName.equals("inout")) {
			current.addArg(attributes.getValue("type"));
		} else if (localName.equals("out")) {
			current.setResult(attributes.getValue("type"));
		} else if (localName.equals("description")) {
			isDescription = true;
		}
	}
	
	 /* (non-Javadoc)
 	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
 	 */
 	public void characters (char ch[], int start, int length) throws SAXException {
		 if (isDescription) {
			 current.setDescription(new String(ch, start, length));
		 }
	 }

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(String uri,
                       String localName,
                       String qName)
                throws SAXException {
		if (qName.equals("transform")) {
			functions.add(current);
			current = null;
		} else if (localName.equals("description")) {
			isDescription = false;
		}
	}
}
