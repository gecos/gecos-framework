package fr.irisa.r2d2.parser.runtime;

public class Span {
	
	private int from;
	private int to;

	public Span(int from, int to) {
		this.from = from;
		this.to = to;
	}

	public Span(Span span) {
		this(span.from, span.to);
	}

	public int from() {
		return from;
	}

	public int to() {
		return to;
	}

	public int length() {
		return to - from;
	}
	
	public String toString() {
		return "["+from+".."+to+"]";
	}
}
