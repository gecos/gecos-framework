package fr.irisa.r2d2.parser.runtime;

public interface IRuleReducer {

	Object reduce(int r, Object[] values);

}
