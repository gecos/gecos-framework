/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.script;

import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.parser.runtime.Span;

public interface IScriptEvaluable {

	/**
	 * Apply.
	 *
	 * @param evaluator the evaluator
	 * @param span the span
	 * @param args the args
	 * @return the node
	 * @throws ScriptException the script exception
	 */
	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException;

}
