package $packageName$;

/**
 * $className$ is a sample primitive. When the parser encounters
 * the '$primitiveName$' function, it executes the apply method.
 */

import fr.irisa.r2d2.gecos.framework.script.nodes.ScriptList;
import fr.irisa.r2d2.gecos.framework.script.nodes.ScriptNode;
import fr.irisa.r2d2.gecos.framework.script.nodes.ScriptNull;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;

public class $className$ implements IScriptEvaluable {

	public ScriptNode apply(IScriptEvaluator evaluator, ScriptList args)
			throws ScriptException {
		System.out.println("I'm a the $primitiveName$ primitive called with args:");
		for (int i = 0; i < args.size(); ++i) {
			System.out.println("   " + args.elementAt(i));
		}
		return new ScriptNull();
	}
}