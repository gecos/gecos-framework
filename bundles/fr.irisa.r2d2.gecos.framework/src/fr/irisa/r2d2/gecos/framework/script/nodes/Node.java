/**
 * 
 */
package fr.irisa.r2d2.gecos.framework.script.nodes;

import fr.irisa.r2d2.parser.runtime.Span;

public abstract class Node {

	protected Span span;

	/**
	 * Instantiates a new node.
	 *
	 * @param span the span
	 */
	public Node(Span span) {
		this.span = span;
	}

	/**
	 * Apply plus.
	 *
	 * @param r the r
	 * @return the node
	 */
	public Node applyPlus(Node r) {
		return asString().applyPlus(r);
	}

	/**
	 * As string.
	 *
	 * @return the string node
	 */
	public StringNode asString() {
		return new StringNode(span, toString());
	}

	/**
	 * Apply minus.
	 *
	 * @param r the r
	 * @return the node
	 */
	public Node applyMinus(Node r) {
		throw new RuntimeException(span + "cannot substract with " + r);
	}

	/**
	 * As boolean.
	 *
	 * @return true, if successful
	 */
	public boolean asBoolean() {
		return false;
	}
	
	/**
	 * As integer.
	 *
	 * @return the integer
	 */
	public Integer asInteger() {
		throw new RuntimeException(span.toString() + " " + this.getClass().getSimpleName() + " considered as integer");
	}
	
	/**
	 * As float.
	 *
	 * @return the double
	 */
	public Double asFloat() {
		throw new RuntimeException(span.toString() + " " + this.getClass().getSimpleName() + " considered as float");
	}
	
	/**
	 * Gets the elements.
	 *
	 * @return the elements
	 */
	public Iterable<Node> getElements() {
		throw new RuntimeException("element at " + span + " could not be enumerated");
	}
	
	/**
	 * As object.
	 *
	 * @return the object
	 */
	public Object asObject() {
		return null;
	}
	
	/**
	 * As object.
	 *
	 * @param cls the cls
	 * @return the object
	 */
	public Object asObject(Class<?> cls) {
		return asObject();
	}
	
}