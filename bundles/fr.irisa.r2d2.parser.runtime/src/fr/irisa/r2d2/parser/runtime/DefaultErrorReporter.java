package fr.irisa.r2d2.parser.runtime;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DefaultErrorReporter implements IErrorReporter {

	private static class Error {
		int start;
		int length;
		String msg;
		public Error(int start, int length, String msg) {
			this.start = start;
			this.length = length;
			this.msg = msg;
		}
	}

	private List<Error> errors = new ArrayList<Error>();
	private List<Error> warnings = new ArrayList<Error>();
	private PrintStream out;
	
	public DefaultErrorReporter(PrintStream out) {
		this.out = out;
	}

	public void reportError(int start, int length, String msg) {
		errors.add(new Error(start, length, msg));
	}
	
	public void reportWarning(int start, int length, String msg) {
		warnings.add(new Error(start, length, msg));
	}

	public void printDiagnostic(String filename) throws IOException {
		printDiagnostic(new FileReader(filename), filename);
	}
	
	public void printDiagnostic(Reader stream, String filename) throws IOException {
		int lineNumber = 1;
		int lineStart = 0;
		StringBuffer line = new StringBuffer();
		while (stream.ready() && (errors.size()+warnings.size()) > 0) {
			int c = stream.read();
			if (c == -1)
				break;
			if (c == '\n') {
				printError(errors,filename,lineNumber, lineStart, line,false);
				printError(warnings,filename,lineNumber, lineStart, line,true);
				lineStart += line.length() + 1;
				++lineNumber;
				line.setLength(0);
			} else
				line.append((char)c);
		}
		if (errors.size() > 0)
			printError(errors,filename,lineNumber, lineStart, line, false);
		if (warnings.size() > 0)
			printError(warnings,filename,lineNumber, lineStart, line, true);
	}

	
	private void printError(List<Error> list,String filename, int lineNumber, int lineStart, StringBuffer line, boolean isWraning) {
		Iterator<Error> e = list.iterator();
		while (e.hasNext()) {
			Error error = e.next();
			if (error.start >= lineStart && error.start <= (lineStart + line.length())) {
				String string = "[" +(isWraning?"Warning":"Error")+" in '"+filename+"' line "+ lineNumber + "]:";
				String offsetStr = "       | ";
				out.println(string);
				out.println(offsetStr);
				out.print(offsetStr);
				out.println(line.toString().replace('\t', ' '));
				out.print(offsetStr);
				int offset = error.start - lineStart;
				for (int i = 0; i < offset; ++i) {
					out.print(" ");
				}
				for (int i = 0; i < error.length && i < (line.length() - offset); ++i) {
					out.print("^");
				}
				out.println();
				out.println(offsetStr+error.msg.replace("\n", "\n"+offsetStr));
				out.println();
				e.remove();
			}
		}
	}
}
