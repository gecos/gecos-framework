


package fr.irisa.r2d2.gecos.framework.parser.ast;


import fr.irisa.r2d2.parser.runtime.AstList;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * @generated
 */
@SuppressWarnings("all")
public class ScriptDoLoop extends ScriptNode {

	/**
	 * @generated
	 */
	protected AstList<ScriptNode> statements ;
	
	/**
	 * @generated
	 */
	protected ScriptNode expr;
	
	/**
	 * @generated
	 */
	
	/**
	 * @generated
	 */
	public ScriptDoLoop(Span span, AstList statements, ScriptNode expr) {
		super(span);
		this.statements = statements;
		this.expr = expr;
	}
	
	public AstList<ScriptNode> getStatements() {
		return statements;
	}
	
	public ScriptNode getExpr() {
		return expr;
	}
	
	
	/**
	 * @generated
	 */
	public void visit(IGecosVisitor visitor) {
		visitor.visitScriptDoLoop(this);
	}


	
}