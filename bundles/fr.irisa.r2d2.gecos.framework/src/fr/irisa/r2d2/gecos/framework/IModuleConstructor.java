/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework;

import java.util.List;

public interface IModuleConstructor {
	
	/**
	 * Gets the arguments.
	 *
	 * @return the arguments
	 */
	List<IModuleArgument> getArguments();

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	String getDescription();

	/**
	 * Append description.
	 *
	 * @param description the description
	 */
	void appendDescription(String description);

	/**
	 * Gets the module.
	 *
	 * @return the module
	 */
	IModule getModule();

	/**
	 * Gets the arguments string.
	 *
	 * @return the arguments string
	 */
	String getArgumentsString();

	/**
	 * Sets the deprecated.
	 *
	 * @param b the new deprecated
	 */
	void setDeprecated(boolean b);

	/**
	 * Checks if is deprecated.
	 *
	 * @return true, if is deprecated
	 */
	boolean isDeprecated();

	/**
	 * Gets the full description.
	 *
	 * @return the full description
	 */
	String getFullDescription();
}
