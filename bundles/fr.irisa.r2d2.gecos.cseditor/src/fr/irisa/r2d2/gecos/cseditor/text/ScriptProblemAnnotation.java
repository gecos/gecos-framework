package fr.irisa.r2d2.gecos.cseditor.text;

import org.eclipse.jface.text.source.Annotation;

public class ScriptProblemAnnotation extends Annotation {
	
	
	public static final String ERROR = "fr.irisa.r2d2.gecos.cseditor.error";
	public static final String WARNING = "fr.irisa.r2d2.gecos.cseditor.warning";
	
	public ScriptProblemAnnotation(String type, String msg) {
		setType(type);
		setText(msg);
	}

}
