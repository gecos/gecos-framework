package fr.irisa.r2d2.gecos.framework.utils;

public class TimeoutException extends RuntimeException {

	private static final long serialVersionUID = 2774772140240151636L;

	/**
	 * Instantiates a new timeout exception.
	 *
	 * @param string the string
	 */
	public TimeoutException(String string) {
		super(string);
	}
}