/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.r2d2.gecos.framework.internal.script;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;

public class PrimitiveFactory extends AbstractFunctionFactory {

	/**
	 * Instantiates a new primitive factory.
	 *
	 * @param name the name
	 * @param element the element
	 */
	public PrimitiveFactory(String name, IConfigurationElement element) {
		super(name, element);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.r2d2.gecos.framework.internal.script.AbstractFunctionFactory#createFunction()
	 */
	protected IScriptEvaluable createFunction() throws ScriptException {
		try {
			return (IScriptEvaluable) getElement().createExecutableExtension("class");
		} catch (CoreException e) {
			throw new ScriptException("impossible to create function " + getName(),e);
		}
	}
}
